require 'active_record'
require_relative 'controller'
require_relative '../../clear_admin_portal/app/models/button.rb'
require_relative '../../clear_admin_portal/app/models/decoder.rb'
require_relative '../../clear_admin_portal/app/models/gui_button.rb'
require_relative '../../clear_admin_portal/app/models/ir_netbox.rb'
require_relative '../../clear_admin_portal/app/models/node.rb'
require_relative '../../clear_admin_portal/app/models/node_type.rb'
require_relative '../../clear_admin_portal/app/models/remote.rb'
require_relative '../../clear_admin_portal/app/models/slot.rb'
require_relative '../../clear_admin_portal/app/models/xtraview_slot.rb'
require_relative '../../protocols/ruby/clear_protocol'
require_relative '../../protocols/ruby/ir_protocol'
require_relative '../ir_api'
require_relative '../../clear_admin_portal/config/environment.rb'
require_relative '../../../clearcore/corelibs/ruby/ip_utility_code'
require_relative '../../../clearcore/corelibs/ruby/clear_logger'

include Clear_Protocol
include IR_Protocol
# Handles all the various messages that it should or could and creates repsonses that gets passed back to through te controller.
# I would expect methods like controller.send(bla) to be called in this space



class MessageHandler

  def initialize controller
    TransactionHandler.init
    @controller = controller
    # Start up instances of ir_api for the amount of ir_netboxes defined for this node
    # Get the node from the databse using your IP
    node = Node.find_by_ip check_local_ip # This function from ip_utility_code.rb
    #node = Node.find_by_ip '172.31.225.103' #(Socket::getaddrinfo(Socket.gethostname))
    # {ir_netbox_ip, ir_api_intance}
    @map_irboxes = {}
    node.ir_netboxes.each do |ir_netbox|
      @map_irboxes[ir_netbox.ip] = IRApi.new method(:handle_response), ir_netbox.ip
    @map_irboxes[ir_netbox.ip].setup
  end
    #p @map_irboxes
  end

  # The the entry point to the message handler
  def handle_message from, message
    
    #Create a new transaction
    transaction_counter = TransactionHandler.get_next_transaction
    transaction = Transaction.new from, message
    TransactionHandler.transactions[transaction_counter] = transaction

    log 'clearcommon', "handle_message:#{message}"
    case message.cmd_cat
      when ClearProtocolConst.ENGINE_MESSAGES
        log 'clearcommon', 'Got ClearEngine Command Type'
        case message.cmd_type
          when ClearProtocolConst.PUSH_BUTTON
            log 'clearcommon', 'Got PUSH_BUTTON command'
            handle_push_button from, message
          else
          log 'clearcommon', "Got Command Type not yet handled in ClearEngine[#{message.cmd_cat}]: #{message.cmd_type} "
        end
      when ClearProtocolConst.COMMON_MESSAGES
        case message.cmd_type
          when ClearProtocolConst.ACK
            log 'clearcommon', 'Got ACK'
          when ClearProtocolConst.NAK
            log 'clearcommon', 'Got NAK'
        end
      else
         log 'clearcommon', "Got Command Category not yet handled: #{spo.cmd_cat} "
    end
  end

  def handle_push_button from, message
    log 'clearcommon', "Handling PushButton Command: #{message.get_button_command}"    
    slot = Slot.find_by_slot_number(message.get_slot_number)
    if slot
      #p slot.decoder.remote.buttons
      #p slot.ir_netbox
      if slot.ir_netbox
        slot.decoder.remote.buttons.each do |button|
          #TODO: Add a rescue should this IF condition fail
          if button.button_command == message.get_button_command
            disd = DownloadIRSignalData.new
            disd.set_actual_signal_data_size button.ir_representation_array.length
            p button.alphabet
            # TODO USE REMOTE TYPE HERE WHEN FIXED! ie MOD_18, MOD_16 DMOD etc. This hack will do for now
	    if button.remote.name == 'explora' || button.remote.name == '1132'|| button.remote.name == 'pvr_brown'  || button.remote.name == 'pvr_brown_tv2' || button.remote.name == 'hd_zapper' # MOD_SIG 16
              disd.set_actual_number_lengths 4 
              disd.set_number_of_periods 0
            elsif button.remote.name == '113x' || button.remote.name == 'pvr_green' #|| button.remote.name == 'hd_zapper' #MOD_SIG 18
              disd.set_actual_number_lengths button.alphabet_array.length / 2
	      p button.alphabet_array.length
              disd.set_number_of_periods 11 
            elsif button.remote.name == '720' #DMOD_SIG
              disd.set_actual_number_lengths 20 
              disd.set_number_of_periods 0
            elsif button.remote.name == 'zapper_green'#DMOD_SIG
              disd.set_actual_number_lengths button.alphabet_length
              disd.set_number_of_periods 0
            else
              puts "GOT UNKNOMN REMOTE!!!! #{button.remote.name}"
            end
            disd.set_frequency_timer_count button.frequency_timer_count.to_i
            alphabet_and_signals = []
            alphabet_and_signals = button.alphabet_array + button.ir_representation_array
            disd.set_length_and_signal_data alphabet_and_signals
            p disd
            @map_irboxes[slot.ir_netbox.ip].enqueue_message slot.ir_slot, disd, TransactionHandler.get_transaction_counter

            #send_message Ack.new :tx_number => message.tx_number
          end
        end 
      else
        # respond with Nak-No IRNetBox configured!!!
      end
    else
      # respond with Nak-Invalid Slot!!!
    end
  end
 
  def create_nak message, error_code 
    nak = Nak.new :tx_number => message.tx_number 
    nak.set_error_code  error_code
  end

  def handle_response type, tx_number
    transaction = TransactionHandler.transactions[tx_number]
    if transaction
      log 'clearcommon', "Internal Transaction Number: #{tx_number} FOR Transaction from: #{transaction.from}, for: #{transaction.message} with tx_number: #{transaction.client_transaction_number} has completed!"
      if type == :ack
        ack = Ack.new :tx_number => transaction.client_transaction_number
        send_message ack, transaction.from
        log 'clearcommon', "ACK IS : #{ack}"
      else
        nak = Nak.new :tx_number => transaction.client_transaction_number
        nak.set_error_code 12
        send_message nak, transaction.from
      end
      TransactionHandler.transactions.delete tx_number
    else
      # Send nak can't find transactions???
    end
  end

  def send_message message, to
    @controller.send_to_client message.to_binary, to
  end

end

class TransactionHandler

  def self.transactions
    @transactions
  end

  def self.init
    @transactions = {}
    @transaction_counter = 1000
  end

  def self.get_next_transaction
    @transaction_counter += 1
  end

  def self.get_transaction_counter
    @transaction_counter
  end
  
  def self.print_transactions
    p @transactions
  end

  def self.get_transaction_from_tx_number tx_number
    if @transactions.has_key? tx_number
      return @transactions[tx_number]
    else
      return nil
    end
  end


end

class Transaction

  attr_reader :from, :message, :client_transaction_number

  def initialize from, message
    @from  = from
    @message = message
    @client_transaction_number = message.tx_number
  end

end

