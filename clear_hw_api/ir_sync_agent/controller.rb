require_relative '../../../clearcore/corelibs/ruby/tcp_server'
require_relative 'message_handler'
require_relative 'constants_and_settings'
require_relative '../../../clearcore/corelibs/ruby/clear_logger'

# Hosts the tcp servers and controllers the data flow to and from it between the internals of this system and the outside world
# ie it will get a message do any pre formating on it parse it and then pass it on to the message handler for processing

include Constants

class Controller
#  include MessageHandlers

  def initialize sync_agent
    @sync_agent = sync_agent
    set_mode SETTINGS.mode 
  end

  def start_sync_agent
    begin
      #@tcp_server = TCPServerSocket.new nil
      @tcp_server = TCPServerSocket.new
      @tcp_server.start SETTINGS.server_port, method(:data_method)
      @message_handler = MessageHandler.new self
      return true
    rescue=>e
      log 'clearcommon', "Got Exception: #{e}"
      log 'clearcommon',  e.backtrace.join("\n")
      return false
    end
  end

  def send_to_client data, client
    @tcp_server.send_data_to data, client
  end

  def data_method data, from
    log 'clearcommon', 'data_method - GOT DATA!!!'
    log 'clearcommon', "data length: #{data.length}, data: #{p data}"
    message_data = []
    data.each_byte do |b|
      message_data << b
    end
    log 'clearcommon', "message_data: #{p message_data}"
    @message_handler.handle_message from, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
  end

end 
