require 'net/telnet'
require_relative '../clear_admin_portal/config/environment.rb'

class PowerUnitAPI

  def initialize
    @user = 'apc'
    @pass = 'apc -c'
    @pdu = nil
  end 

  def connect power_unit_ip
    @pdu = Net::Telnet::new('Host' => power_unit_ip,
                            'Timeout' => 25,
                            'Prompt' => /[$%#>] \z/n)
    sleep 2;
    @pdu.puts (@user) { |c| puts c }
    sleep 1;
    @pdu.puts (@pass) { |c| puts c }
  end 

  def slot_on slot_number
    slot = Slot.find_by_slot_number slot_number
    power_unit_slot = slot.power_unit_slot
    pdu = PowerUnit.find_by_id slot.power_unit_id 
    power_unit_ip = pdu.ip
    switch_on power_unit_ip, power_unit_slot 
  end 

  def switch_on power_unit_ip, power_unit_slot
    connect power_unit_ip
    sleep 1;
    @pdu.puts ("on #{power_unit_slot}") { |c| puts c }
    disconnect power_unit_ip
  end 

  def slot_off slot_number
    slot = Slot.find_by_slot_number slot_number
    power_unit_slot = slot.power_unit_slot
    pdu = PowerUnit.find_by_id slot.power_unit_id 
    power_unit_ip = pdu.ip
    switch_off power_unit_ip, power_unit_slot 
  end 

  def switch_off power_unit_ip, power_unit_slot
    connect power_unit_ip
    sleep 1;
    @pdu.puts ("off #{power_unit_slot}") { |c| puts c }
    disconnect power_unit_ip
  end 

  def slot_reboot slot_number
    slot = Slot.find_by_slot_number slot_number
    power_unit_slot = slot.power_unit_slot
    pdu = PowerUnit.find_by_id slot.power_unit_id 
    power_unit_ip = pdu.ip
    reboot power_unit_ip, power_unit_slot 
  end 

  def reboot power_unit_ip, power_unit_slot
    connect power_unit_ip
    sleep 1;
    @pdu.puts ("reboot #{power_unit_slot}") { |c| puts c }
    disconnect power_unit_ip
  end

  def disconnect power_unit_ip
    @pdu.close
  end 

end

if __FILE__ == $0

  pdu_api = PowerUnitAPI.new
  #pdu_api.reboot '10.17.183.180', 1 
  pdu_api.slot_off 1
  sleep 2
  pdu_api.slot_on 1


end 
