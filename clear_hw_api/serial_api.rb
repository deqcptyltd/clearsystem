require 'net/telnet'
require_relative '../../clearcore/corelibs/ruby/tcp_server'
require_relative '../clear_admin_portal/config/environment.rb'

class SerialAPI

  def initialize client=nil
    @sts_port_delta = 10000
    @client_port_delta = 40000
    @active = true
    @sts = nil
    @serial_server = TCPServerSocket.new
    @client = client
  end

  def connect sts_ip, port
    @sts = Net::Telnet::new('Host' => sts_ip,
                            'Port' => port,
                            'Timeout' => 25,
                            'Prompt' =>  /[$%#>] \z/n)
  end

  def sts_connect sts_ip, port, username="admin", password="superuser"
    @sts = Net::Telnet::new('Host' => sts_ip,
                            'Port' => port,
                            'Timeout' => 25,
                            'Prompt' =>  /[$%#>] \z/n)
    sleep 2;
    @sts.puts (username) { |c| puts c }
    sleep 1;
    @sts.puts (password) { |c| puts c }
  end 

  def slot_listen slot_number
    slot = Slot.find_by_slot_number slot_number
    sts_port = slot.serial_server_slot + @sts_port_delta
    serial_server = SerialServer.find_by_id slot.serial_server_id
    sts_ip = serial_server.ip
    listen sts_ip, sts_port, slot_number + @client_port_delta
  end

  
  def listen sts_ip, port, client_port
    puts " CLIENT IS #{@client}"
    puts "SERIAL LOGGER IS STARTING FOR STS #{sts_ip} on port #{port} with client port #{client_port}"
    @serial_server.start client_port, method(:serial_data_method)
    connect sts_ip, port
    while @active
      @sts.waitfor (/~/) do |serial_data|
          if @client
            send_to_client serial_data, @client
          else
            print serial_data
          end
      end
    end
  end

  def disconnect sts_ip
    @sts.close
    @active = false
  end

  def configure_baud_rate sts_ip, baud_rate=115200 , username="admin", password="superuser"
    sts_connect sts_ip, 23, "admin", "superuser" 
    @sts.puts ("set line * speed #{baud_rate}") { |c| puts c }
  end
    
  def send_to_client data, client
    @serial_server.send_data_to data, client
  end

  def serial_data_method data
    #The tcp server does not need any input, it only streams the serial log 
  end

end


if __FILE__ == $0

  serial_api = SerialAPI.new 
  #serial_api.listen '10.17.183.181', 10001
  serial_api.slot_listen 1



end
