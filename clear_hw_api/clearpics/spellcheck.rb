class SpellCheck

  def initialize
    @wrong_word = []
  end

  def check required_word, sample_word
    check_word sample_word
    if @wrong_word
      suggestions = list_word @wrong_word
      if (suggestions.include? required_word)  || (suggestions.include? required_word.downcase)
        return required_word 
      end 
    end 
    return false
  end 

private  

  def check_word word
    wrong = `echo "#{word}" | aspell --list`
    if wrong != ""
      @wrong_word = wrong.gsub(/\s+/, '')
    end
  end 

  def list_word word
    word_list = `echo "#{word}" | aspell -a`
    suggestions = word_list.lines.to_a[1..-1].join 
    suggestions.gsub!('&', '')
    suggestions.gsub!(/\s+/, '')
    return_array = suggestions.split(',')
    return_array.shift
    return_array
  end

end 

if __FILE__ == $0

  spellcheck = SpellCheck.new
  puts spellcheck.check "Movies", "llovies"

end 
