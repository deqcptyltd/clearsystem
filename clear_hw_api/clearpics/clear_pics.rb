$: << '/var/lib/gems/1.9.1/gems/rtesseract-1.2.2/lib'
$: << '/var/lib/gems/1.9.1/gems/chunky_png-1.3.1/lib'
#$: << '/home/master/.rvm/gems/ruby-1.9.3-p551@clear/gems/chunky_png-1.3.1/lib/'
#$: << '/home/master/.rvm/gems/ruby-1.9.3-p551@clear/gems/rtesseract-1.2.2/lib'
require 'chunky_png'
require 'rtesseract'
#require 'chunky_png/rmagick'
require_relative 'spellcheck'


class ClearPics

  include ChunkyPNG::Color

  attr_accessor :udp_address
  attr_accessor :path, :slot_number
  attr_accessor :video_device
  
  def check_for_movement timeout, save, percent, rectangles
    return_hash = { :status => false }
    return_hash[:results] = {}
    begin
      if @udp_address != nil
	count = 1 
        while count < timeout
	  suppress_output do
            system "ffmpeg -vcodec rawvideo -video_size 720x576 -f video4linux2 -deinterlace -i #{@video_device} -f image2 -vf fps=fps=1/1 -vframes 2 -vsync vfr #{@path}thumb%02d.png"
	  end
	  unless File.exist?("#{@path}thumb01.png") && File.exist?("#{@path}thumb02.png")
            raise "ffmpeg call did not produce desired 'thumb*.png' file"
          end

          percentage_difference = compare_images "#{@path}thumb01.png", "#{@path}thumb02.png", rectangles
	  if percentage_difference > percent
            return_hash[:status] = true
            return_hash[:results].merge! "percentage_difference" => percentage_difference
            return_hash[:results].merge! "duration" => count 
	    break
	  end
	  count += 1
	end
      end
    end
    return_hash
  end

  def compare_multiple_images rectangles, timeout, image_path=nil, percent=nil
    return_hash = { :status => false }
    return_hash[:results] = {}
    if @video_device != nil
      fps = 10
      total_images = timeout * fps.to_f
      prestart_pids = `ps -ef | grep ffmpeg | grep -v grep |   awk -F ' ' '{print $2}'`
      prestart_array = prestart_pids.split(/\n/).reject(&:empty?)
      fork do
        suppress_output do
          system "ffmpeg -vcodec rawvideo -video_size 720x576 -f video4linux2 -deinterlace -i #{@video_device} -pix_fmt rgba -vcodec rawvideo -f image2 -vf fps=fps=#{fps}/1 -vframes #{total_images} -vsync vfr #{@path}compare_multiple%02d.rgba"
        end
      end
      poststart_pids = `ps -ef | grep -v grep |  grep ffmpeg | awk -F ' ' '{print $2}'` 
      poststart_array = poststart_pids.split(/\n/).reject(&:empty?)
      found_start = false
      (1..total_images - 1).each do |count|
        count_string = count < 10 ? '0' + count.to_s : count.to_s
        count_plus_one = count + 1 < 10 ? '0' + (count + 1).to_s : (count + 1).to_s
        if Dir["#{@path}compare_multiple#{count_string}.rgba"].empty? && Dir["#{@path}compare_multiple#{count_plus_one}.rgba"].empty? 
          puts "Could not find #{@path}compare_multiple#{count_string}.rgba or #{@path}compare_multiple#{count_plus_one}.rgba"
          sleep 0.5
          next
        end

        first_image = nil

        if image_path
          first_image = image_path
        else
          first_image = Dir["#{@path}compare_multiple#{count_string}.rgba"][0]

          if !found_start && test_for_not_black(Dir["#{@path}compare_multiple#{count_string}.rgba"][0]) 
            puts "Found pixel that was not black in image #{count}"
            count += 1
            next
          else
            found_start = true
          end
        end
        
        puts "Comparing first_image #{first_image} with Dir['#{@path}compare_multiple#{count_plus_one}.rgba'][0]"
        percentage_difference = compare_images first_image, Dir["#{@path}compare_multiple#{count_plus_one}.rgba"][0], rectangles
        puts "percentage_difference #{percentage_difference}"
        if percent
          if percentage_difference < 100 - percent
            return_hash[:status] = true
            return_hash[:results].merge! "duration"  => count  * (1 / fps.to_f)
            break
          end
        else
          if percentage_difference > 10
            return_hash[:status] = true
            return_hash[:results].merge! "duration"  => count  * (1 / fps.to_f)
            break
          end
        end
      end
    end
    
    # Cleanup ffmpeg process
    ffmpeg_pid = compare(prestart_array, poststart_array)
    if ffmpeg_pid
      `kill -9 #{ffmpeg_pid}`
    end

    puts "RETURN HASH IS #{return_hash}"
    return_hash
  end

  def determine_path slot_number
    working_dir = `pwd`.strip()
    common_folder = working_dir.index "clearsystem"
    @path = working_dir[0..common_folder]
    @path << "clearsystem"[1..-1]  
    @path << "/public/slot#{slot_number}/"
    @slot_number = slot_number
  end

  def clean_path slot_number
    to_clean = determine_path slot_number
    puts "THIS IS THE CLEAN_PATH PATH - #{@path}"
    puts `rm -rf #{@path}*.png`
    puts `rm -rf #{@path}*.tiff`
    puts `rm -rf #{@path}*.rgba`
  end 

  def take_picture type, rectangles = nil
    puts "CALLING TAKE PICTURE"
    if File.exist? "#{@path}snapshot.#{type.downcase}" 
      puts "RENAMING SNAPSHOT WITH TYPE #{type}"
      File.rename( "#{@path}snapshot.#{type.downcase}", "#{@path}snapshot.#{type.downcase}.old" )
    end
    if @video_device != nil
      suppress_output do
        system "ffmpeg -vcodec rawvideo -video_size 720x576 -f video4linux2 -deinterlace -i #{@video_device} -f image2 -vf fps=fps=2/1 -vframes 20 -vsync vfr #{@path}snapshot.#{type.downcase}"
      end 
      sleep 1
      unless File.exist? "#{@path}snapshot.#{type.downcase}"
        puts "ffmpeg call did not produce desired 'snapshot.#{type} file"
        raise "ffmpeg call did not produce desired 'snapshot.#{type} file"
      end
  
      paths = []
      if rectangles
        rectangles.each_pair do |key, rectangle|
          image = RTesseract.read("#{@path}snapshot.#{type.downcase}") do |img| 
            x = rectangle[:x].to_i
            y = rectangle[:y].to_i
            width = rectangle[:width].to_i
            height = rectangle[:height].to_i
            img = img.excerpt(x, y, width, height)
            img.write("#{@path}snapshot_rectangle#{key}.#{type.downcase}")
            paths <<  (File.expand_path "#{@path}snapshot_rectangle#{key}.#{type.downcase}").to_s
          end
        end
      else
        paths << (File.expand_path "#{@path}snapshot.#{type.downcase}").to_s
        p paths
      end

      return paths.join ', '
    end
  end

  def move_picture type
    if File.exist? "#{@path}snapshot.#{type.downcase}" 
      File.rename( "#{@path}snapshot.#{type.downcase}", "#{@path}snapshot-#{Time.now.to_i}.#{type.downcase}" )
    end
  end 

  def call_compare_images file_reference, percent, time_out, rectangles, file_reference_rectangles=nil
    return_hash = { :status => false }
    return_hash[:results] = {}
    if File.exist? file_reference
      new_picture = take_picture "png"
      percentage_difference = compare_images file_reference, new_picture, rectangles, file_reference_rectangles
      return_hash[:status] = percentage_difference < 100 - percent
      return_hash[:results].merge! "percentage_match" => 100 - percentage_difference
      puts "#{return_hash}"
      return return_hash
    else
      raise "File reference not found"
    end
  end

  def compare_images file_one, screen_capture, areas_to_compare, file_reference_rectangles=nil
    puts "Comparing #{file_one} with #{screen_capture}"
    if (File.exist? file_one) && (File.exist? screen_capture)
      images = []
      files = [file_one, screen_capture]
      files.each do |file|
        file_type = file[file.rindex('.') + 1, file.length]
        if file_type == 'png'
          images << ChunkyPNG::Image.from_file(file)
        elsif file_type == 'rgba'
          images << ChunkyPNG::Image.from_rgba_stream(720, 576, File.read(file))
        else
          puts "That file format, #{file_type}, is not allowed"
          raise "File type not accepted"
        end
      end

      if file_reference_rectangles
        if areas_to_compare.length != 1 && file_reference_rectangles.length != 1
          return 0
        end
        areas_to_compare.each do |key, area|
          file_reference_rectangles.each do |file_reference_key, rectangle|
            if area[:width] != rectangle[:width] || area[:height] != rectangle[:height]
              return 0
            end
            images.first.crop!(rectangle[:x], rectangle[:y], rectangle[:width], rectangle[:height])
            images.first.save("#{@path}/first.png")
          end
          images.last.crop!(area[:x], area[:y], area[:width], area[:height])
          images.last.save("#{@path}last.png")
        end
        areas_to_compare.clear
        file_reference_rectangles.each do |file_reference_key, rectangle|
          areas_to_compare[file_reference_key] = { x: 0, y: 0, width: rectangle[:width] - 1, height: rectangle[:height] - 1 }
        end
      end

      output = ChunkyPNG::Image.new(images.first.width, images.first.height, WHITE)
      diff = []
      max_percentage_difference = 0
      if areas_to_compare != nil
        total_area_size = 0
        areas_to_compare.each_pair do |key, rectangle|
          x_start = rectangle[:x].to_i
          x_stop = rectangle[:x].to_i + rectangle[:width].to_i
          (x_start..x_stop).each do |x|
            y_start = rectangle[:y].to_i
            y_stop = rectangle[:y].to_i + rectangle[:height].to_i
            (y_start..y_stop).each do |y|
              unless images.first[x, y] == images.last[x, y]
                score = Math.sqrt(
                (r(images.last[x,y]) - r(images.first[x, y])) ** 2 + (g(images.last[x,y]) - g(images.first[x, y])) ** 2 +
                (b(images.last[x,y]) - b(images.first[x, y])) ** 2) / Math.sqrt(MAX ** 2 * 3)
                output[x, y] = grayscale(MAX - (score * MAX).round)
                diff << score
              end
            end
          end
          output.save("#{@path}diff.png")
          current_area_size = rectangle[:width].to_i * rectangle[:height].to_i
          total_area_size += rectangle[:width].to_i * rectangle[:height].to_i
        end
        if diff.any?
          percentage_difference = ((diff.inject {|sum, value| sum + value} / total_area_size) * 100).round(2)
          max_percentage_difference = percentage_difference if percentage_difference > max_percentage_difference
          puts "difference = #{max_percentage_difference}%"
          return max_percentage_difference
        else
          return 0
        end
      end
    else
      raise "Cannot compare files if either file does not exist" 
    end
  end

  def do_catalogue_ocr rectangles
    image_file = take_picture "tiff"
    if !File.exists? image_file 
      raise "File does not exist"
      return 1
    end
    return_object = Hash.new
    return_object[:status] = true
    return_object[:results] = {}
    return_object[:results].merge! :images => []
    image = RTesseract.read(image_file) do |img|
      img.write("#{image_file[0, image_file.rindex('.')]}.png")
      return_object[:results][:images] << "#{image_file[0, image_file.rindex('.')]}.png"
    end 
    if rectangles == nil
      rectangles = Hash.new
      image = RTesseract.read(image_file) do |img|
        rectangle = {x: 0, y: 0, width: img.columns - 1, height: img.rows - 1}
        rectangles[1] = rectangle
      end 
    end
    count = 1
    rectangles.each_pair do |key, rectangle|
      image = RTesseract.read(image_file) do |img| 
        p img.class
        time_string = Time.now.to_i.to_s
        x = rectangle[:x].to_i
        y = rectangle[:y].to_i
        width = rectangle[:width].to_i
        height = rectangle[:height].to_i
        img = img.excerpt(x, y, width, height)

        # This is used on the client to generate a log. All the processing below needs to happen after the png is saved
        # otherwise the png is impossible to view
        img = img.separate  # grayscale
        img = img.scale(3)

        #Recommended to keep this loop value below 6
        3.times do
          img = img.sharpen(0, 2)
          img = img.despeckle
        end 
        img = img.gaussian_blur(2, 2)#img.write("#{@path}ocr_file#{count}.png")

        img.write("#{@path}ocr_file#{count}_before_contrast-#{time_string}_slot#{@slot_number}.png")
        #if img.rows < 120
          #15.times {img = img.contrast true}
        #end
        #img = img.sharpen(0, 3)

        img.write("#{@path}ocr_file#{count}.tiff")
        img.write("#{@path}ocr_file#{count}.png")
        start_of_text = find_start_of_image_text "#{@path}ocr_file#{count}.png"
        img.write("#{@path}ocr_file-#{time_string}_slot#{@slot_number}.png")
        return_object[:results][:images] << "#{@path}ocr_file-#{time_string}_slot#{@slot_number}.png"
      end
      rt = RTesseract.new("#{@path}ocr_file#{count}.tiff", :lang => "eng", :processor => 'rmagick')
      puts "OCR found - #{rt.to_s}"
      result = false
      if !result
        return_object[:results].merge! "#{ key }_string" => rt.to_s
      end
      return_object[:results].merge! "#{ key }_result" => result
      return_object[:status] = result
      count += 1
    end
    return_object
  end

  def do_ocr rectangles=nil
    image_file = take_picture "tiff"
    if !File.exists? image_file 
      raise "File does not exist"
      return 1
    end
    return_object = Hash.new
    return_object[:status] = true
    return_object[:results] = {}
    return_object[:results].merge! :images => []
    image = RTesseract.read(image_file) do |img|
      img.write("#{image_file[0, image_file.rindex('.')]}.png")
      return_object[:results][:images] << "#{image_file[0, image_file.rindex('.')]}.png"
    end 
    if rectangles == nil
      rectangles = Hash.new
      image = RTesseract.read(image_file) do |img|
        rectangle = {x: 0, y: 0, width: img.columns - 1, height: img.rows - 1}
        rectangles[1] = rectangle
      end 
    end
    count = 1
    rectangles.each_pair do |key, rectangle|
      image = RTesseract.read(image_file) do |img| 
        p img.class
        x = rectangle[:x].to_i
        y = rectangle[:y].to_i
        width = rectangle[:width].to_i
        height = rectangle[:height].to_i
        img = img.excerpt(x, y, width, height)

        # This is used on the client to generate a log. All the processing below needs to happen after the png is saved
        # otherwise the png is impossible to view
        #img.write("#{@path}ocr_file#{count}.png")
        img = img.separate  # grayscale
        img = img.scale(3)
        # img.contrast true
        time_string = Time.now.to_i.to_s
        img.write("#{@path}ocr_file#{count}_before_contrast-#{time_string}_slot#{@slot_number}.png")
        is_black_on_white = test_for_not_black("#{@path}ocr_file#{count}_before_contrast-#{time_string}_slot#{@slot_number}.png")
        despeckle_count = 6
        if is_black_on_white
          despeckle_count = 4
        end
        despeckle_count.times do
          img = img.sharpen(0, 2)
          img = img.despeckle
        end 
        img = img.gaussian_blur(2, 2)
        
        puts "width is #{img.columns} and height is #{img.rows}"
        puts "is_black_on_white #{is_black_on_white}"
        if (img.rows < 120 && img.columns < 100) || is_black_on_white
          contrast_count = 10
          if is_black_on_white
            contrast_count = 20
          end
          contrast_count.times do
            # img = img.contrast true
          end 
        end

        img.write("#{@path}ocr_file#{count}.tiff")
        img.write("#{@path}ocr_file#{count}.png")
        start_of_text = find_start_of_image_text "#{@path}ocr_file#{count}.png"
        end_of_text = 0
        new_top = 0
        new_bottom = img.rows
        if img.rows < 120
          end_of_text = img.columns - find_end_of_image_text("#{@path}ocr_file#{count}.png")
          new_top = 0.15 * img.rows
          if img.rows > 65
            new_bottom = 0.9 * img.rows
          end
        end

        puts "Image width is #{img.columns} and start_of_text #{start_of_text} is and end_of_text is #{end_of_text}"
        img = img.excerpt(start_of_text, 0, img.columns - start_of_text - end_of_text, img.rows)
        img.write("#{@path}ocr_file-final.tiff")
        img.write("#{@path}ocr_file-final.png")
        img.write("#{@path}ocr_file-#{time_string}_slot#{@slot_number}.png")
        return_object[:results][:images] << "#{@path}ocr_file-#{time_string}_slot#{@slot_number}.png"
      end
      rt = RTesseract.new("#{@path}ocr_file-final.tiff", :lang => "eng", :processor => 'rmagick')
      puts "OCR found - #{rt.to_s}"
      result = false
      if !result
        return_object[:results].merge! "#{ key }_string" => rt.to_s
      end
      return_object[:results].merge! "#{ key }_result" => result
      return_object[:status] = result
      count += 1
    end
    return_object
  end

  def test_for_not_black image_file_name
    file_type = image_file_name[image_file_name.rindex('.') + 1..-1]
    puts file_type
    if file_type == 'png'
      image = ChunkyPNG::Image.from_file(image_file_name)
    else
      image = ChunkyPNG::Image.from_rgba_stream(720, 576, File.read(image_file_name))
    end
    found_not_black = false
    pixel_count = 0
    12.times do |position|
      if r(image[position, position]) > 100 || g(image[position, position]) > 100 || b(image[position, position]) > 100
        found_not_black = true
        pixel_count += 1
        if pixel_count > 2
          break
        end
      elsif pixel_count > 0
        pixel_count = 0
      end
    end
    found_not_black
  end

  def find_start_of_image_text file_name
    RTesseract.read(file_name) do |img|
      20.times do
        img = img.contrast true
      end 
      img.write("#{@path}contrasted.png")
    end
    image = ChunkyPNG::Image.from_file("#{@path}contrasted.png")
    width = image.width
    height = image.height
    is_background_black = true
    colour_pixel_count = 0
    20.times do |position|
      if r(image[position, position]) > 50 && g(image[position, position]) > 50 && b(image[position, position]) > 50
        colour_pixel_count += 1
      end
    end
    if colour_pixel_count > 10
      is_background_black = false
    end
    start_of_image_text = nil
    height_factors = [0.33, 0.5, 0.67]
    height_factors.each do |i|
      colour_pixel_count = 0
      width.times do |w|
        red = r(image[w, (height * i).to_i])
        green = g(image[w, (height * i).to_i])
        blue = b(image[w, (height * i).to_i])
        # puts "w =  #{w} and the height is #{(height * i).to_i} and Red #{red} Green #{green} Blue #{blue}"
        if is_background_black
          if red > 10 && green > 10 && blue > 10
            colour_pixel_count += 1
          else
            colour_pixel_count = 0
          end
        else
          if red < 10 && green < 10 && blue < 10
            colour_pixel_count += 1
          else
            colour_pixel_count = 0
          end
        end
        if colour_pixel_count > 3
          if w > 30
            if start_of_image_text
              start_of_image_text = [start_of_image_text, w - 30].min
            else
              start_of_image_text = w - 30
            end
          else
            start_of_image_text = 0
          end
          break
        end
      end
    end
    if !start_of_image_text
      start_of_image_text = 0
    end
    return start_of_image_text
  end

  def find_end_of_image_text file_name
    image = ChunkyPNG::Image.from_file("#{@path}contrasted.png")
    width = image.width
    height = image.height
    is_background_black = true
    colour_pixel_count = 0
    20.times do |position|
      if r(image[width - position - 1, position]) > 10 && g(image[width - position - 1, position]) > 10 && b(image[width - position - 1, position]) > 10
        colour_pixel_count += 1
      end
    end
    if colour_pixel_count > 10
      is_background_black = false
    end
    end_of_image_text = nil
    height_factors = [0.33, 0.5, 0.67]
    height_factors.each do |i|
      colour_pixel_count = 0
      width.downto(1) do |w|
        red = r(image[w - 1, (height * i).to_i])
        green = g(image[w - 1, (height * i).to_i])
        blue = b(image[w - 1, (height * i).to_i])
        # puts "w =  #{w} and the height is #{(height * 0.5).to_i} and Red #{red} Green #{green} Blue #{blue}"
        if is_background_black
          if red > 10 && green > 10 && blue > 10
            colour_pixel_count += 1
          else
            colour_pixel_count = 0
          end
        else
          if red < 10 && green < 10 && blue < 10
            colour_pixel_count += 1
          else
            colour_pixel_count = 0
          end
        end
        if colour_pixel_count > 3
          if width - w > 30
            if end_of_image_text
              end_of_image_text = [end_of_image_text, w + 30].max
            else
              end_of_image_text = w + 30
            end
          else
            end_of_image_text = width
          end
          break
        end   
      end
    end
    if !end_of_image_text
      end_of_image_text = width
    end
    return end_of_image_text
  end

  def colour_check red, green, blue, rectangles, threshold, percent
    image_file = take_picture "png"
    image = ChunkyPNG::Image.from_file(image_file)
    return_object = Hash.new
    return_object[:status] = true
    return_object[:results] = {}
    pixel_match_count = 0
    if rectangles == nil
      rectangles = Hash.new
      image = RTesseract.read(image_file) do |img|
        rectangle = {x: 0, y: 0, width: img.columns - 1, height: img.rows - 1}
        rectangles[1] = rectangle
      end 
    end
    rectangles.each do |key, rectangle|
      image.crop(rectangle[:x], rectangle[:y], rectangle[:width], rectangle[:height]).save("#{@path}colour_check_#{key}.png")
      x_start = rectangle[:x].to_i
      x_stop = rectangle[:x].to_i + rectangle[:width].to_i
      (x_start..x_stop).each do |x|
        y_start = rectangle[:y].to_i
        y_stop = rectangle[:y].to_i + rectangle[:height].to_i
        (y_start..y_stop).each do |y|
          r_diff = (red - r(image[x, y])).abs
          g_diff = (green - g(image[x, y])).abs
          b_diff = (blue - b(image[x, y])).abs
          if r_diff <= threshold && g_diff <= threshold && b_diff <= threshold
            pixel_match_count += 1
          end
        end
      end
      return_object[:results].merge! "match_percentage_#{ key }" => (pixel_match_count / (rectangle[:width].to_i * rectangle[:height].to_i).to_f) * 100
      return_object[:status] = percent < (pixel_match_count / (rectangle[:width].to_i * rectangle[:height].to_i).to_f) * 100
    end
    p "colour_check #{return_object}"
    return_object
  end

  def suppress_output
    begin
      orig_stderr = $stderr.clone
      orig_stdout = $stdout.clone
      $stderr.reopen File.new('/dev/null', 'w')
      $stdout.reopen File.new('/dev/null', 'w')
      retval = yield
    rescue Exception => e
      $stdout.reopen orig_stdout
      $stderr.reopen orig_stderr
      raise e
    ensure
      $stdout.reopen orig_stdout
      $stderr.reopen orig_stderr
    end
    retval
  end

  private
    def compare first, second 
      diff = second - first 
      diff[0].to_i
    end
end

if __FILE__ == $0

  clear_pics = ClearPics.new
  clear_pics.path = "/tmp/"
  clear_pics.video_device = "/dev/video1"
  clear_pics.udp_address = "not nil"
  #  ScreenNameBounds            = (64,72,192,34)
  #  MainMenuBounds              = (74,111,176,23)
  #  MainMenuBoundsOffSet        = (0,0,0,0)
  #  DropDownMenuBounds          = (80,146,158,30)#(96,141,170,30)
  #  DropDownMenuBoundsOffSet    = (0,31,0,0)
  #  SubMenuBounds               = (272,142,166,31)#(286,141,170,30)
  #  SubMenuBoundsOffSet         = (0,31.5,0,0)
  #  SubMenuOptionBounds         = (71,150,364,30)
  #  SubMenuOptionValueBounds    = (270,151,154,30)
  #  SubMenuOptionBoundsOffSet   = (0,32.5,0,0)
  #  SubMenuItemHelpTextBounds   = (455,142,190,283)
  #  SubMenuOptionAddBounds      = (62,182,376,32)
  #  SubMenuOptionValueAddBounds = (266,180,154,32)
  #  
  #  SubMenuOptionAddBounds2      = (65,198,382,32)
  #  SubMenuOptionValueAddBounds2 = (266,198,154,32)
  

  # rectangles = { "0" => {x:74, y:146 - 16 + (32*0), width:158, height:30} }
  # rectangles = { "0" => {x:270, y:151 - 16 + (32*0), width:154, height:30} }
  # rectangles = { "0" => {x:266, y:180 - 16 + (32*0), width:154, height:32} }

   rectangles = { "0" => {x:73, y:378, width:210, height:19} } # Catalogue
  # rectangles = { "0" => {x:355, y:185, width:15, height:25} } # Playlist Record Icon
  # rectangles = { "0" => {x:370, y:185, width:15, height:25} } # Playlist Record Icon
  # rectangles = { "0" => {x:300, y:177, width:124, height:30} } # Option Number 2
  # rectangles = { "0" => {x:475, y:142, width:190, height:280} } # Help Text
  # rectangles = {"0"=>{:x=>240, :y=>416, :width=>211, :height=>21}} # I-Plate Channel Number and Name
  # rectangles = {"0"=>{:x=>174, :y=>452, :width=>164, :height=>32}} # I-Plate Line Two Base
  # rectangles = {"0"=>{:x=>210, :y=>190, :width=>300, :height=>240}} # Notification
  # rectangles = {"0"=>{:x=>220, :y=>142, :width=>190, :height=>33}} # SubMenuOptionBounds
  # rectangles = {"0"=>{:x=>275, :y=>168, :width=>175, :height=>24}} # SubMenuOptionValueBounds
 #  rectangles = {"0"=>{:x=>70, :y=>100, :width=>190, :height=>24}} # MainMenuBounds
   clear_pics.slot_number = 10001

  # result =  clear_pics.do_ocr rectangles
  result =  clear_pics.do_catalogue_ocr rectangles
  # other_rectangles = rectangles.clone
  # clear_pics.call_compare_images '/home/master/clear/cleartest/resources/images/CF_playlist_screen_first_record_currently_watching_icon.png', 35, 30, rectangles, other_rectangles
  # clear_pics.colour_check 190, 190, 190, rectangles, 90, 30

  #rectangles = { "0" => {x:0, y:0, width:719, height:575} } # Playlist Record Icon
  #clear_pics.check_for_movement 10, true, 80, rectangles

end
