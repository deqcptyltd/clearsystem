require_relative 'spellcheck'

class OCRParser

  attr_accessor :matched_strings
  attr_accessor :unmatched_strings
  attr_accessor :status
  attr_accessor :method
  attr_accessor :matched_strings_duplicate

  def initialize required_strings, mode
    @mode = mode
    @search_string = ''
    @required_strings = required_strings 
    @matched_strings = []
    @unmatched_strings = []
    @required_strings_duplicate = []
    @method = ''
  end

  def match search_string 
    found = false
    until found do 
      @search_string = search_string 
      if @search_string != ''
        @status = true
      end 
      @search_string = @search_string.gsub(/\n/, '')
      @search_string = @search_string.gsub(/\s+\Z/, '')
      puts "SEARCH STRING IS #{@search_string}"
      if match_perfect then break end 
        create_arrays
      if default_parse then break end 
        run_spellcheck 
      if default_parse then break end 
        @required_strings_duplicate = @required_strings
        temp_array = []
        @matched_strings.each do |matched_string| 
          new_string = matched_string.split(' ') 
          temp_array << new_string
        end 
        @required_strings = temp_array
        default_parse
        @required_strings = @required_strings_duplicate
      break
    end 
    @unmatched_strings = []
    if @required_strings[0].class == Array 
      @required_strings.each_with_index { |required_string, index| @unmatched_strings << required_string.join(' ') }
    else 
      @required_strings.each_with_index { |required_string, index| @unmatched_strings << required_string }
    end 
    @required_strings = @unmatched_strings
  end 

  private

  def default_parse
    if match_multinary then return true end 
    if match_multinary_with_drilldown then return true end 
    if match_multinary_with_concat then return true end 
    if match_unary then return true else return false end
  end

  def remove_non_ascii 
    encoding_options = {
      :invalid => :replace,
      :undef => :replace,
      :replace => '',
    }
    @search_string.encode(Encoding.find('ASCII'), encoding_options)
  end 

  def create_arrays 
    temp_array = []
    @required_strings.each do |required_string| 
      new_string = required_string.split(' ') 
      temp_array << new_string
    end 
    @required_strings = temp_array
    @search_string = @search_string.split(' ')
  end
  
  def match_perfect 
    @required_strings.each do |required_string|
      if required_string.downcase == @search_string.downcase
        @matched_strings << required_string 
        @required_strings.delete required_string
        puts "MATCHED #{required_string} WITH PERFECT STRATEGY"
        @method = 'Perfect'
        return true
      end 
    end 
    return false
  end
  
  def match_unary threshold=0.6 
    puts "match unary"
    @required_strings.each do |required_string| 
      if @search_string.size * required_string.size == 1
        sample_word = @search_string[0]
        required_word = required_string[0]
        result = drilldown required_word, sample_word, threshold
        if result && (!@matched_strings.include? required_string[0])
          @matched_strings << required_string[0]
          @required_strings.delete required_string
          puts "MATCHED #{required_string} WITH UNARY STRATEGY"
          @method = 'Unary'
          return true
        end 
      end 
    end 
    return false
  end 

  def match_multinary
    @required_strings.each do |required_string| 
      matched_word_array = []
      required_string.each do |required_word| 
        if @search_string.include? required_word 
          matched_word_array << required_word 
        end 
      end 
      #puts matched_word_array
      if (matched_word_array.size.to_f / required_string.size.to_f) >= 0.6
        if !@matched_strings.include? required_string
          @matched_strings << required_string.join(' ')
          @required_strings.delete required_string
          puts "MATCHED #{required_string} WITH MULTINARY STRATEGY"
          @method = 'Multinary'
          return true
        end 
      end 
    end 
    return false
  end 

  def match_multinary_with_drilldown 
    @required_strings.each_with_index do |required_string, required_index| 
      matched_word_array = []
      sample_word_array = []
      if @mode == :catalogue
        puts "Mode is catalogue"
        if ((required_string.size.to_f / @search_string.size.to_f) >= 0.6) && ((@search_string.size.to_f / required_string.size.to_f) >= 0.6)
          required_string.each do |required_word| 
            @search_string.each do |found_word| 
              if required_word.downcase == found_word.downcase 
                sample_word_array << required_word
              else 
                result = drilldown required_word, found_word, 0.65
                if result 
                  sample_word_array << result 
                end 
              end 
            end 
          end 
          required_string.each do |required_word| 
            if sample_word_array.include? required_word 
              matched_word_array << required_word 
            end 
          end 
          if ((matched_word_array.size.to_f / required_string.size.to_f) >= 0.65) 
            if !@matched_strings.include? required_string
              @matched_strings << required_string.join(' ')
              @required_strings.delete required_string
              puts "MATCHED #{required_string} WITH MULTINARY-DRILLDOWN STRATEGY"
              @method = 'Multinary Drilldown'
              return true
            end 
          end 
        end 
      else 
        required_string.each do |required_word| 
          @search_string.each do |found_word| 
            if required_word.downcase == found_word.downcase 
              sample_word_array << required_word
            else 
              result = drilldown required_word, found_word 
              if result 
                sample_word_array << result 
              end 
            end 
          end 
        end 
        required_string.each do |required_word| 
          if sample_word_array.include? required_word 
            matched_word_array << required_word 
          end 
        end 
        if ((matched_word_array.size.to_f / required_string.size.to_f) >= 0.6) 
          if !@matched_strings.include? required_string
            @matched_strings << required_string.join(' ')
            @required_strings.delete required_string
            puts "MATCHED #{required_string} WITH MULTINARY-DRILLDOWN STRATEGY"
            @method = 'Multinary Drilldown'
            return true
          end 
        end 
      end 
    end 
    return false
  end 

  def match_multinary_with_concat
    concat_array = []
    @required_strings.each_with_index do |required_string, index| 
      concat_array << required_string.join
    end 
    sample_string = @search_string.join
    concat_array.each_with_index do |concat_string, concat_index| 
      result = drilldown concat_string, sample_string, 0.75
      if result
        if !matched_strings.include? @required_strings[concat_index]
          @matched_strings << @required_strings[concat_index].join(' ')
          @required_strings.delete_at concat_index
          puts "MATCHED #{@required_strings[concat_index]} WITH MULTINARY-CONCAT STRATEGY"
          @method = 'Multinary Concat'
          return true
        end 
      end 
    end 
    return false
  end 

  def drilldown required_word, sample_word, threshold=0.6
    sample_word_array = sample_word.split('')
    required_word_array = required_word.split('')
    if required_word_array.size < 4
      return false
    end
    word_array = []
    temp_array = required_word_array
    required_word_array.each_with_index do |required_letter, i|
      if (sample_word_array.include? required_letter.downcase) || (sample_word_array.include? required_letter.upcase)
        word_array << required_letter
        required_word_array[i] = ''
        if sample_word_array.include? required_letter.downcase
          sample_word_array[sample_word_array.index(required_letter.downcase)] = ''
        else
          sample_word_array[sample_word_array.index(required_letter.upcase)] = ''
        end
      end 
    end
    required_word_array.delete('')
    puts "threshold is #{threshold}"
    puts "THRESHOLDS WERE:"
    puts ((word_array.size.to_f / required_word.size.to_f))
    puts ((required_word.size.to_f / sample_word.size.to_f))
    if ((word_array.size.to_f / required_word.size.to_f) >= threshold) && ((required_word.size.to_f / sample_word.size.to_f) >= threshold)
      puts "DRILLDOWN SUCCESS: Sample: #{word_array} Required: #{required_word}"
      return required_word 
    else 
      return false 
    end 
  end 
  
  def run_spellcheck 
    puts "RUNNING SPELLCHECK"
    spellcheck = SpellCheck.new
    @required_strings.each do |required_string| 
      required_string.each do |required_word| 
        @search_string.each_with_index do |found_word, found_word_index| 
          result = spellcheck.check required_word, found_word
          if result 
            puts "SPELLCHECK RESULT IS #{result}"
            @search_string.delete found_word  
            @search_string.insert(found_word_index, result)
          end 
        end 
      end 
    end 
  end 

end 


if __FILE__ == $0

  required_strings = ['If I Stay']
 # required_strings = ['The quick brown fox jumps over the lazy dog', 'Ciao', 'Hello', 'I have multiple incorrect words', 'I have too many incorrect words' ]
 # found_strings  ww= ['the Quick brown fox jumps over the lazy dog', 'ciaa', 'Mella', 'I have multipl increct words', 'i hav too mny ncorrec word' ]

  #found_strings = ["Transformers: Age of Extinction\n\n", "Think Like A Man Ton\n\n", "Guardians of the Galaxy\n\n", "Wish I Was Here\n\n", "Repentance\n\n", "Black Nativity: Extended Musical...\n\n", "The Invisible Woman\n\n", "Tammr\n\n", "Dawn of the Planet cf the Apes\n\n", "How to Train Your Dragon 2\n\n", "The Hundred-Fool Joumey\n\n", "Drive Hard\n\n", "Trevor Noah: African American\n\n", "Tyler Perry's A Iadea Christmas\n\n", "Die Spookvan Uniandale\n\n", "Bad Neighbours\n\n", "204: Getting Away with Hurder\n\n", "The Elpendables 3\n\n", "Behaving Badly\n\n"]
  search_string = 'lll Stay'
  #required_strings = ['have']
  #required_strings = ["The Necessary Death of Charlie...", "Behaving Badly", "22 Jump Street", "Tinker Bell and the Pirate Fairy", "Bad Neighbours", "Chef", "How to Train Your Dragon 2", "Trevor Noah: African American", "Guardians of the Galaxy", "Dawn of the Planet of the Apes", "The Expendables 3", "The Invisible Woman", "Tammy", "Think Like A Man Too", "Repentance", "Tyler Perry's A Madea Christmas", "Transformers: Age of Extinction", "204: Getting Away with Murder", "Die Spook van Uniondale", "Black Nativity: Extended Musical...", "Wish I Was Here", "Drive Hard", "Life of a King", "The Hundred-Foot Journey"]




  catalogue_parser = OCRParser.new  required_strings, :catalogue
  catalogue_parser.match search_string
  print  'Matched: '
  p catalogue_parser.matched_strings
  print  'Unmatched: '
  p catalogue_parser.unmatched_strings

end 
