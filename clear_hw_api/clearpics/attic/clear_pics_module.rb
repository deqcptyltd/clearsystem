$: << '/var/lib/gems/1.9.1/gems/rtesseract-1.2.2/lib'
$: << '/var/lib/gems/1.9.1/gems/chunky_png-1.3.1/lib'
require 'rtesseract'
require 'chunky_png'

include ChunkyPNG::Color
class ClearPics
  @percentage_change_trigger = 8  # The percentage 2 pics have to
                                      # change by to be considered movement
  @udp_address = nil
  @video_device_address = nil
  @timeout = 1000   #value in milliseconds

  def set_address_udp address
    @udp_address = address
    @video_device_address = nil
    unless @udp_address.downcase.include? "udp://"
      raise "address not in correct format. i.e. 'udp://239.1.1.1:2001'"
    end
  end

  def set_address_video_device videoDevice
    @video_device_address = videoDevice
    @udp_address = nil
     unless @video_device_address.downcase.include? "/dev/video"
      raise "address not in correct format. i.e. '/dev/video0'"
    end
  end

  def set_percentage_change_trigger value
    begin
      @percentage_change_trigger = value.to_i
    rescue
      raise "set_percentage_change_trigger value cannot be converted to integer"
    end
  end

  def set_timeout value
    begin
      @timeout = value.to_i
    rescue
      raise "value recived cannot be converted to integer"
    end
  end

  # This method will take screenshots on a specific stream,
  # compare them and return true/false based on % change
  #TODO add @timeout value to be delay between pictures
  def check_for_movement timeout, save, percent, rectangles
    return_hash = { :status => false }
    return_hash[:results] = {}
    begin
      if @udp_address != nil
        stfu do
          system "ffmpeg -analyzeduration 10M -probesize 10M -i #{@udp_address} -deinterlace -f image2 -vf fps=fps=1/1 -vframes 2 -vsync vfr thumb\%02d.png"
        end
        unless File.exist? "thumb01.png"
          raise "ffmpeg call did not produce desired 'thumb*.png' file"
        end
      elsif @video_device_address != nil
        stfu do
          system "ffmpeg -analyzeduration 10M -probesize 10M -vcodec rawvideo -video_size 720x576 -f video4linux2 -deinterlace -i #{@video_device_address} -f image2 -vf fps=fps=1/1 -vframes 2 -vsync vfr thumb\%02d.png"
        end
        unless File.exist? "thumb01.png"
          raise "ffmpeg call did not produce desired 'thumb*.png' file"
        end
      end
    rescue Exception=>e
      puts "it broke :-(\n\rCaught: #{e}"
    else
      _percentage_difference = compare_two_images 'thumb01.png', 'thumb02.png', rectangles
      return_hash[:status] = true
      return_hash[:results].merge! "percentage_difference" => _percentage_difference
      return_hash[:results].merge! "duration" => 0.123123 #TODO: get the amout of time it took 
      puts "check_for_movement #{_percentage_difference >= percent}"
      return return_hash
    end
  end

  # Method to send ffmpeg garble to infinity... and beyond!
  # Not in use at the moment
  def stfu
    begin
      orig_stderr = $stderr.clone
      orig_stdout = $stdout.clone
      $stderr.reopen File.new('/dev/null', 'w')
      $stdout.reopen File.new('/dev/null', 'w')
      retval = yield
    rescue Exception => e
      $stdout.reopen orig_stdout
      $stderr.reopen orig_stderr
      raise e
    ensure
      $stdout.reopen orig_stdout
      $stderr.reopen orig_stderr
    end
    retval
  end

  def take_picture type, rectangles = nil
    #TODO: Whatever we need to do with rectangles
    if File.exist? "snapshot.#{type.downcase}" 
      File.rename( "snapshot.#{type.downcase}", "snapshot.#{type.downcase}.old" )
    end
    if @udp_address != nil
      stfu do
      system "ffmpeg -y -analyzeduration 10M -probesize 10M -i #{@udp_address} -deinterlace -f image2 -vf fps=fps=3/1 -vframes 1 -vsync vfr snapshot.#{type.downcase}"
      end
      unless File.exist? "snapshot.#{type.downcase}"
          raise "ffmpeg call did not produce desired 'snapshot.#{type} file"
      end
      return (File.expand_path "snapshot.#{type.downcase}").to_s
    elsif @video_device_address != nil
      stfu do
        system "ffmpeg -y -analyzeduration 10M -probesize 10M -vcodec rawvideo -video_size 720x576 -f video4linux2 -i #{@video_device_address} -f image2 -vf fps=fps=3/1 -vframes 1 -vsync vfr snapshot.#{type.downcase}"
      end
      unless File.exist? "snapshot.#{type.downcase}"
        raise "ffmpeg call did not produce desired 'snapshot.#{type} file"
      end
      return (File.expand_path "snapshot.#{type.downcase}").to_s
    end
  end

  def compare_images file_reference, percent, time_out, rectangles
    return_hash = { :status => false }
    return_hash[:results] = {}
    if File.exist? file_reference
        new_picture = take_picture "png", rectangles
        percentage_difference = compare_two_images file_reference, new_picture, rectangles
        return_hash[:status] = true
        return_hash[:results].merge! "percentage_difference" => percentage_difference
        puts "#{return_hash}"
        return return_hash
      else
        raise "File reference not found"
    end
  end
    
  def compare_two_images file_one, file_two, areas_to_compare
    puts "Comparing #{file_one} with #{file_two}"
    if (File.exist? file_one) && (File.exist? file_two)
      images = [
        ChunkyPNG::Image.from_file(file_one),
        ChunkyPNG::Image.from_file(file_two)
      ]

      output = ChunkyPNG::Image.new(images.first.width, images.first.height, WHITE)
      diff = []

      if areas_to_compare != nil
        # load the areas to compare from the hashmap object
        puts 'Number of areas to compare ' + areas_to_compare.size.to_s
        count = 1
        total_area_size = 0

        puts "areas_to_compare #{areas_to_compare}"

        areas_to_compare.each_pair do |key, rectangle|
          #TODO: Remove these puts
          puts "i #{rectangle}"
          puts "Area #{count}:"
          puts "            X: #{rectangle[:x]}"
          puts "            Y: #{rectangle[:y]}"
          puts "        Width: #{rectangle[:width]}"
          puts "       Height: #{rectangle[:height]}"
          
          # TODO test if area to check is actually inside the image

                    x_start = rectangle[:x].to_i
          x_stop = rectangle[:x].to_i + rectangle[:width].to_i
          (x_start..x_stop).each do |x|
            y_start = rectangle[:y].to_i
            y_stop = rectangle[:y].to_i + rectangle[:height].to_i
            (y_start..y_stop).each do |y|
              unless images.first[x, y] == images.last[x, y]
                score = Math.sqrt(
                  (r(images.last[x,y]) - r(images.first[x, y])) ** 2 +
                  (g(images.last[x,y]) - g(images.first[x, y])) ** 2 +
                  (b(images.last[x,y]) - b(images.first[x, y])) ** 2
                ) / Math.sqrt(MAX ** 2 * 3)

                output[x, y] = grayscale(MAX - (score * MAX).round)
                diff << score
              end
            end
          end
          output.save('diff.png')
          current_area_size = rectangle[:width].to_i * rectangle[:height].to_i
          total_area_size += rectangle[:width].to_i * rectangle[:height].to_i
          count += 1
        end
        if diff.any?
          percentage_difference = ((diff.inject {|sum, value| sum + value} / total_area_size) * 100).round(2)
          puts "difference = #{percentage_difference}%"
          return percentage_difference
        else
          return 0
        end
      end
    end #if files exist
  end #def
 
  def do_ocr_on_image image_file, search_string, rectangle_hash=nil
    puts image_file
    return_object = Hash.new
    return_object[:status] = true
    return_object[:results] = {}

    if rectangle_hash == nil
      rectangle_hash = Hash.new
      rect_item = {
        x: 0,
        y: 0,
        width: 715,
        height: 570  # TODO get image width and height from image?
      }
      rectangle_hash["item"] = rect_item
    end
    count = 1
    rectangle_hash.each_pair do |key, rectangle|
      image = RTesseract.read(image_file) do |img| 
        x = rectangle[:x].to_i
        y = rectangle[:y].to_i
        width = rectangle[:width].to_i
        height = rectangle[:height].to_i
        img = img.excerpt(x, y, width, height)
        img = img.separate  # grayscale
        img = img.sharpen(0, 0.95)
        img = img.scale(3.0)
        img.write("ocr#{count}.tiff")
        puts "wrote ocr to ocr#{count}.tiff"
        (1..40).each do |i|
          img = img.gaussian_blur(3, 0.5)
          img = img.sharpen(5, 0.64)
        end
        img = img.despeckle
        img.write("ocr_file#{count}.tiff")
      end
      rt = RTesseract.new("ocr_file#{count}.tiff", :lang => "eng", :processor => 'rmagick')
      result = rt.to_s.include? search_string
      puts "ocr actually found #{rt}"
      return_object[:results].merge! "#{key}" => result
      unless result
        return_object[:status] = false
      end
      count += 1
    end
    return return_object
  end

    def ocr_on_image search_string, rectangles=nil
      image_file = take_picture "tiff", rectangles
      object = do_ocr_on_image image_file, search_string, rectangles
      return object
    end
  end

  # deprecated
 # def compare_two_images file_one, file_two
  #   if (File.exist? file_one) && (File.exist? file_two)
  #     images = [
  #       ChunkyPNG::Image.from_file(file_one),
  #       ChunkyPNG::Image.from_file(file_two)
  #     ]
  #     output = ChunkyPNG::Image.new(images.first.width, images.last.width, WHITE)

  #     diff = []

  #     images.first.height.times do |y|
  #       images.first.row(y).each_with_index do |pixel, x|
  #         unless pixel == images.last[x,y]
  #           score = Math.sqrt(
  #             (r(images.last[x,y]) - r(pixel)) ** 2 +
  #             (g(images.last[x,y]) - g(pixel)) ** 2 +
  #             (b(images.last[x,y]) - b(pixel)) ** 2
  #           ) / Math.sqrt(MAX ** 2 * 3)

  #           output[x,y] = grayscale(MAX - (score * MAX).round)
  #           diff << score
  #         end
  #       end
  #     end
  #     output.save('diff.png')
  #     percentage_difference = ((diff.inject {|sum, value| sum + value} / images.first.pixels.length) * 100).round(2)
  #     return percentage_difference
  #   end #if files exist
  # end #def


