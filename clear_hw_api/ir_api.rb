$: << '.'

require 'open3'
require 'rubygems'	
#require 'thread'	
require_relative '../protocols/ruby/ir_protocol'
require_relative '../../clearcore/corelibs/ruby/clear_logger'
require_relative '../../clearcore/corelibs/ruby/tcp_client'
	
include IR_Protocol

class QObject
  attr_reader :ir_slot, :message, :tx_number

  def initialize ir_slot, message, tx_number
    @ir_slot = ir_slot
    @message = message
    @tx_number = tx_number
  end

end

class IRApi

  IR_PORT = 10001
  
  #def log a, b
  #end

  def initialize response_method, irnetbox_ip 
    @response_method = response_method #TODO Add checks to make sure it is a closure->see tcp_server/client.rb
    @irnetbox_ip = irnetbox_ip
    @responded = false
    @connected = false
    @count = 0

    @byte_data = []

    @previous_slot = -1

    @dq = true
    @que = Queue.new
    @count = 0

    setup_transaction_cache
    @response_ticker = []

    Thread.new do
      while @dq
        @count += 1
        if @count > 1000
          log 'clearcommon', 'Nothing to deque'
          @count = 0
        end
        if @que.length > 0
          q_obj = @que.pop
          handle_message q_obj
        else
          sleep 0.01
        end
      end
    end

  end

  def setup_transaction_cache
    @transaction_cache = {}
    @transaction_lookup = {}
    # Add push_button transaction signature to lookup
    # TODO: Implement various lookups for transaction checker
    @transaction_lookup[:enable_slot] = [ SetAllOutputsUsingBitMaskSeperated, CPLDInstruction, AllocateMemoryIRSignal]
    @transaction_lookup[:push_button] = [ DownloadIRSignalData, OutputIRSignalData]
    #TODO: Will add any future transaction signatures here!!
    # Setup empty cache for push_button transaction
    @transaction_cache[:push_button] = []
  end

  def setup      
    log 'clearcommon', 'Setting up connection to IR_NetBox'
    @tcp_client = TCPSocketClientSocket.new
    @tcp_client.connect @irnetbox_ip, IR_PORT, method(:receive_tcp_data), method(:receive_tcp_notification)

    @connected = true
    #Power on the CPLD
    log 'clearcommon', 'Powering on the CPLD'
    power_on = PowerOn.new
    @tcp_client.send common_send power_on.to_binary	
    # TODO: Hook this response back to the food chain to pass information back and worth
    wait_response
    log 'clearcommon', 'Powered on'

    log 'clearcommon', 'Resetting the CPLD'
    # Reset the CPLD
    cpld_instruction = CPLDInstruction.new 
    cpld_instruction.set_code 0x00
    @tcp_client.send common_send cpld_instruction.to_binary
    wait_response
  end

  def enqueue_message ir_slot, message, tx_number      
    @latest_qo = QObject.new(ir_slot, message, tx_number)
    @que << @latest_qo 
    # Add the transaction to the cache...currently only have push_button
    if message.class == DownloadIRSignalData
      @transaction_cache[:push_button] << @latest_qo 
    end
  end


  def handle_message q_obj
    if q_obj.message.msg_type == IRProtocolConst.DOWNLOAD_IR_SIGNAL_DATA
      if @previous_slot == -1 
        @previous_slot = q_obj.ir_slot 
        # Enable port
        enable_port q_obj.ir_slot
      end
      if @previous_slot != q_obj.ir_slot
        @previous_slot = q_obj.ir_slot
        enable_port q_obj.ir_slot
      end
      pressbutton q_obj.message
    end
  end

  def enable_port output_port
    # Enable IR slots	
    log 'clearcommon', 'Enabling the IR slots'
    saoubms =  SetAllOutputsUsingBitMaskSeperated.new

    case output_port 
      when 1
        saoubms.set_states_port1_to4 0b00000011
      when 2
        saoubms.set_states_port1_to4 0b00001100
      when 3
        saoubms.set_states_port1_to4 0b00110000
      when 4
        saoubms.set_states_port1_to4 0b11000000
      when 5 
        saoubms.set_states_port5_to8 0b00000011	
      when 6 
        saoubms.set_states_port5_to8 0b00001100	
      when 7 
        saoubms.set_states_port5_to8 0b00110000	
      when 8 
        saoubms.set_states_port5_to8 0b11000000	
      when 9
        saoubms.set_states_port9_to12 0b00000011
      when 10
        saoubms.set_states_port9_to12 0b00001100
      when 11
        saoubms.set_states_port9_to12 0b00110000
      when 12
        saoubms.set_states_port9_to12 0b11000000
      when 13
        saoubms.set_states_port13_to16 0b00000011
      when 14
        saoubms.set_states_port13_to16 0b00001100
      when 15
        saoubms.set_states_port13_to16 0b00110000
      when 16
        saoubms.set_states_port13_to16 0b11000000
      else	
        log 'clearcommon', 'Invalid IR output port'
    end
    @tcp_client.send common_send saoubms.to_binary
    wait_response
  
    # Set enabled LEDs
    cpld_instruction = CPLDInstruction.new  
    cpld_instruction.set_code 0x17
    @tcp_client.send common_send cpld_instruction.to_binary
    wait_response
  
    allocate_memory = AllocateMemoryIRSignal.new 
    @tcp_client.send common_send allocate_memory.to_binary
    wait_response
  end

  def pressbutton download_ir_signal_data 
    p download_ir_signal_data
    download_ir_signal_data.set_intra_signal_pause 100
    #download_ir_signal_data.set_frequency_timer_count 65376
    #download_ir_signal_data.set_number_of_periods 0
    download_ir_signal_data.set_maximum_number_lengths 0
    #download_ir_signal_data.set_actual_number_lengths 4
    download_ir_signal_data.set_maximum_signal_data_size 0
    download_ir_signal_data.set_number_of_signal_repeats 1 

    log 'clearcommon', "Download IR signal data: #{p download_ir_signal_data.to_binary}"

    @tcp_client.send common_send download_ir_signal_data.to_binary
    wait_response

    output_ir_signal = OutputIRSignalData.new
    @tcp_client.send common_send output_ir_signal.to_binary
    wait_response
  end

  def tear_down
    #Reset the CPLD
    cpld_instruction = CPLDInstruction.new 
    cpld_instruction.set_code 0x00
    @tcp_client.send common_send cpld_instruction.to_binary
    wait_response

    #Power off the CPLD
    log 'clearcommon', 'Powering off the CPLD'
    power_on = PowerOff.new
    @tcp_client.send common_send power_on.to_binary
    log 'clearcommon', 'Powered off'
    wait_response

    @tcp_client.disconnect

    @dq = false

    @que.clear
  end

  def common_send data
    # Add the '#' character infront of outgoing data
    @responded = false
    to_send = [35]
    return to_send + data
  end
	
  def wait_response
    count = 0
    log 'clearcommon', 'Waiting for response...'
    while !@responded && count < 50
      sleep 0.05
      count += 1
    end
  end


  def receive_tcp_data data
    begin
      log 'clearcommon', "Received data: |#{p data}|" 
      data.each_char {|c| @byte_data << c.ord}
      log 'clearcommon', "Buffer byte_data: |#{p @byte_data}|"
      #log "Received data: |#{@byte_data}|"
      if @byte_data.length < 3
        return
      else
        byte_data = []
        if @byte_data.length > 4
          byte_data = @byte_data[0..3]
          @byte_data = @byte_data[4..-1]
        else
          byte_data = @byte_data.clone
          @byte_data = []
        end
        @responded = true
        ir_binary = IRProtocolBinary.new :data => byte_data
        ir_object = ir_binary.to_ir_protocol_object
        if ir_object.class == Error
          log 'clearcommon', "Error Number:#{ir_object.get_code}"
          # Send a NAK with details of @latest_qo tx_number
          @response_method.call :nak, @latest_qo.tx_number
          # Here got through whole cache and remove exact object from correct cache
          @transaction_cache[:push_button].delete @latest_qo
        else
          log 'clearcommon', "Received:#{ir_object.class} :)!"
          @response_ticker << ir_object.class
          test_response_ticker_matches_transaction
        end

      end
    rescue=>e
      log "receive_tcp_data got Excpetion: #{e}"
    end
  end

  def test_response_ticker_matches_transaction
    # Compare the current ticket to each one of the transaction representations in @transaction_cache
    # 
    # TODO: Loop to check ticker length agains all transactions known and do below test for lengths that match 
    #
    #
    if @response_ticker.length >= @transaction_lookup[:push_button].length
      # Get first item of push_button
      matched = []
      remove_in_front_of_this_index = 0
      @response_ticker.each do |val|
        matched << val
        remove_in_front_of_this_index += 1
        if matched.length == @transaction_lookup[:push_button].length && matched == @transaction_lookup[:push_button]
          log 'clearcommon', 'ACK state reached'
          qo_to_ack = @transaction_cache[:push_button].shift
          @response_method.call :ack, qo_to_ack.tx_number
          matched.clear
        else
          if matched.length == @transaction_lookup[:push_button].length
            matched.shift
          end
        end
      end
      @response_ticker.shift(remove_in_front_of_this_index - 1)
    end

  end

  def receive_tcp_notification host, state, message
    log 'clearcommon', "Received notification: #{host}-#{state}-#{message}"
    if state && message == "Connected!"
      log 'clearcommon', message
    end
  end


end



if __FILE__ == $0

  ir_api = IRApi.new nil, "10.17.183.147"
  # Testing code goes here
  ir_api.setup

  #ir_api.enable_port 2
  
  channel_plus_alphabet = []
  "3D851F5A03BD0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| channel_plus_alphabet << number.to_i(16)} # Size in DB 32 bytes
  channel_plus_ir_representation = []
  "000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F".scan(/../).each {|number| channel_plus_ir_representation << number.to_i(16)}

  disd = DownloadIRSignalData.new
  puts "+_+_+_+_+_+_+_+_"
  p channel_plus_ir_representation.length
  disd.set_actual_signal_data_size channel_plus_ir_representation.length
  disd.set_length_and_signal_data channel_plus_alphabet + channel_plus_ir_representation

  ir_api.enqueue_message 2, disd, 1 
  ir_api.enqueue_message 2, disd, 2
  ir_api.enqueue_message 2, disd, 3
  ir_api.enqueue_message 2, disd, 4

  0.upto(10) do
  	sleep 0.1
  end

  ir_api.tear_down

  0.upto(10) do
  	sleep 0.1
  end

end 
