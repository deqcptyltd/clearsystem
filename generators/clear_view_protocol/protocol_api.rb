#!/usr/bin/env ruby

require 'rubygems'
require 'active_support'

class String

    # overwrite a section of self with a new string
    def overwrite(location, string)
        tmp = self
        while tmp.length < location + string.length
            tmp += " "
        end
        out = tmp[0, location] + string
        out += tmp[location + string.length..-1]
    end

    def starts_with(string)
        self[0, string.length] == string
    end

    def underscore
      self.gsub(/::/, '/').
      gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
      gsub(/([a-z\d])([A-Z])/,'\1_\2').
      tr("-", "_").
      downcase
    end    
end

class Tabify
    attr_reader :str
    def initialize(str, tab_length)
        @str = str
        @tab_length = tab_length
        expand_tabs
    end

    # Some shenanigans here to make sure we round up -
    # there may be a better way to do this
    def cells
        ret = @str.length.to_f/@tab_length.to_f
    	return (ret + 1).to_i if ret > ret.to_i
	ret.to_i
    end

    def [](*n)
        if (n[0].class == Range)
            a = [n[0].begin, n[0].end]
                
            # change negatives to end based offsets
            a.map! do |x| 
                if x < 0
                    cells - (x*-1)
                else
                    x
                end
            end
            ret = ""
            (a[0]..a[1]).each {|i| ret += self[i]}
            ret 
        elsif (n.length == 2)
            ret = ""
            n[1].times {|i| ret += self[n[0]+i]}
            ret
        elsif (n.length == 1)
            @str[n[0]*@tab_length, @tab_length] || " "*@tab_length
        end	
    end

private
    def expand_tabs
        exp = ""
        @str.each_byte do |b|
            c = b.chr
            if c == "\t"
                exp += " "
                while (exp.length % @tab_length != 0)
                    exp += " "
                end
            else
                exp += c
            end
        end
        @str = exp
    end
end

class Tag
    attr_reader :name, :type, :xml_name
    attr_accessor :description

    def initialize(name, type, xml_name, description)
        @name = name; @type = type; @xml_name = xml_name; @description = description
				@width = 0
       
        @format_map = {"U8" => "b", "U16" => "W", "U32" => "L", 
                       "S8" => "b", "S16" => "w", "S32" => "l",
                       "STRING" => "S", "TIMESTAMP" => "T", "DATA" => "D", "CT" => "", "X" => "x"}

        @csharp_map = {"U8" => "Byte", "U16" => "UInt16", "U32" => "UInt32", 
                       "S8" => "Byte", "S16" => "Int16", "S32" => "Int32",
                       "STRING" => "String", "TIMESTAMP" => "DateTime",  "DATA" => "Byte[]", "CT" => "String", "X" => "x"}
       
        @c_map = {"U8" => "uchar", "U16" => "uint16", "U32" => "uint32", 
                       "S8" => "char", "S16" => "int16", "S32" => "int32",
                       "STRING" => "uchar", "POSITION" => "Position", 
                       "TIMESTAMP" => "epoch", "DATA" => "char*", "X" => "x"}

        @csharp_def_map = {"U8" => "0", "U16" => "0", "U32" => "0", 
                       "S8" => "0", "S16" => "0", "S32" => "0",
                       "STRING" => 'string.Empty', "TIMESTAMP" => "DateTime.Now",  "DATA" => "new Byte[]{}", "CT" => @xml_name, "X" => "x"}
				
	    @ruby_def_map = {"U8" => "0", "U16" => "0", "U32" => "0", 
                       "S8" => "0", "S16" => "0", "S32" => "0",
                       "STRING" => "''", "TIMESTAMP" => "Time.Now", "DATA" => "[]", "X" => "x"}

        @python_def_map = {"U8" => "0", "U16" => "0", "U32" => "0", 
                       "S8" => "0", "S16" => "0", "S32" => "0",
                       "STRING" => "''", "TIMESTAMP" => "time.Now", "DATA" => "[]", "X" => "x"}    
  
	    @btd_object_map = {"U8" => "ClearProtocolByte", "U16" => "ClearProtocolUWord", "U32" => "ClearProtocolULong", 
                       "S8" => "StreamingProtocolByte", "S16" => "StreamingProtocolWord", "S32" => "ClearProtocolLong",
                       "STRING" => "StreamingProtocolString", "TIMESTAMP" => "StreamingProtocolDate", "DATA" => "ClearProtocolData", "X" => "x"}

        #Detect STRING fields with width appended
    
        if @type.starts_with("STRING") && @type.length > 6
            @width = @type[6..-1].to_i
            @type = "STRING"
        end

    	  if !@format_map.keys.include?(@type) && @type != "CT"            
            raise "Datatype '#{@type}' is not valid when creating tag"
        end 
    end

    #one-line description
    def desc
        description.gsub("\n", " ")
    end

    def to_xml_name
	xml_name.gsub("\n", " ")
    end

    #name without spaces
    def field_name
 	name.gsub(" ", "")
    end

    #name without spaces, first letter small
    def c_field_name
        field_name[0, 1].downcase + field_name[1..-1]
    end

    #type format string
    def to_format
        format_char = @format_map[@type] || "?"
        format_char += @width.to_s if @width != 0
        format_char
    end

    #C native data types
    def to_c_type
        @c_map[@type] || "?"
    end

    #width of data types which have widths, in [], or nothing
    def to_c_width
        return "" if @width == 0
        return "[#{@width}]"
    end

    #C# native data types
    def to_csharp_type
        @csharp_map[@type] || "?"
    end			

    def to_csharp_def
        @csharp_def_map[@type] || "?"
    end

    #Ruby native data types
    def to_ruby_def
	@ruby_def_map[@type] || "?"
    end

    #Python native data types
    def to_python_def
	@python_def_map[@type] || "?"
    end

    #BtdObject data types
    def to_btd_object_type
        @btd_object_map[@type] || "?"
    end
end

class Type
    attr_reader :region, :type, :name, :category, :command_type, :tags
    def initialize(name, category, command_type, region)
        @name = name
        @category = category
        @command_type = command_type
		    @region = region
        @tags = []
	      @last_tag_added = nil
    end

    def add_tag(name, type, xml_name, description)
	begin
		tag = Tag.new(name, type, xml_name, description)
	rescue => e
		puts "Adding field with Name: '#{name}' Type: '#{type}' XML Name: '#{xml_name} ' Description: '#{description}'"
		puts e.message
		puts
		exit(1)
	end
	@tags << tag
	@last_tag_added = tag
    end 

    def add_to_last_tag_description(s)
	if (@last_tag_added)
            @last_tag_added.description += "\n" + s
        end	    
    end

    def to_format
        format = ""
        max = 0
        @tags.each {|t| format << t.to_format}

	#If the format string is empty, we need to be specific about it, so incomplete entries
	#can be differentiated from entries with no tags
	return "-" if format.length == 0
        format
    end
end

class ProtocolDoc
    attr_reader :types, :comments
    def initialize(filename)
        @types = []               #store types
        @tag_regexp = /^\w+.*(BIT|U8|U16|U32|S8|S16|S32|STRING|DATA|TIMESTAMP|CT)/
        read(filename)
    end

private

    def read(filename)     
	current_region = nil
        current_type = nil
	line_num = 0
        File.new(filename).each_line do |l|
	    line_num += 1
            next if l =~ /^\s*#/ 
            next if l =~ /^\s*$/ 

            t = Tabify.new(l.rstrip, 8)
            
            #look for an btd start
	    if (t.str =~ %r{^\s*\[R\] (.*)})
		current_region = $1.strip
            elsif (t.str =~ /^\s*\[T\] (\d+) (\d+) (.*)/)                #eg. [T] 1 1 WhateverMessage
                category = $1.strip
                command_type = $2.strip
                name = $3.strip
                current_type = Type.new(name, category, command_type, current_region)
	        @types << current_type  
            elsif (current_type && t.str =~ @tag_regexp)  #tag entry
                name, type, xml_name, desc = t[0,3].strip, t[3,2].strip, t[5,2].strip, t[7..-1].strip
                current_type.add_tag(name, type, xml_name, desc)                                               
            elsif t.cells > 7                                #long enough to be a tag + desc
                if (t[0..5].strip.length == 0 && t[7..-1].strip.length != 0 && current_type)
		    current_type.add_to_last_tag_description(t[7..-1].strip)
                end
            else
		puts "WARNING, LINE #{line_num} UNRECOGNISED:"
		puts l
	    end
        end
    end
end

if ($0 == __FILE__) 
    ud = ProtocolDoc.new("clear_view_protocol.txt")
    
    ud.types.each do |t|
        next if !t
        puts "Region: #{t.region}, Name: #{t.name}, format: #{t.to_format}"
        t.tags.each do |tag|
            puts "  tag #{tag.c_field_name}, type #{tag.to_c_type}#{tag.to_c_width}"
            #tag.description.split("\n").each {|l| puts "     " + l.strip}
        end
        puts
    end
    require 'yaml'
    File.open("clear_protocol.yaml", "w"){|f| YAML.dump(ud, f)}
end


