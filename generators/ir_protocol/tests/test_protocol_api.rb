#!/usr/bin/env ruby
$LOAD_PATH << '.'
require 'protocol_api'
require 'test/unit'

class TestString < Test::Unit::TestCase
    def test_overwrite
        assert_equal("thisabout here", "this test here".overwrite(4, "about"))
        assert_equal("this test heabout", "this test here".overwrite(12, "about"))
        assert_equal("blah", "".overwrite(0, "blah"))
        assert_equal("    blah", "".overwrite(4, "blah"))
    end

    def test_starts_with
        assert("this is cool".starts_with("this"))
        assert("this is cool".starts_with("this is cool"))
    end
end

class TestTabify < Test::Unit::TestCase
    def setup
        @t = Tabify.new("one two threfour", 4)
    end

    def test_cells
        assert_equal(4, @t.cells)
    end

    def test_index
        assert_equal("one ", @t[0])
        assert_equal("two ", @t[1])
        assert_equal("thre", @t[2])
        assert_equal("four", @t[3])
        
        assert_equal("one two ", @t[0..1])
        assert_equal("one two threfour", @t[0..3])
        assert_equal("two thre", @t[1..2])
        
        assert_equal("one two threfour", @t[0..-1])
        assert_equal("two threfour", @t[1..-1])
        assert_equal("threfour", @t[2..-1])
        assert_equal("four", @t[3..-1])
        assert_equal("two thre", @t[1..-2])

        assert_equal("one two ", @t[0, 2])
        assert_equal("one two threfour", @t[0, 4])
        assert_equal("two thre", @t[1, 2])
    end
end

