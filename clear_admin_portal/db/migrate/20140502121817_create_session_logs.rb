class CreateSessionLogs < ActiveRecord::Migration
  def change
    create_table :session_logs do |t|
      t.string :start
      t.string :end
      t.string :type
      t.integer :slot

      t.timestamps
    end
  end
end
