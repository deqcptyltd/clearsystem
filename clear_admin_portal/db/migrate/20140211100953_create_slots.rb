class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.integer :slot_number
      t.integer :node_id
      t.integer :ir_netbox_id
      t.integer :ir_slot
      t.string :input_codec
      t.string :size
      t.string :input_format
      t.string :video_device
      t.string :output_codec
      t.string :output_format
      t.integer :bitrate
      t.integer :framerate
      t.string :output_url

      t.timestamps
    end
  end
end
