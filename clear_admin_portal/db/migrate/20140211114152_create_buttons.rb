class CreateButtons < ActiveRecord::Migration
  def change
    create_table :buttons do |t|
      t.string :alphabet
      t.string :ir_representation
      t.string :name
      t.integer :button_command

      t.timestamps
    end
  end
end
