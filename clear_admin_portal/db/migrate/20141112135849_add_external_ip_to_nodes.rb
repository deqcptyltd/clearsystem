class AddExternalIpToNodes < ActiveRecord::Migration
  def change
    add_column :nodes, :external_ip, :string
  end
end
