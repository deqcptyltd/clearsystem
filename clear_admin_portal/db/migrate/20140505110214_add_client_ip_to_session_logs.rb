class AddClientIpToSessionLogs < ActiveRecord::Migration
  def change
    add_column :session_logs, :client_ip, :string
  end
end
