class CreateRemotes < ActiveRecord::Migration
  def change
    create_table :remotes do |t|
      t.string :name
      t.string :model
      t.string :image_url
      t.string :type

      t.timestamps
    end
  end
end
