class CreateNodeTypes < ActiveRecord::Migration
  def change
    create_table :node_types do |t|
      t.string :type
      t.integer :node_id

      t.timestamps
    end
  end
end
