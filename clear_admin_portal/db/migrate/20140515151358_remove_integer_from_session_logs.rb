class RemoveIntegerFromSessionLogs < ActiveRecord::Migration
  def up
    remove_column :session_logs, :integer
  end

  def down
    add_column :session_logs, :integer, :string
  end
end
