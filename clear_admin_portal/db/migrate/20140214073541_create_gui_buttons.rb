class CreateGuiButtons < ActiveRecord::Migration
  def change
    create_table :gui_buttons do |t|
      t.string :image_url
      t.integer :x
      t.integer :y
      t.integer :width
      t.integer :height
      t.integer :button_id
      t.integer :remote_id

      t.timestamps
    end
  end
end
