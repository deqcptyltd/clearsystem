class ChangeTypeToNodeType < ActiveRecord::Migration
  def up
  	rename_column :node_types, :type, :node_type
  end

  def down
  	rename_column :node_types, :node_type, :node
  end
end
