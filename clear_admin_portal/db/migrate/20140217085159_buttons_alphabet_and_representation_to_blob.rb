class ButtonsAlphabetAndRepresentationToBlob < ActiveRecord::Migration
  def up
  	change_column :buttons, :alphabet, :binary, :limit => 32.byte
  	change_column :buttons, :ir_representation, :binary, :limit => 256.byte
  end

  def down
  	change_column :buttons, :alphabet, :string
  	change_column :buttons, :ir_representation, :string
  end
end
