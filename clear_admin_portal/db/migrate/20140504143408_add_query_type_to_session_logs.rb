class AddQueryTypeToSessionLogs < ActiveRecord::Migration
  def change
    add_column :session_logs, :query_type, :string
  end
end
