class FixColumnNames < ActiveRecord::Migration
  def up
    rename_column :session_logs, :end, :session_end
  end

  def down
  end
end
