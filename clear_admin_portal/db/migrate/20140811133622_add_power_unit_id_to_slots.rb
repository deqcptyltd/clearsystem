class AddPowerUnitIdToSlots < ActiveRecord::Migration
  def change
    add_column :slots, :power_unit_id, :integer
  end
end
