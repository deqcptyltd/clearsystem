class AddAlphabetLengthToButtons < ActiveRecord::Migration
  def change
    add_column :buttons, :alphabet_length, :integer
  end
end
