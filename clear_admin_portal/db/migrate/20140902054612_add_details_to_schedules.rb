class AddDetailsToSchedules < ActiveRecord::Migration
  def change
    add_column :schedules, :repo, :string
    add_column :schedules, :branch, :string
    add_column :schedules, :xml, :string
    add_column :schedules, :product, :string
    add_column :schedules, :repeats, :integer
    add_column :schedules, :repeat_end, :date
  end
end
