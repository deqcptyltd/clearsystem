class RemoveXtraviewSlotFromSessionLogs < ActiveRecord::Migration
  def up
    remove_column :session_logs, :xtraview_slot
  end

  def down
    add_column :session_logs, :xtraview_slot, :string
  end
end
