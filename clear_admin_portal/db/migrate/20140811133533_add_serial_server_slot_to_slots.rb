class AddSerialServerSlotToSlots < ActiveRecord::Migration
  def change
    add_column :slots, :serial_server_slot, :integer
  end
end
