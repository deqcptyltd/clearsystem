class AddPowerUnitSlotToSlots < ActiveRecord::Migration
  def change
    add_column :slots, :power_unit_slot, :integer
  end
end
