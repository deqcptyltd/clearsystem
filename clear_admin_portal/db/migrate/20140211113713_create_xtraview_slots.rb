class CreateXtraviewSlots < ActiveRecord::Migration
  def change
    create_table :xtraview_slots do |t|
      t.integer :primary_slot_id
      t.integer :secondary_slot_id

      t.timestamps
    end
  end
end
