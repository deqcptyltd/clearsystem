class AddXtraviewSlotToSessionLogs < ActiveRecord::Migration
  def change
    add_column :session_logs, :xtraview_slot, :integer
  end
end
