# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create!([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create!(name: 'Emanuel', city: cities.first)
#

Node.delete_all
nodeA = Node.create!(:name => 'A', :ip => '10.17.183.17', :slot_count => 32, :ir_netbox_count=>1)
nodeB = Node.create!(:name => 'B', :ip => '10.17.183.21', :slot_count => 32, :ir_netbox_count=>2)

IrNetbox.delete_all
netbox_1 = IrNetbox.create!(:ip => '10.17.183.11', :node_id => nodeA.id, :slot_count => 16 , :port => 10001)
netbox_2 = IrNetbox.create!(:ip => '10.17.183.14', :node_id => nodeA.id, :slot_count => 16 , :port => 10001)
netbox_3 = IrNetbox.create!(:ip => '10.17.183.19', :node_id => nodeB.id, :slot_count => 16 , :port => 10001)
netbox_4 = IrNetbox.create!(:ip => '10.17.183.20', :node_id => nodeB.id, :slot_count => 16 , :port => 10001)

Slot.delete_all 
# Rack 1
slot_1 =  Slot.create!(:slot_number => 1,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 1,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video0',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_2 =  Slot.create!(:slot_number => 2,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 2,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video1',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_3 =  Slot.create!(:slot_number => 3,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 3,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video2',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_4 =  Slot.create!(:slot_number => 4,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 4,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video3',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_5 =  Slot.create!(:slot_number => 5,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 5,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video12', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_6 =  Slot.create!(:slot_number => 6,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 6,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video13', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_7 =  Slot.create!(:slot_number => 7,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 7,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video14', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_8 =  Slot.create!(:slot_number => 8,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 8,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video16', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_9 =  Slot.create!(:slot_number => 9,   :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 9,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video24', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_10 = Slot.create!(:slot_number => 10,  :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 10, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video25', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_11 = Slot.create!(:slot_number => 11,  :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 11, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video26', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_12 = Slot.create!(:slot_number => 12,  :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 12, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video27', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_13 = Slot.create!(:slot_number => 13,  :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 13, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video36', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_14 = Slot.create!(:slot_number => 14,  :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 14, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video37', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_15 = Slot.create!(:slot_number => 15,  :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 15, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video38', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_16 = Slot.create!(:slot_number => 16,  :node_id => nodeA.id, :ir_netbox_id => netbox_1.id, :ir_slot => 16, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video39', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')

slot_17 = Slot.create!(:slot_number => 17,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 1,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video48', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_18 = Slot.create!(:slot_number => 18,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 2,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video49', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_19 = Slot.create!(:slot_number => 19,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 3,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video50', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_20 = Slot.create!(:slot_number => 20,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 4,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video51', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_21 = Slot.create!(:slot_number => 21,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 5,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video60', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_22 = Slot.create!(:slot_number => 22,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 6,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video61', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_23 = Slot.create!(:slot_number => 23,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 7,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video62', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_24 = Slot.create!(:slot_number => 24,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 8,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video63', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_25 = Slot.create!(:slot_number => 25,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 9,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video72', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_26 = Slot.create!(:slot_number => 26,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 10, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video73', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_27 = Slot.create!(:slot_number => 27,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 11, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video74', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_28 = Slot.create!(:slot_number => 28,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 12, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video75', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_29 = Slot.create!(:slot_number => 29,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 13, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video84', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_30 = Slot.create!(:slot_number => 30,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 14, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video85', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_31 = Slot.create!(:slot_number => 31,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 15, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video86', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_32 = Slot.create!(:slot_number => 32,  :node_id => nodeA.id, :ir_netbox_id => netbox_2.id, :ir_slot => 16, :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video87', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')

# Rack 2
slot_33 = Slot.create!(:slot_number => 33, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 1,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video0',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_34 = Slot.create!(:slot_number => 34, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 2,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video1',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_35 = Slot.create!(:slot_number => 35, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 3,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video2',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_36 = Slot.create!(:slot_number => 36, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 4,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video3',  :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_37 = Slot.create!(:slot_number => 37, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 5,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video12', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_38 = Slot.create!(:slot_number => 38, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 6,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video13', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_39 = Slot.create!(:slot_number => 39, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 7,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video14', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_40 = Slot.create!(:slot_number => 40, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 8,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video16', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_41 = Slot.create!(:slot_number => 41, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 9,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video24', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_42 = Slot.create!(:slot_number => 42, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 10,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video25', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_43 = Slot.create!(:slot_number => 43, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 11,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video26', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_44 = Slot.create!(:slot_number => 44, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 12,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video27', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_45 = Slot.create!(:slot_number => 45, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 13,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video36', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_46 = Slot.create!(:slot_number => 46, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 14,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video37', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_47 = Slot.create!(:slot_number => 47, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 15,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video38', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_48 = Slot.create!(:slot_number => 48, :node_id => nodeB.id, :ir_netbox_id => netbox_3.id, :ir_slot => 16,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video39', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')

slot_49 = Slot.create!(:slot_number => 49, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 1,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video48', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_50 = Slot.create!(:slot_number => 50, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 2,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video49', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_51 = Slot.create!(:slot_number => 51, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 3,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video50', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_52 = Slot.create!(:slot_number => 52, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 4,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video51', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_53 = Slot.create!(:slot_number => 53, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 5,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video60', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_54 = Slot.create!(:slot_number => 54, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 6,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video61', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_55 = Slot.create!(:slot_number => 55, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 7,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video62', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_56 = Slot.create!(:slot_number => 56, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 8,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video63', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_57 = Slot.create!(:slot_number => 57, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 9,   :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video72', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_58 = Slot.create!(:slot_number => 58, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 10,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video73', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_59 = Slot.create!(:slot_number => 59, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 11,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video74', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_60 = Slot.create!(:slot_number => 60, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 12,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video75', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_61 = Slot.create!(:slot_number => 61, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 13,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video84', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_62 = Slot.create!(:slot_number => 62, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 14,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video85', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_63 = Slot.create!(:slot_number => 63, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 15,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video86', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')
slot_64 = Slot.create!(:slot_number => 64, :node_id => nodeB.id, :ir_netbox_id => netbox_4.id, :ir_slot => 16,  :input_codec => 'rawvideo', :size => '720x576', :input_format => 'video4linux2', :video_device => '/dev/video87', :output_codec => 'libx264', :output_format => 'mpegts', :bitrate => 768, :framerate => 25, :output_url => '239.1.1.1')

NodeType.delete_all
NodeType.create!(:node_id => 1, :node_type => 'master')
NodeType.create!(:node_id => 2, :node_type => 'slave')

Remote.delete_all
remote_1 =Remote.create!(:image_url => "/assets/nextgen_remote.png",    :model => 'explora',        :name => "explora",  remote_type: "non-XMPP" )
remote_2 =Remote.create!(:image_url => "/assets/pvr_remote_1.png",      :model => 'pvr_green',      :name => "pvr_green",    remote_type: "XMPP" )
remote_3 =Remote.create!(:image_url => "/assets/pvr_remote_2.png",      :model => 'pvr_brown',      :name => "pvr_brown",    remote_type: "non-XMPP" )
remote_4 =Remote.create!(:image_url => "/assets/zapper_green.png",      :model => 'zapper_green',   :name => "zapper_green", remote_type: "non-XMPP" )
remote_5 =Remote.create!(:image_url => "/assets/zapper_remote_1.png",   :model => '1132',           :name => "1132", remote_type: "zapper_remote_2" )
remote_6 =Remote.create!(:image_url => "/assets/720_remote.png",        :model => '720',            :name => "720",      remote_type: "non-XMPP" )
remote_7 =Remote.create!(:image_url => "/assets/zapper_remote_2.png",   :model => '113x',           :name => "720_remote",      remote_type: "XMPP" )

Decoder.delete_all
# UEC HD 4 Tuner
Decoder.create!(:back_image_url => "/assets/uec_hd_4_tuner_back.png", :front_image_url => "/assets/uec_hd_4_tuner_front.png", :manufacturer => 'Multichoice', model: 'UEC HD 4 Tuner',  name: 'UEC HD 4 Tuner', :remote_id => remote_2.id, :secure_csn => 123456782, :serial => 'U900 028 715', :slot_id => slot_2.id,    :smartcard => 41292616343  )
Decoder.create!(:back_image_url => "/assets/uec_hd_4_tuner_back.png", :front_image_url => "/assets/uec_hd_4_tuner_front.png", :manufacturer => 'Multichoice', model: 'UEC HD 4 Tuner',  name: 'UEC HD 4 Tuner', :remote_id => remote_2.id, :secure_csn => 123456783, :serial => 'U900 047 565', :slot_id => slot_3.id,    :smartcard => 41292616368  )
# Pace 4 Tuner
Decoder.create!(:back_image_url => "/assets/pace_4_tuner_back.png",   :front_image_url => "/assets/pace_4_tuner_front.png",   :manufacturer => 'Multichoice', model: 'Pace 4 Tuner',    name: 'Pace 4 Tuner',   :remote_id => remote_2.id, :secure_csn => 269550005, :serial => 'P300 016 344', :slot_id => slot_4.id,    :smartcard => 10078364014  )
Decoder.create!(:back_image_url => "/assets/pace_4_tuner_back.png",   :front_image_url => "/assets/pace_4_tuner_front.png",   :manufacturer => 'Multichoice', model: 'Pace 4 Tuner',    name: 'Pace 4 Tuner',   :remote_id => remote_2.id, :secure_csn => 269835564, :serial => 'P300 061 418', :slot_id => slot_5.id,    :smartcard => 10078363974  )
# Explora
Decoder.create!(:back_image_url => "/assets/explora_back.png",        :front_image_url => "/assets/explora_front.png",        :manufacturer => 'Multichoice', model: 'Explora',         name: 'Explora',        :remote_id => remote_1.id, :secure_csn => 299886986, :serial => 'P600 121 050', :slot_id => slot_1.id,    :smartcard => 10037000345  )
Decoder.create!(:back_image_url => "/assets/explora_back.png",        :front_image_url => "/assets/explora_front.png",        :manufacturer => 'Multichoice', model: 'Explora',         name: 'Explora',        :remote_id => remote_1.id, :secure_csn => 299891466, :serial => 'P600 121 058', :slot_id => slot_6.id,    :smartcard => 10037000337  )
Decoder.create!(:back_image_url => "/assets/explora_back.png",        :front_image_url => "/assets/explora_front.png",        :manufacturer => 'Multichoice', model: 'Explora',         name: 'Explora',        :remote_id => remote_1.id, :secure_csn => 299891323, :serial => 'P600 121 091', :slot_id => slot_7.id,    :smartcard => 10037000352  )
Decoder.create!(:back_image_url => "/assets/explora_back.png",        :front_image_url => "/assets/explora_front.png",        :manufacturer => 'Multichoice', model: 'Explora',         name: 'Explora',        :remote_id => remote_1.id, :secure_csn => 299887135, :serial => 'P600 121 406', :slot_id => slot_8.id,    :smartcard => 10037000360  )
Decoder.create!(:back_image_url => "/assets/explora_back.png",        :front_image_url => "/assets/explora_front.png",        :manufacturer => 'Multichoice', model: 'Explora',         name: 'Explora',        :remote_id => remote_1.id, :secure_csn => 299867388, :serial => 'P600 093 009', :slot_id => slot_9.id,    :smartcard => 42905269355  )
Decoder.create!(:back_image_url => "/assets/explora_back.png",        :front_image_url => "/assets/explora_front.png",        :manufacturer => 'Multichoice', model: 'Explora',         name: 'Explora',        :remote_id => remote_1.id, :secure_csn => 299890105, :serial => 'P600 121 407', :slot_id => slot_10.id,   :smartcard => 10037000329  )
# Pace HD PVR 2P
Decoder.create!(:back_image_url => "/assets/pace_hd_pvr_2p_back.png", :front_image_url => "/assets/pace_hd_pvr_2p_front.png", :manufacturer => 'Multichoice', model: 'Pace HD PVR 2P',  name: 'Pace HD PVR 2P', :remote_id => remote_2.id, :secure_csn => 123456790, :serial => 'P400 865 639', :slot_id => slot_11.id,   :smartcard => 42873817136  )
Decoder.create!(:back_image_url => "/assets/pace_hd_pvr_2p_back.png", :front_image_url => "/assets/pace_hd_pvr_2p_front.png", :manufacturer => 'Multichoice', model: 'Pace HD PVR 2P',  name: 'Pace HD PVR 2P', :remote_id => remote_2.id, :secure_csn => 223456790, :serial => 'P400 866 556', :slot_id => slot_14.id,   :smartcard => 42873913679  )
Decoder.create!(:back_image_url => "/assets/pace_hd_pvr_2p_back.png", :front_image_url => "/assets/pace_hd_pvr_2p_front.png", :manufacturer => 'Multichoice', model: 'Pace HD PVR 2P',  name: 'Pace HD PVR 2P', :remote_id => remote_2.id, :secure_csn => 323456790, :serial => 'P400 866 558', :slot_id => slot_15.id,   :smartcard => 42873816997  )
Decoder.create!(:back_image_url => "/assets/pace_hd_pvr_2p_back.png", :front_image_url => "/assets/pace_hd_pvr_2p_front.png", :manufacturer => 'Multichoice', model: 'Pace HD PVR 2P',  name: 'Pace HD PVR 2P', :remote_id => remote_2.id, :secure_csn => 423456790, :serial => 'P400 866 564', :slot_id => slot_28.id,   :smartcard => 42873913687  )
Decoder.create!(:back_image_url => "/assets/pace_hd_pvr_2p_back.png", :front_image_url => "/assets/pace_hd_pvr_2p_front.png", :manufacturer => 'Multichoice', model: 'Pace HD PVR 2P',  name: 'Pace HD PVR 2P', :remote_id => remote_2.id, :secure_csn => 523456790, :serial => 'P400 866 562', :slot_id => slot_40.id,   :smartcard => 42873816955  )
Decoder.create!(:back_image_url => "/assets/pace_hd_pvr_2p_back.png", :front_image_url => "/assets/pace_hd_pvr_2p_front.png", :manufacturer => 'Multichoice', model: 'Pace HD PVR 2P',  name: 'Pace HD PVR 2P', :remote_id => remote_2.id, :secure_csn => 723456790, :serial => 'P400 866 366', :slot_id => slot_45.id,   :smartcard => 42873913653  )
Decoder.create!(:back_image_url => "/assets/pace_hd_pvr_2p_back.png", :front_image_url => "/assets/pace_hd_pvr_2p_front.png", :manufacturer => 'Multichoice', model: 'Pace HD PVR 2P',  name: 'Pace HD PVR 2P', :remote_id => remote_2.id, :secure_csn => 823456790, :serial => 'P400 866 568', :slot_id => slot_56.id,   :smartcard => 42873816948  )
# SD PVR DVR 3000
Decoder.create!(:back_image_url => "/assets/dualview_back.png",      :front_image_url => "/assets/dualview_front.png",        :manufacturer => 'Multichoice', model: 'SD PVR',          name: 'SD PVR',         :remote_id => remote_2.id, :secure_csn => 123456791, :serial => 'X000 759 124', :slot_id => slot_12.id,   :smartcard => 10078363867  )
Decoder.create!(:back_image_url => "/assets/dualview_back.png",      :front_image_url => "/assets/dualview_front.png",        :manufacturer => 'Multichoice', model: 'SD PVR',          name: 'SD PVR',         :remote_id => remote_2.id, :secure_csn => 223456791, :serial => 'X000 755 928', :slot_id => slot_13.id,   :smartcard => 10078363875  )
Decoder.create!(:back_image_url => "/assets/dualview_back.png",      :front_image_url => "/assets/dualview_front.png",        :manufacturer => 'Multichoice', model: 'SD PVR',          name: 'SD PVR',         :remote_id => remote_2.id, :secure_csn => 323456791, :serial => 'X000 755 959', :slot_id => slot_19.id,   :smartcard => 10078363909  )
Decoder.create!(:back_image_url => "/assets/dualview_back.png",      :front_image_url => "/assets/dualview_front.png",        :manufacturer => 'Multichoice', model: 'SD PVR',          name: 'SD PVR',         :remote_id => remote_2.id, :secure_csn => 423456791, :serial => 'X000 759 354', :slot_id => slot_33.id,   :smartcard => 10078363966  )

# HD PVR 2U
Decoder.create!(:back_image_url => "/assets/hd_pvr_2u_back.png",      :front_image_url => "/assets/hd_pvr_2u_front.png",      :manufacturer => 'Multichoice', model: 'HD PVR 2U',       name: 'HD PVR 2U',      :remote_id => remote_3.id, :secure_csn => 623456791, :serial => 'V300 045 921', :slot_id => slot_23.id,   :smartcard => 42584077616  )
Decoder.create!(:back_image_url => "/assets/hd_pvr_2u_back.png",      :front_image_url => "/assets/hd_pvr_2u_front.png",      :manufacturer => 'Multichoice', model: 'HD PVR 2U',       name: 'HD PVR 2U',      :remote_id => remote_3.id, :secure_csn => 523456791, :serial => 'V300 054 755', :slot_id => slot_25.id,   :smartcard => 42819590995  )
# 720i
Decoder.create!(:back_image_url => "/assets/720i_back.png",           :front_image_url => "/assets/720i_front.png",           :manufacturer => 'Multichoice', model: '720i',            name: '720i',           :remote_id => remote_6.id, :secure_csn => 323456792, :serial => 'U100 777 847', :slot_id => slot_47.id,   :smartcard => 10078363792  )
Decoder.create!(:back_image_url => "/assets/720i_back.png",           :front_image_url => "/assets/720i_front.png",           :manufacturer => 'Multichoice', model: '720i',            name: '720i',           :remote_id => remote_6.id, :secure_csn => 123456792, :serial => 'U101 388 846', :slot_id => slot_48.id,   :smartcard => 10078363982  )

# 910
Decoder.create!(:back_image_url => "/assets/910_back.png",            :front_image_url => "/assets/910_front.png",            :manufacturer => 'Multichoice', model: '910',             name: '910',            :remote_id => remote_4.id, :secure_csn => 123456793, :serial => 'U300 386 265', :slot_id => slot_49.id,  :smartcard => 10078363727  )

# 990
Decoder.create!(:back_image_url => "/assets/990_back.png",            :front_image_url => "/assets/990_front.png",            :manufacturer => 'Multichoice', model: '990',             name: '990',            :remote_id => remote_4.id, :secure_csn => 123456794, :serial => 'U501 070 715', :slot_id => slot_64.id,  :smartcard => 10078363826  )

# Dualview
Decoder.create!(:back_image_url => "/assets/dualview_back.png",       :front_image_url => "/assets/dualview_front.png",       :manufacturer => 'Multichoice', model: 'Dualview',        name: 'Dualview',       :remote_id => remote_7.id, :secure_csn => 123456795, :serial => 'V000 299 148', :slot_id => slot_29.id,  :smartcard => 10078363958  )
Decoder.create!(:back_image_url => "/assets/dualview_back.png",       :front_image_url => "/assets/dualview_front.png",       :manufacturer => 'Multichoice', model: 'Dualview tv2',    name: 'Dualview tv2',   :remote_id => remote_7.id, :secure_csn => 223456795, :serial => 'V_00 299 148', :slot_id => slot_30.id,  :smartcard => 20078363958  )
Decoder.create!(:back_image_url => "/assets/dualview_back.png",       :front_image_url => "/assets/dualview_front.png",       :manufacturer => 'Multichoice', model: 'Dualview',        name: 'Dualview',       :remote_id => remote_7.id, :secure_csn => 323456795, :serial => 'V000 168 784', :slot_id => slot_31.id,  :smartcard => 10078363735  )
Decoder.create!(:back_image_url => "/assets/dualview_back.png",       :front_image_url => "/assets/dualview_front.png",       :manufacturer => 'Multichoice', model: 'Dualview tv2',    name: 'Dualview tv2',   :remote_id => remote_7.id, :secure_csn => 423456795, :serial => 'V_00 168 784', :slot_id => slot_32.id,  :smartcard => 20078363735  )
# 1110
Decoder.create!(:back_image_url => "/assets/1110_back.png",           :front_image_url => "/assets/1110_front.png",           :manufacturer => 'Multichoice', model: '1110',            name: '1110',           :remote_id => remote_4.id, :secure_csn => 123456796, :serial => 'U601 611 094', :slot_id => slot_16.id,  :smartcard => 10078363883  )
Decoder.create!(:back_image_url => "/assets/1110_back.png",           :front_image_url => "/assets/1110_front.png",           :manufacturer => 'Multichoice', model: '1110',            name: '1110',           :remote_id => remote_4.id, :secure_csn => 223456796, :serial => 'U601 160 256', :slot_id => slot_18.id,  :smartcard => 10078363891  )
Decoder.create!(:back_image_url => "/assets/1110_back.png",           :front_image_url => "/assets/1110_front.png",           :manufacturer => 'Multichoice', model: '1110',            name: '1110',           :remote_id => remote_4.id, :secure_csn => 323456796, :serial => 'U602 142 404', :slot_id => slot_20.id,  :smartcard => 10078363917  )
Decoder.create!(:back_image_url => "/assets/1110_back.png",           :front_image_url => "/assets/1110_front.png",           :manufacturer => 'Multichoice', model: '1110',            name: '1110',           :remote_id => remote_4.id, :secure_csn => 423456796, :serial => 'U601 387 948', :slot_id => slot_24.id,  :smartcard => 10078364022  )
Decoder.create!(:back_image_url => "/assets/1110_back.png",           :front_image_url => "/assets/1110_front.png",           :manufacturer => 'Multichoice', model: '1110',            name: '1110',           :remote_id => remote_4.id, :secure_csn => 623456796, :serial => 'U600 412 717', :slot_id => slot_46.id,  :smartcard => 10078363858  )
Decoder.create!(:back_image_url => "/assets/1110_back.png",           :front_image_url => "/assets/1110_front.png",           :manufacturer => 'Multichoice', model: '1110',            name: '1110',           :remote_id => remote_4.id, :secure_csn => 723456796, :serial => 'U601 646 760', :slot_id => slot_63.id,  :smartcard => 10078363818  )

# Zapper 1131
Decoder.create!(:back_image_url => "/assets/1131_back.png",           :front_image_url => "/assets/1131_front.png",           :manufacturer => 'Multichoice', model: '1131',            name: '1131',           :remote_id => remote_7.id, :secure_csn => 123456797, :serial => 'U200 639 947', :slot_id => slot_22.id,  :smartcard => 10078363925  )
Decoder.create!(:back_image_url => "/assets/1131_back.png",           :front_image_url => "/assets/1131_front.png",           :manufacturer => 'Multichoice', model: '1131',            name: '1131',           :remote_id => remote_7.id, :secure_csn => 523456797, :serial => 'U202 180 964', :slot_id => slot_26.id,  :smartcard => 10078363784  )
Decoder.create!(:back_image_url => "/assets/1131_back.png",           :front_image_url => "/assets/1131_front.png",           :manufacturer => 'Multichoice', model: '1131',            name: '1131',           :remote_id => remote_7.id, :secure_csn => 223456797, :serial => 'U202 390 334', :slot_id => slot_27.id,  :smartcard => 10078363933  )
Decoder.create!(:back_image_url => "/assets/1131_back.png",           :front_image_url => "/assets/1131_front.png",           :manufacturer => 'Multichoice', model: '1131',            name: '1131',           :remote_id => remote_7.id, :secure_csn => 323456797, :serial => 'U200 700 095', :slot_id => slot_42.id,  :smartcard => 10128138962  )
Decoder.create!(:back_image_url => "/assets/1131_back.png",           :front_image_url => "/assets/1131_front.png",           :manufacturer => 'Multichoice', model: '1131',            name: '1131',           :remote_id => remote_7.id, :secure_csn => 423456797, :serial => 'U200 681 204', :slot_id => slot_51.id,  :smartcard => 10128139317  )
Decoder.create!(:back_image_url => "/assets/1131_back.png",           :front_image_url => "/assets/1131_front.png",           :manufacturer => 'Multichoice', model: '1131',            name: '1131',           :remote_id => remote_7.id, :secure_csn => 623456797, :serial => 'U201 724 667', :slot_id => slot_55.id,  :smartcard => 10078363750  )
Decoder.create!(:back_image_url => "/assets/1131_back.png",           :front_image_url => "/assets/1131_front.png",           :manufacturer => 'Multichoice', model: '1131',            name: '1131',           :remote_id => remote_7.id, :secure_csn => 723456797, :serial => 'U200 822 167', :slot_id => slot_61.id,  :smartcard => 10078363990  )

# Zapper 1132
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 123456798, :serial => 'V102 429 157', :slot_id => slot_17.id,  :smartcard => 10064811267  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 223456798, :serial => 'V102 422 611', :slot_id => slot_21.id,  :smartcard => 10065062753  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 323456798, :serial => 'V102 419 053', :slot_id => slot_34.id,  :smartcard => 10065062696  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 423456798, :serial => 'V102 413 548', :slot_id => slot_35.id,  :smartcard => 10065062712  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 523456798, :serial => 'V102 422 603', :slot_id => slot_36.id,  :smartcard => 10065062746  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 623456798, :serial => 'V102 413 563', :slot_id => slot_37.id,  :smartcard => 10064992364  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 723456798, :serial => 'V102 429 197', :slot_id => slot_38.id,  :smartcard => 10064811382  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 823456798, :serial => 'V102 413 940', :slot_id => slot_39.id,  :smartcard => 10064992372  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 133456798, :serial => 'V102 422 579', :slot_id => slot_41.id,  :smartcard => 10065062779  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 233456798, :serial => 'V102 413 761', :slot_id => slot_43.id,  :smartcard => 10065062761  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 633456798, :serial => 'V101 271 923', :slot_id => slot_44.id,  :smartcard => 10078363768  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 333456798, :serial => 'V102 181 519', :slot_id => slot_50.id,  :smartcard => 10128139309  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 433456798, :serial => 'V102 413 539', :slot_id => slot_52.id,  :smartcard => 10065062720  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 533456798, :serial => 'V101 936 714', :slot_id => slot_53.id,  :smartcard => 10128139325  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 733456798, :serial => 'V101 783 448', :slot_id => slot_54.id,  :smartcard => 10078363743  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 833456798, :serial => 'V101 476 826', :slot_id => slot_57.id,  :smartcard => 10078363776  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 933456798, :serial => 'V101 379 808', :slot_id => slot_58.id,  :smartcard => 10078363842  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 143456798, :serial => 'V102 782 881', :slot_id => slot_59.id,  :smartcard => 10078363941  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 243456798, :serial => 'V102 075 111', :slot_id => slot_60.id,  :smartcard => 10078364006  )
Decoder.create!(:back_image_url => "/assets/1132_back.png",          :front_image_url => "/assets/1132_front.png",            :manufacturer => 'Multichoice', model: '1132',           name: '1132',            :remote_id => remote_5.id, :secure_csn => 343456798, :serial => 'V101 176 316', :slot_id => slot_62.id,  :smartcard => 10078363800  )


XtraviewSlot.delete_all
XtraviewSlot.create!(:primary_slot_id => slot_1.id, :secondary_slot_id => slot_17.id)
XtraviewSlot.create!(:primary_slot_id => slot_18.id, :secondary_slot_id => slot_2.id)
XtraviewSlot.create!(:primary_slot_id => slot_3.id, :secondary_slot_id => slot_19.id)
XtraviewSlot.create!(:primary_slot_id => slot_4.id, :secondary_slot_id => slot_20.id)
XtraviewSlot.create!(:primary_slot_id => slot_21.id, :secondary_slot_id => slot_5.id)
XtraviewSlot.create!(:primary_slot_id => slot_6.id, :secondary_slot_id => slot_22.id)
XtraviewSlot.create!(:primary_slot_id => slot_23.id, :secondary_slot_id => slot_7.id)
XtraviewSlot.create!(:primary_slot_id => slot_8.id, :secondary_slot_id => slot_24.id)
XtraviewSlot.create!(:primary_slot_id => slot_9.id, :secondary_slot_id => slot_25.id)
XtraviewSlot.create!(:primary_slot_id => slot_10.id, :secondary_slot_id => slot_26.id)
XtraviewSlot.create!(:primary_slot_id => slot_27.id, :secondary_slot_id => slot_11.id)
XtraviewSlot.create!(:primary_slot_id => slot_28.id, :secondary_slot_id => slot_12.id)
XtraviewSlot.create!(:primary_slot_id => slot_15.id, :secondary_slot_id => slot_13.id)
XtraviewSlot.create!(:primary_slot_id => slot_14.id, :secondary_slot_id => slot_16.id)



Button.delete_all
GuiButton.delete_all

# **** Explora remote ****
power_alphabet_nextgen = []
"3D831F5803BD0BCA000000000000000000000000000000000000000000000000".scan(/../).each {|number| power_alphabet_nextgen << number.to_i(16)}
power_ir_representaion_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030202020202020203020302030202020202030203020302020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030202020202020203020302030202020202030203020302020202020202020202027F".scan(/../).each {|number| power_ir_representaion_nextgen << number.to_i(16)}
nextgen_button_command_1 = Button.create!(:name =>"Explora_Power", :alphabet => power_alphabet_nextgen, :ir_representation => power_ir_representaion_nextgen, :button_command => 1 )

volume_plus_alphabet_nextgen = []
"3D861F5A03BD0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| volume_plus_alphabet_nextgen << number.to_i(16)}
volume_plus_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020202020202030202020302020203020302030203020202030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020202030202020302020203020302030203020202030202020202020202027F".scan(/../).each {|number| volume_plus_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_2 = Button.create!(:name =>"Explora_VolumePlus", :alphabet => volume_plus_alphabet_nextgen, :ir_representation => volume_plus_ir_representation_nextgen, :button_command => 2 )

volume_minus_alphabet_nextgen = []
"3D861F5A03BC0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| volume_minus_alphabet_nextgen << number.to_i(16)}
volume_minus_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020202020202030203020302020203020302030203020202020202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020202030203020302020203020302030203020202020202020202020202027F".scan(/../).each {|number| volume_minus_ir_representation_nextgen<< number.to_i(16)}
nextgen_button_command_3 = Button.create!(:name =>"Explora_VolumeMinus", :alphabet => volume_minus_alphabet_nextgen, :ir_representation => volume_minus_ir_representation_nextgen, :button_command => 3 )

channel_plus_alphabet_nextgen = []
"3D851F5A03BD0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| channel_plus_alphabet_nextgen << number.to_i(16)}
channel_plus_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F".scan(/../).each {|number| channel_plus_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_4 = Button.create!(:name =>"Explora_ChannelPlus", :alphabet => channel_plus_alphabet_nextgen, :ir_representation => channel_plus_ir_representation_nextgen, :button_command => 4 )

channel_minus_alphabet_nextgen = []
"3D851F5803BF0BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| channel_minus_alphabet_nextgen << number.to_i(16)}
channel_minus_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020202020302020203020302020203020302030202020302020202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020302020203020302020203020302030202020302020202020202020202027F".scan(/../).each {|number| channel_minus_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_5 = Button.create!(:name =>"Explora_ChannelMinus", :alphabet => channel_minus_alphabet_nextgen , :ir_representation => channel_minus_ir_representation_nextgen , :button_command => 5 )

mute_alphabet_nextgen = []
"3D831F5A03BD0BC7000000000000000000000000000000000000000000000000".scan(/../).each {|number| mute_alphabet_nextgen << number.to_i(16)}
mute_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202030202020202020203020302020203020202030203020302020202020202020202027F000102020203020302020203020302030203020302020202020302020203020202030202020202020203020302020203020202030203020302020202020202020202027F".scan(/../).each {|number| mute_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_7 = Button.create!(:name =>"Explora_Mute", :alphabet => mute_alphabet_nextgen, :ir_representation => mute_ir_representation_nextgen, :button_command => 7 )

backup_alphabet_nextgen = []
"3D851F5A03C00BC7000000000000000000000000000000000000000000000000".scan(/../).each {|number| backup_alphabet_nextgen << number.to_i(16)}
backup_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030202020302020202020302030202020202030202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030202020302020202020302030202020202030202020302030202020202020202027F".scan(/../).each {|number| backup_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_8 = Button.create!(:name =>"Explora_Backup", :alphabet => backup_alphabet_nextgen, :ir_representation => backup_ir_representation_nextgen, :button_command => 8 )

tv_guide_alphabet_nextgen = []
"3D821F5A03BC0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| tv_guide_alphabet_nextgen << number.to_i(16)}
tv_guide_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030202020302020203020302030202020202030202020302020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030202020302020203020302030202020202030202020302020202020202020202027F".scan(/../).each {|number| tv_guide_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_9 = Button.create!(:name =>"Explora_TV_Guide", :alphabet => tv_guide_alphabet_nextgen, :ir_representation => tv_guide_ir_representation_nextgen, :button_command => 9 )

info_alphabet_nextgen = []
"3D821F5A03BD0BC7000000000000000000000000000000000000000000000000".scan(/../).each {|number| info_alphabet_nextgen << number.to_i(16)}
info_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030202020302030203020302030202020202030202020202020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030202020302030203020302030202020202030202020202020202020202020202027F".scan(/../).each {|number| info_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_10 = Button.create!(:name =>"Explora_Info", :alphabet => info_alphabet_nextgen, :ir_representation => info_ir_representation_nextgen, :button_command => 10 )

menu_alphabet_nextgen = []
"3D821F5A03BC0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| menu_alphabet_nextgen << number.to_i(16)}
menu_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020302020202020302020203020302020202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020302020202020302020203020302020202020302030202020202020202027F".scan(/../).each {|number| menu_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_11 = Button.create!(:name =>"Explora_Menu", :alphabet => menu_alphabet_nextgen, :ir_representation => menu_ir_representation_nextgen, :button_command => 11 )

gotv_alphabet_nextgen = []
"3D821F5A03BC0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| gotv_alphabet_nextgen << number.to_i(16)}
gotv_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020302020202020302020203020302020202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020302020202020302020203020302020202020302030202020202020202027F".scan(/../).each {|number| gotv_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_12 = Button.create!(:name =>"Explora_GOtv", :alphabet => gotv_alphabet_nextgen, :ir_representation => gotv_ir_representation_nextgen, :button_command => 12 )

rewind_alphabet_nextgen = []
"3D7F1F5A03C20BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| rewind_alphabet_nextgen << number.to_i(16)}
rewind_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020202020202020302020203020302020203020302030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020202020202020302020203020302020203020302030202020202020202027F".scan(/../).each {|number| rewind_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_13 = Button.create!(:name =>"Explora_Rew", :alphabet => rewind_alphabet_nextgen, :ir_representation => rewind_ir_representation_nextgen, :button_command => 13 )

play_alphabet_nextgen = []
"3D831F5A03BF0BCB000000000000000000000000000000000000000000000000".scan(/../).each {|number| play_alphabet_nextgen << number.to_i(16)}
play_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020202020203020302020203020302020203020302020202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020202020203020302020203020302020203020302020202020202020202027F".scan(/../).each {|number| play_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_14 = Button.create!(:name =>"Explora_Play", :alphabet => play_alphabet_nextgen, :ir_representation => play_ir_representation_nextgen, :button_command => 14 )

stop_alphabet_nextgen = []
"3D831F5A03BD0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| stop_alphabet_nextgen << number.to_i(16)}
stop_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020202030203020302020203020302020203020202020202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020202030203020302020203020302020203020202020202020202020202027F".scan(/../).each {|number| stop_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_15 = Button.create!(:name =>"Explora_Stop", :alphabet => stop_alphabet_nextgen, :ir_representation => stop_ir_representation_nextgen, :button_command => 15 )

pause_alphabet_nextgen = []
"3D821F5A03BE0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| pause_alphabet_nextgen << number.to_i(16)}
pause_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020202020203020302020203020302020203020302020202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020202020203020302020203020302020203020302020202020202020202027F".scan(/../).each {|number| pause_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_16 = Button.create!(:name =>"Explora_Pause", :alphabet => pause_alphabet_nextgen, :ir_representation => pause_ir_representation_nextgen, :button_command => 16 )

record_alphabet_nextgen = []
"3D801F5803BF0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| record_alphabet_nextgen << number.to_i(16)}
record_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020302030202020302020203020302020202020202030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020302030202020302020203020302020202020202030202020202020202027F".scan(/../).each {|number| record_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_17 = Button.create!(:name =>"Explora_Record", :alphabet => record_alphabet_nextgen, :ir_representation => record_ir_representation_nextgen, :button_command => 17 )

ffwd_alphabet_nextgen = []
"3D831F5803C10BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| ffwd_alphabet_nextgen << number.to_i(16)}
ffwd_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020202030202020302020203020302020203020202030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020202030202020302020203020302020203020202030202020202020202027F".scan(/../).each {|number| ffwd_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_18 = Button.create!(:name =>"Explora_FFwd", :alphabet => ffwd_alphabet_nextgen, :ir_representation => ffwd_ir_representation_nextgen, :button_command => 18 )

ok_alphabet_nextgen = []
"3D871F5A03BD0BCB000000000000000000000000000000000000000000000000".scan(/../).each {|number| ok_alphabet_nextgen << number.to_i(16)}
ok_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020202020203020302030202020202020203020302020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020202020203020302030202020202020203020302020202020202020202027F".scan(/../).each {|number| ok_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_19 = Button.create!(:name =>"Explora_Ok", :alphabet => ok_alphabet_nextgen, :ir_representation => ok_ir_representation_nextgen, :button_command => 19 )

on_demand_alphabet_nextgen = []
"3D821F5A03BB0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| on_demand_alphabet_nextgen << number.to_i(16)}
on_demand_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020202030203020302030202020202020203020202020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020202030203020302030202020202020203020202020202020202020202027F".scan(/../).each {|number| on_demand_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_20 = Button.create!(:name =>"Explora_OnDemand", :alphabet => on_demand_alphabet_nextgen, :ir_representation => on_demand_ir_representation_nextgen, :button_command => 20 )

alt_alphabet_nextgen = []
"3D851F5803BD0BCA000000000000000000000000000000000000000000000000".scan(/../).each {|number| alt_alphabet_nextgen << number.to_i(16)}
alt_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020302030203020302030202020202020202020202020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020302030203020302030202020202020202020202020202020202020202027F".scan(/../).each {|number| alt_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_21 = Button.create!(:name =>"Explora_ALT", :alphabet => alt_alphabet_nextgen, :ir_representation => alt_ir_representation_nextgen, :button_command =>  21 )

box_office_alphabet_nextgen = []
"3D7F1F5A03BD0BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| box_office_alphabet_nextgen << number.to_i(16)}
box_office_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020302020203020302030202020202020202020302020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020302020203020302030202020202020202020302020202020202020202027F".scan(/../).each {|number| box_office_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_22 = Button.create!(:name =>"Explora_BoxOffice", :alphabet => box_office_alphabet_nextgen, :ir_representation => box_office_ir_representation_nextgen, :button_command => 22 )

option_alphabet_nextgen = []
"3D871F5A03C30BCA000000000000000000000000000000000000000000000000".scan(/../).each {|number| option_alphabet_nextgen << number.to_i(16)}
option_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020302030203020302020203020302020202020202020202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020203020302030203020302020203020302020202020202020202020202020202027F".scan(/../).each {|number| option_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_23 = Button.create!(:name =>"Explora_Option", :alphabet => option_alphabet_nextgen, :ir_representation => option_ir_representation_nextgen, :button_command => 23 )

help_alphabet_nextgen = []
"3D851F5A03C00BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| help_alphabet_nextgen << number.to_i(16)}
help_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020302030202020302030203020302020202020202030202020202020202020202020202027F000102020203020302020203020302030203020302020202020302020203020302030202020302030203020302020202020202030202020202020202020202020202027F".scan(/../).each {|number| help_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_24 = Button.create!(:name =>"Explora_Help", :alphabet => help_alphabet_nextgen, :ir_representation => help_ir_representation_nextgen, :button_command => 24 )

playlist_alphabet_nextgen = []
"3D851F5A03BD0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| playlist_alphabet_nextgen << number.to_i(16)}
playlist_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020302030202020302030202020202020202020202030202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020302030202020302030202020202020202020202030202020202020202027F".scan(/../).each {|number| playlist_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_25 = Button.create!(:name =>"Explora_Playlist", :alphabet => playlist_alphabet_nextgen, :ir_representation => playlist_ir_representation_nextgen, :button_command => 25 )

search_alphabet_nextgen = []
"3D7F1F5A03C20BD1000000000000000000000000000000000000000000000000".scan(/../).each {|number| search_alphabet_nextgen << number.to_i(16)}
search_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030202020202030203020302030202020202030203020202020202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030202020202030203020302030202020202030203020202020202020202020202027F".scan(/../).each {|number| search_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_26 = Button.create!(:name =>"Explora_Search", :alphabet => search_alphabet_nextgen, :ir_representation => search_ir_representation_nextgen, :button_command => 26 )

arc_alphabet_nextgen = []
"3D851F5703BE0BCB000000000000000000000000000000000000000000000000".scan(/../).each {|number| arc_alphabet_nextgen << number.to_i(16)}
arc_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020203020302020203020302020203020302020202020302020202020202020202027F".scan(/../).each {|number| arc_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_27 = Button.create!(:name =>"Explora_ARC", :alphabet => arc_alphabet_nextgen, :ir_representation => arc_ir_representation_nextgen, :button_command => 27 )

exit_alphabet_nextgen = []
"3D801F5803C00BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| exit_alphabet_nextgen << number.to_i(16)}
exit_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020202020302030202020302020203020302030202020202030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020302030202020302020203020302030202020202030202020202020202027F".scan(/../).each {|number| exit_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_28 = Button.create!(:name =>"Explora_Exit", :alphabet => exit_alphabet_nextgen, :ir_representation => exit_ir_representation_nextgen, :button_command => 28 )

tv_alphabet_nextgen = []
"3D801F5803C00BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| tv_alphabet_nextgen << number.to_i(16)}
tv_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020203020202020202020302030202020302020203020302030202020202030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020302030202020302020203020302030202020202030202020202020202027F".scan(/../).each {|number| tv_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_29 = Button.create!(:name =>"Explora_TV", :alphabet => tv_alphabet_nextgen, :ir_representation => tv_ir_representation_nextgen, :button_command =>  29 )

profile_alphabet_nextgen = []
"3D801F5A03BC0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| profile_alphabet_nextgen << number.to_i(16)}
profile_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030202020202030202020302030202020202030203020202030202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030202020202030202020302030202020202030203020202030202020202020202027F".scan(/../).each {|number| profile_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_30 = Button.create!(:name =>"Explora_Profile", :alphabet => profile_alphabet_nextgen, :ir_representation => profile_ir_representation_nextgen, :button_command => 30 )

left_alphabet_nextgen = []
"3D851F5803BF0BD0000000000000000000000000000000000000000000000000".scan(/../).each {|number| left_alphabet_nextgen << number.to_i(16)}
left_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020202020202020302030202020202020203020302030202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020202020202020302030202020202020203020302030202020202020202027F".scan(/../).each {|number| left_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_31 = Button.create!(:name =>"Explora_Left", :alphabet => left_alphabet_nextgen, :ir_representation => left_ir_representation_nextgen, :button_command => 31 )

right_alphabet_nextgen = []
"3D871F5803BF0BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| right_alphabet_nextgen << number.to_i(16)}
right_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020202030202020302030202020202020203020202030202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020202030202020302030202020202020203020202030202020202020202027F".scan(/../).each {|number| right_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_32 = Button.create!(:name =>"Explora_Right", :alphabet => right_alphabet_nextgen, :ir_representation => right_ir_representation_nextgen, :button_command => 32 )

up_alphabet_nextgen = []
"3D8C1F5A03BC0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| up_alphabet_nextgen << number.to_i(16)}
up_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030202020302030202020302030202020202030202020202030202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030202020302030202020302030202020202030202020202030202020202020202027F".scan(/../).each {|number| up_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_33 = Button.create!(:name =>"Explora_Up", :alphabet => up_alphabet_nextgen, :ir_representation => up_ir_representation_nextgen, :button_command => 33 )

down_alphabet_nextgen = []
"3D841F5A03BC0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| down_alphabet_nextgen << number.to_i(16)}
down_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020302030203020302020202020302030202020202020202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020202020302030203020302020202020302030202020202020202020302030202020202020202027F".scan(/../).each {|number| down_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_34 = Button.create!(:name =>"Explora_Down", :alphabet => down_alphabet_nextgen, :ir_representation => down_ir_representation_nextgen, :button_command =>  34 )

number_0_alphabet_nextgen = []
"3D831F5803BF0BC7000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_0_alphabet_nextgen << number.to_i(16)}
number_0_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020202020202020302030203020302030203020302030202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020202020202020202020302030203020302030203020302030202020202020202027F".scan(/../).each {|number| number_0_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_35 = Button.create!(:name =>"Explora_0", :alphabet => number_0_alphabet_nextgen, :ir_representation => number_0_ir_representation_nextgen, :button_command => 35 )

number_1_alphabet_nextgen = []
"3D841F5A03C00BCC000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_1_alphabet_nextgen << number.to_i(16)}
number_1_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020202020203020302030203020302030203020302020202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020202020202020203020302030203020302030203020302020202020202020202027F".scan(/../).each {|number| number_1_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_36 = Button.create!(:name =>"Explora_1", :alphabet => number_1_alphabet_nextgen, :ir_representation => number_1_ir_representation_nextgen, :button_command => 36 )

number_2_alphabet_nextgen = []
"3D831F5A03BB0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_2_alphabet_nextgen << number.to_i(16)}
number_2_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020202030202020302030203020302030203020202030202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020202020202030202020302030203020302030203020202030202020202020202027F".scan(/../).each {|number| number_2_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_37 = Button.create!(:name =>"Explora_2", :alphabet => number_2_alphabet_nextgen, :ir_representation => number_2_ir_representation_nextgen, :button_command => 37 )

number_3_alphabet_nextgen = []
"3D871F5A03BC0BCB000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_3_alphabet_nextgen << number.to_i(16)}
number_3_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020202030203020302030203020302030203020202020202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020202020202030203020302030203020302030203020202020202020202020202027F".scan(/../).each {|number| number_3_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_38 = Button.create!(:name =>"Explora_3", :alphabet => number_3_alphabet_nextgen, :ir_representation => number_3_ir_representation_nextgen, :button_command => 38 )

number_4_alphabet_nextgen = []
"3D8B1F5A03BD0BCD000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_4_alphabet_nextgen << number.to_i(16)}
number_4_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020302020202020302030203020302030202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020202020302020202020302030203020302030202020302030202020202020202027F".scan(/../).each {|number| number_4_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_39 = Button.create!(:name =>"Explora_4", :alphabet => number_4_alphabet_nextgen, :ir_representation => number_4_ir_representation_nextgen, :button_command => 39 )

number_5_alphabet_nextgen = []
"3D871F5703C00BC7000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_5_alphabet_nextgen << number.to_i(16)}
number_5_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020302020203020302030203020302030202020302020202020202020202027F".scan(/../).each {|number| number_5_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_40 = Button.create!(:name =>"Explora_5", :alphabet => number_5_alphabet_nextgen, :ir_representation => number_5_ir_representation_nextgen, :button_command => 40 )

number_6_alphabet_nextgen = []
"3D851F5803C20BC5000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_6_alphabet_nextgen << number.to_i(16)}
number_6_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020302030202020302030203020302030202020202030202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020202020302030202020302030203020302030202020202030202020202020202027F".scan(/../).each {|number| number_6_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_41 = Button.create!(:name =>"Explora_6", :alphabet => number_6_alphabet_nextgen, :ir_representation => number_6_ir_representation_nextgen, :button_command => 41 )

number_7_alphabet_nextgen = []
"3D8A1F5A03BC0BC7000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_7_alphabet_nextgen << number.to_i(16)}
number_7_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020202020302030203020302030203020302030202020202020202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020202020302030203020302030203020302030202020202020202020202020202027F".scan(/../).each {|number| number_7_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_42 = Button.create!(:name =>"Explora_7", :alphabet => number_7_alphabet_nextgen, :ir_representation => number_7_ir_representation_nextgen, :button_command => 42 )

number_8_alphabet_nextgen = []
"3D871F5803BE0BD0000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_8_alphabet_nextgen << number.to_i(16)}
number_8_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020203020202020202020302030203020302020203020302030202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020203020202020202020302030203020302020203020302030202020202020202027F".scan(/../).each {|number| number_8_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_43 = Button.create!(:name =>"Explora_8", :alphabet => number_8_alphabet_nextgen, :ir_representation => number_8_ir_representation_nextgen, :button_command => 43 )

number_9_alphabet_nextgen = []
"3D851F5803BB0BC7000000000000000000000000000000000000000000000000".scan(/../).each {|number| number_9_alphabet_nextgen << number.to_i(16)}
number_9_ir_representation_nextgen = []
"000102020203020302020203020302030203020302020202020302020202020202020203020202020203020302030203020302020203020302020202020202020202027F000102020203020302020203020302030203020302020202020302020202020202020203020202020203020302030203020302020203020302020202020202020202027F".scan(/../).each {|number| number_9_ir_representation_nextgen << number.to_i(16)}
nextgen_button_command_44 = Button.create!(:name =>"Explora_9", :alphabet => number_9_alphabet_nextgen, :ir_representation => number_9_ir_representation_nextgen, :button_command => 44 )


GuiButton.create!(:button_id => nextgen_button_command_1.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1075, :y => 70)
GuiButton.create!(:button_id => nextgen_button_command_2.id, :remote_id => remote_1.id, :height => 20, :width => 25,  :x => 1140, :y => 336)
GuiButton.create!(:button_id => nextgen_button_command_3.id, :remote_id => remote_1.id, :height => 20, :width => 25,  :x => 1100, :y => 358)
GuiButton.create!(:button_id => nextgen_button_command_4.id, :remote_id => remote_1.id, :height => 20, :width => 25,  :x => 1016, :y => 338)
GuiButton.create!(:button_id => nextgen_button_command_5.id, :remote_id => remote_1.id, :height => 20, :width => 25,  :x => 1056, :y => 358)
GuiButton.create!(:button_id => nextgen_button_command_7.id, :remote_id => remote_1.id, :height => 20, :width => 25,  :x => 1140, :y => 300)
GuiButton.create!(:button_id => nextgen_button_command_8.id, :remote_id => remote_1.id, :height => 20, :width => 25,  :x => 1135, :y => 160)
GuiButton.create!(:button_id => nextgen_button_command_9.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1028, :y => 122)
GuiButton.create!(:button_id => nextgen_button_command_10.id, :remote_id => remote_1.id, :height => 20, :width => 25,  :x => 1018, :y => 160)
GuiButton.create!(:button_id => nextgen_button_command_11.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1075, :y => 155)
GuiButton.create!(:button_id => nextgen_button_command_12.id, :remote_id => remote_1.id, :height => 0, :width => 0,  :x => 0, :y => 0)
GuiButton.create!(:button_id => nextgen_button_command_13.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1034, :y => 427)
GuiButton.create!(:button_id => nextgen_button_command_14.id, :remote_id => remote_1.id, :height => 35, :width => 40,  :x => 1069, :y => 394)
GuiButton.create!(:button_id => nextgen_button_command_15.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1025, :y => 383)
GuiButton.create!(:button_id => nextgen_button_command_16.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1025, :y => 400)
GuiButton.create!(:button_id => nextgen_button_command_17.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1132, :y => 383)
GuiButton.create!(:button_id => nextgen_button_command_18.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1122, :y => 427)
GuiButton.create!(:button_id => nextgen_button_command_19.id, :remote_id => remote_1.id, :height => 25, :width => 25,  :x => 1077, :y => 230)
GuiButton.create!(:button_id => nextgen_button_command_20.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1036, :y => 90)
GuiButton.create!(:button_id => nextgen_button_command_21.id, :remote_id => remote_1.id, :height => 25, :width => 20,  :x => 1016, :y => 300)
GuiButton.create!(:button_id => nextgen_button_command_22.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1115, :y => 90)
GuiButton.create!(:button_id => nextgen_button_command_23.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1074, :y => 324)
GuiButton.create!(:button_id => nextgen_button_command_24.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1022, :y => 538)
GuiButton.create!(:button_id => nextgen_button_command_25.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1075, :y => 107)
GuiButton.create!(:button_id => nextgen_button_command_26.id, :remote_id => remote_1.id, :height => 20, :width => 30,  :x => 1120, :y => 122)
GuiButton.create!(:button_id => nextgen_button_command_27.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1136, :y => 538)
GuiButton.create!(:button_id => nextgen_button_command_28.id, :remote_id => remote_1.id, :height => 0, :width => 0,  :x => 0, :y => 0)
GuiButton.create!(:button_id => nextgen_button_command_29.id, :remote_id => remote_1.id, :height => 30, :width => 15,  :x => 1137, :y => 200)
GuiButton.create!(:button_id => nextgen_button_command_30.id, :remote_id => remote_1.id, :height => 30, :width => 15,  :x => 1025, :y => 200)
GuiButton.create!(:button_id => nextgen_button_command_31.id, :remote_id => remote_1.id, :height => 45, :width => 20,  :x => 1044, :y => 220)
GuiButton.create!(:button_id => nextgen_button_command_32.id, :remote_id => remote_1.id, :height => 45, :width => 20,  :x => 1115, :y => 220)
GuiButton.create!(:button_id => nextgen_button_command_33.id, :remote_id => remote_1.id, :height => 20, :width => 45,  :x => 1067, :y => 198)
GuiButton.create!(:button_id => nextgen_button_command_34.id, :remote_id => remote_1.id, :height => 20, :width => 45,  :x => 1067, :y => 270)
GuiButton.create!(:button_id => nextgen_button_command_35.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1078, :y => 538)
GuiButton.create!(:button_id => nextgen_button_command_36.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1041, :y => 453)
GuiButton.create!(:button_id => nextgen_button_command_37.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1083, :y => 453)
GuiButton.create!(:button_id => nextgen_button_command_38.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1120, :y => 453)
GuiButton.create!(:button_id => nextgen_button_command_39.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1041, :y => 480)
GuiButton.create!(:button_id => nextgen_button_command_40.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1083, :y => 480)
GuiButton.create!(:button_id => nextgen_button_command_41.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1120, :y => 480)
GuiButton.create!(:button_id => nextgen_button_command_42.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1041, :y => 508)
GuiButton.create!(:button_id => nextgen_button_command_43.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1083, :y => 508)
GuiButton.create!(:button_id => nextgen_button_command_44.id, :remote_id => remote_1.id, :height => 20, :width => 20,  :x => 1120, :y => 508)
# ***********************1
