class ButtonController < ApplicationController

  def buttons
    @buttons = Button.all
  end

  def update_button
    button_id = params[:button_id]
    button = Button.find_by_id button_id
    button.alphabet = params[:alphabet]
    button.ir_representation = params[:ir_representation]
    button.name = params[:name]
    button.button_command = params[:button_command]
    button.frequency_timer_count = params[:frequency_timer_count]
    button.save!
    render :nothing => true
  end

end 
