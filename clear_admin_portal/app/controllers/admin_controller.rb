require_relative '../../../../clearsystem/protocols/clear_view_protocol'
require_relative '../../../../clearsystem/protocols/ruby/clear_protocol'
require_relative '../../../../clearcore/corelibs/ruby/tcp_client'
require_relative '../../../../clearcore/corelibs/ruby/constants'
require_relative '../../../../clearcore/corelibs/ruby/ip_utility_code'

class AdminController < ApplicationController
  
  include Clear_View_Protocol
  include Clear_Protocol
  include Constants

  @@view_slot = 0
  @@count = 0
  @@tx_number = 0
  @@positions = {}
  @@opened_slot = {}

  def index
    @remotes = Remote.all
    @button_hash = Hash.new
    @@master_tcp_client = TCPSocketClientSocket.new
    @@master_tcp_client.connect check_local_ip, master_server_port, method(:data_method), method(:notification_method) 
  end

  def handle_message from, message
    puts "Master Portal-Message Handler-handle_message:#{message} from #{from}"
    case message.cmd_cat
    when ClearViewProtocolConst.MASTER_MESSAGES
      puts "Master Portal-Message Handler-Got Master Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.DECODER_REQUEST_RESPONSE
        puts "Master Portal-Message Handler-Decoder request response received"
        handle_decoder_request_response message
      end
    else puts "Command does not apply to master portal"
    end
  end

  def data_method data
    puts "data_method -TCP CLIENT GOT DATA!!!"
    p data
      puts "TCP CLIENT data_method ELSE REACHED"
      message_data = []
      data.each_byte do |b|
        message_data << b
      end
      p message_data
      handle_message check_local_ip, ClearViewProtocolBinary.new(:data=>message_data).to_clear_view_protocol_object
  end

  def notification_method source, state, message
    puts 'IRController notification_method called!'
    p source
    p state
    p message
  end

  def unlock_slot 
    slot_number = params[:slot].to_i
    engine_tcp_client = TCPSocketClientSocket.new
    slot = Slot.find_by_slot_number slot_number
    ip = slot.node.ip
    engine_tcp_client.connect ip, (slot_number + tcp_server_port_delta), method(:data_method), method(:notification_method) 
    stop_engine = StopEngine.new tx_number: get_tx_number
    stop_engine.set_slot_number slot_number
    engine_tcp_client.send stop_engine.to_binary
    render :nothing => true
  end 

  def lock_slot 
    slot_number = params[:slot].to_i
    slot = Slot.find_by_slot_number slot_number
    ip = slot.node.ip
    tx_number = get_tx_number
    start_clear_test = StartClearTest.new tx_number: tx_number
    start_clear_test.set_slot_number slot_number
    @@master_tcp_client.send start_clear_test.to_binary
    slot_wait = 0
    while !@@opened_slot[tx_number]
      if slot_wait < 30 
        slot_wait += 1
        sleep 1
        puts "waiting"
      else 
        @@opened_slot[tx_number] = 0
      end
    end 
    slot = Slot.find_by_slot_number @@opened_slot[tx_number]
    node = Node.find_by_id slot.node_id
    ip = node.external_ip
    render :text => "http://#{ip}:#{@@opened_slot[tx_number] + slot_port_delta}/portal/index"
  end 

  def handle_decoder_request_response message
    slot_number = message.get_slot_number 
    if @@opened_slot[message.tx_number] == nil 
      @@opened_slot[message.tx_number] = slot_number
    end
  end

  def get_tx_number
    @@tx_number += 1
  end

   def update_view_slot
    if params[:slot].to_i == 0 
      @@count = 0 
      @@positions = {}
      render :nothing => true
    else 
      @@view_slot = params[:slot]
      @@count += 1
      @@positions[@@count] = @@view_slot
      render :nothing => true
    end
  end

  def remove_view_slot
    slot = params[:slot]
    index = @@positions.key slot
    @@positions.delete index
  end 

  def vlc
    @slot = Slot.find_by_slot_number @@view_slot
    @count = @@count
    @udp_port_delta = 2000
  end

  def active_slots 
    @slots = Slot.all
    @node_names = []
    all_nodes = Node.all
    all_nodes.each { |fetched_node| @node_names << fetched_node.name }
    @locked_slots = Slot.where(locked: true)
    @unlocked_slots = Slot.where(locked: false)
  end 

  def active_slots_sidebar
    node_name = params[:node]
    node = Node.find_by_name node_name
    @slots = node.slots
    @locked_slots = Slot.where(locked: true)
  end 

  end
