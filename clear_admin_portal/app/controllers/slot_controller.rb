class SlotController < ApplicationController
  
  def slots 
    @slots = Slot.all
  end 
  
  def slot_video
    @slots = Slot.all
  end 

  def update_slot
    slot_number = params[:slot_number]
    slot = Slot.find_by_id slot_number
    slot.node_id = params[:node_id]
    slot.ir_netbox_id = params[:ir_netbox_id]
    slot.ir_slot = params[:ir_slot]
    slot.audio_device = params[:audio_device]
    slot.power_unit_id = params[:pdu_id]
    slot.power_unit_slot = params[:pdu_slot]
    slot.serial_server_id = params[:serial_id]
    slot.serial_server_slot = params[:serial_slot]
    slot.save!
    render :nothing => true
  end

  def update_slot_video
    slot_number = params[:slot_number]
    slot = Slot.find_by_id slot_number
    slot.input_codec = params[:input_codec]
    slot.size = params[:size]
    slot.input_format = params[:input_format]
    slot.video_device = params[:video_device]
    slot.output_codec = params[:output_codec]
    slot.output_format = params[:output_format]
    slot.bitrate = params[:bitrate]
    slot.framerate = params[:framerate]
    slot.output_url = params[:output_url]
    slot.save!
    render :nothing => true
  end

   def create_slot
    last_slot = Slot.maximum(:slot_number).to_i 
    slot = Slot.new 
    slot.ir_netbox_id = params[:ir_netbox_id]
    slot.slot_number = last_slot + 1 
    slot.ir_slot = params[:ir_slot]
    slot.node_id = params[:node_id]
    slot.power_unit_id = params[:pdu_id]
    slot.power_unit_slot = params[:pdu_slot]
    slot.serial_server_id = params[:serial_id]
    slot.input_codec = 'h264'
    slot.size = '720x576'
    slot.input_format = 'video4linux2'
    slot.video_device = '/dev/video0'
    slot.output_codec = 'x264'
    slot.output_format = 'mpegts'
    slot.bitrate = 768
    slot.framerate = 25
    slot.output_url = '239.1.1.1'
    slot.save!
    render :nothing => true
  end

  def delete_slot
    slot_number = params[:slot_number]
    slot = Slot.find_by_slot_number slot_number
    slot.delete
    render :nothing => true
  end


end 
