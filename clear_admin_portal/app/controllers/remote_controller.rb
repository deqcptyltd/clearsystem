class RemoteController < ApplicationController

  def remotes
    @remotes = Remote.all
  end

  def update_remote
    remote_id = params[:remote_id]
    remote = Remote.find_by_id remote_id
    remote.name = params[:name]
    remote.model = params[:model]
    remote.remote_type = params[:remote_type]
    remote.image_url = params[:image_url]
    remote.save!
    render :nothing => true
  end

  def create_remote
    remote = Remote.new
    remote.name = params[:name]
    remote.model = params[:model]
    remote.remote_type = params[:remote_type]
    remote.image_url = params[:image_url]
    remote.save!
    render :nothing => true
  end 

  def delete_remote
    remote_id = params[:remote_id]
    remote = Remote.find_by_id remote_id
    remote.delete
    render :nothing => true
  end 

end 
