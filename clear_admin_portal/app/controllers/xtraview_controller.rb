class XtraviewController < ApplicationController

  def xtraviews
    @xtraviews = XtraviewSlot.all
  end

  def update_xtraview
    xtraview_id = params[:xtraview_id]
    xtraview = XtraviewSlot.find_by_id xtraview_id
    xtraview.primary_slot_id = params[:primary_slot_id]
    xtraview.secondary_slot_id = params[:secondary_slot_id]
    xtraview.save!
    render :nothing => true
  end

  def create_xtraview
    xtraview = XtraviewSlot.new
    xtraview.primary_slot_id = params[:primary_slot_id]
    xtraview.secondary_slot_id = params[:secondary_slot_id]
    xtraview.save!
    render :nothing => true
  end

    def delete_xtraview
    xtraview_id = params[:xtraview_id]
    xtraview = XtraviewSlot.find_by_id xtraview_id
    xtraview.delete
    render :nothing => true
  end


end 
