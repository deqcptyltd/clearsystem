class DecoderController < ApplicationController

  def decoders
    @decoders = Decoder.all
  end
  
  def update_decoder
    decoder_id = params[:decoder_id]
    decoder = Decoder.find_by_id decoder_id.to_i
    decoder.errors.full_messages
    decoder.name = params[:name]
    decoder.model = params[:model]
    decoder.manufacturer = params[:manufacturer]
    decoder.serial = params[:serial]
    decoder.smartcard = params[:smartcard]
    decoder.secure_csn = params[:secure_csn]
    decoder.remote_id = params[:remote_id]
    decoder.slot_id = params[:slot_id]
    decoder.front_image_url = params[:front_image_url]
    decoder.back_image_url = params[:back_image_url]
    decoder.save!
    decoder.errors.full_messages
    render :nothing => true
  end 

  def create_decoder
    decoder = Decoder.new 
    decoder.name = params[:name]
    decoder.model = params[:model]
    decoder.manufacturer = params[:manufacturer]
    decoder.serial = params[:serial]
    decoder.smartcard = params[:smartcard]
    decoder.secure_csn = params[:secure_csn]
    decoder.remote_id = params[:remote_id]
    decoder.slot_id = params[:slot_id]
    decoder.front_image_url = params[:front_image_url]
    decoder.back_image_url = params[:back_image_url]
    decoder.save!
    render :nothing => true
  end

  def delete_decoder
    decoder_id = params[:decoder_id]
    decoder = Decoder.find_by_id decoder_id
    decoder.delete
    render :nothing => true
  end 


end 
