class GuiButtonController < ApplicationController

    @button_hash = Hash.new

  def gui_buttons
    @gui_buttons = GuiButton.all
    @remotes = Remote.all
  end

  def gui_button_fields
    session[:active_remote] = params[:active]
    @active = session[:active_remote]
    @gui_buttons = GuiButton.all
    @remotes = Remote.all
    @button_hash = Hash.new
    @gui_buttons.each do |gui_hash_key|
      @button_hash[gui_hash_key.id] = Button.find_by_id gui_hash_key.button_id
    end
  end

  def update_active_remote
    session[:active_remote] = params[:active]
    @active = session[:active_remote]
    render :nothing => true
  end

  def update_gui_button
    session[:active_remote] = params[:active]
    @active = session[:active_remote]
    gui_button_id = params[:gui_button_id]
    puts "gui_button_id is #{gui_button_id}"
    gui_button = GuiButton.find_by_id gui_button_id
    puts "Guibutton is #{gui_button.id}"
    puts "Width is #{params[:width]}"
    gui_button.width = params[:width]
    gui_button.height = params[:height]
    gui_button.x = params[:x]
    gui_button.y = params[:y]
    gui_button.remote_id = params[:remote_id]
    gui_button.save!
    render :nothing => true
  end

end 
