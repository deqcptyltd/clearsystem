class GuiButton < ActiveRecord::Base
  attr_accessible :button_id, :remote_id, :height, :image_url, :width, :x, :y

  belongs_to :button
  belongs_to :remote

  validates :button_id, :remote_id, :height, :width, :x, :y, presence: true
  validates :button_id, uniqueness: true
  validates :button_id, :remote_id, :height, :width, :x, :y, numericality: { only_integer: true }

end
