class Slot < ActiveRecord::Base
  attr_accessible  :bitrate, :framerate, :input_codec, :input_format, :ir_netbox_id, :node_id, :output_codec, :output_format, :output_url, :size, :slot_number, :video_device, :ir_slot
  
  belongs_to :node
  belongs_to :ir_netbox 
  has_one :decoder
  has_one :xtraview_slot

  validates :ir_netbox_id, :node_id, :slot_number, :ir_slot, :bitrate, :framerate, :input_codec, :input_format, :output_format, :output_url, :size, :video_device, presence: true
  validates :bitrate, :framerate, :ir_netbox_id, :node_id, :slot_number, :ir_slot,  numericality: { only_integer: true }
  validates :slot_number, uniqueness: true 
  #validates :size, format: { with: /Ad{1,3}[x]d{1,3}\z/, message: "Size must be in pixels times pixels format"}
  validates :framerate, :inclusion => 5..35
end
