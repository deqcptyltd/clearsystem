class SessionLog < ActiveRecord::Base
  attr_accessible :session_end, :slot, :start, :query_type, :client_ip, :client_host, :xtraview_slot
end
