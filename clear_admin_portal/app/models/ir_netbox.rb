class IrNetbox < ActiveRecord::Base

  attr_accessible :ip, :node_id, :port, :slot_count

	has_many :slots
	belongs_to :node

	validates :ip, :node_id, :port, :slot_count, presence: true
	validates :ip, uniqueness: true
	validates :node_id, :port, :slot_count, numericality: { only_integer: true }
#	validates :ip, format: { with: /Ad{1,3}[.]d{1,3}[.]d{1,3}[.]d{1,3}\z/ , message: "must be an IP address"}
	validates :ip, format: { with: /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/ , message: "must be an IP address"}
	validates :port, :inclusion => 1..65535
	validates :slot_count, :inclusion => 1..16
end
