class Decoder < ActiveRecord::Base
  attr_accessible :back_image_url, :front_image_url, :manufacturer, :model, :name, :remote_id, :secure_csn, :serial, :slot_id, :smartcard

  belongs_to :remote
  belongs_to :slot

  validates :back_image_url, :front_image_url, :manufacturer, :model, :name, :remote_id, :secure_csn, :serial, :slot_id, :smartcard, presence: true
  validates :secure_csn, :serial, :slot_id, :smartcard, uniqueness: true
  validates :remote_id, :secure_csn, :slot_id, :smartcard,  numericality: { only_integer: true }
	validates_format_of :back_image_url, :front_image_url,
  :with    => %r{\.(gif|jpe?g|png)$}i,
  :message => "must have an image extension"
  validates :secure_csn, length: { is: 9}
  validates :smartcard, length: { minimum: 10, maximum: 11}


end
