class Remote < ActiveRecord::Base
  attr_accessible :image_url, :model, :name, :remote_type

  #belongs_to :decoder
  has_many :buttons, through: :gui_buttons
  has_many :gui_buttons

  validates :image_url, :model, :name, :remote_type, presence: true
  validates :name, uniqueness: true
  validates_format_of :image_url,
  :with    => %r{\.(gif|jpe?g|png)$}i,
  :message => "must have an image extension"

end
