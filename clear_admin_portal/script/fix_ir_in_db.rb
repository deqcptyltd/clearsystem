require_relative '../config/environment'

ActiveRecord::Base.establish_connection

Remote.all.each do |remote|
  if remote.name != 'explora'
    remote.buttons.each do |button|
      temp_alpha = []
      button.alphabet.scan(/../).each {|number| temp_alpha << number.to_i(16)}
      button.alphabet = temp_alpha
      temp_ir_rep = []
      button.ir_representation.scan(/../).each {|number| temp_ir_rep << number.to_i(16)}
      button.ir_representation = temp_ir_rep
      button.save!
    end
  end
end
