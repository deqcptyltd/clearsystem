require_relative '../clear_admin_portal/config/environment.rb'
ActiveRecord::Base.establish_connection
button_commands = { "power" => 1, "vol_plus" => 2, "vol_minus" => 3, "channel_plus" => 4, "channel_minus" => 5, "mute" => 7, "backup" => 8, "tv_guide" => 9, "info" => 10, "menu" => 11, "gotv" => 12, "rew" => 13, "play" => 14, "stop" => 15, "pause" => 16, "record" => 17, "ffwd" => 18,
                    "ok" => 19, "ondemand" => 20, "alt" => 21, "boxoffice" => 22, "option" => 23, "help" => 24, "playlist" => 25, "search" => 26, "arc" => 27, "exit" => 28, "tv" => 29, "profile" => 30, "left" => 31, "right" => 32, "up" => 33, "down" => 34, "0" => 35, "1" => 36, "2" => 37, "3" => 38,
                    "4" => 39, "5" => 40, "6" => 41, "7" => 42, "8" => 43, "9" => 44, "language" => 45, "exit" => 46, "red" => 47, "blue" => 48, "white" => 49, "yellow" => 50, "green" => 51, "help" => 52, "subtitles" => 53, "audio" => 54, "profile" => 55 }
parse_array = []
file_name = "#{ARGV[0]}"
File.open(file_name ,"r").each do |line|
  line.slice! "XMPProtocol False"
  line.slice! "XMPProtocol True"
  line.slice! "Empty Database"
  line.slice! "Device "
  line.slice! "/"
  line.gsub! /\t/, ''
  line.slice! "MOD_SIG16"
  line.slice! "MOD_SIG18"
  line.gsub! '+', '_plus'
  line.gsub! "-", '_minus'
  line.downcase!
  if  !((line =~ /\n/) == 1)
    parse_array << line
  end
end 
control_type = parse_array[0]
control_type.gsub!(/\s/,'')
parse_array.shift
parse_array.each do |line|
  p line
  command_index = (line.index(' ') -1)
  command = line[0..command_index]
  line.slice! (0..(command_index + 1))
  line.upcase!
  header = line[0..29]
  frequency_timer_count = header[8..11].to_i(16)
  p frequency_timer_count
  alphabet_length = line[18..19].to_i(16)
  p alphabet_length
  line.slice! (0..29)
  alphabet = nil
  if alphabet_length <= 4      
    alphabet = line[0..63]
    line.slice! (0..63)
  elsif alphabet_length == 10 || alphabet_length == 9
    alphabet = line[0..65]
    line.slice! (0..65)
  else 
    alphabet = line[0..((alphabet_length * 4) -1)]
    line.slice! (0..((alphabet_length *4) -1))
  end
  ir_representation = line
  puts alphabet
  puts ir_representation 
  #button = Button.find_by_name "#{control_type}_#{command}"
  temp_alphabet = []
  alphabet.scan(/../).each {|number| temp_alphabet << number.to_i(16)}
  p temp_alphabet
  #button.alphabet = temp_alphabet
  temp_ir_rep = []
  ir_representation.scan(/../).each {|number| temp_ir_rep << number.to_i(16)}
  #button.ir_representation = temp_ir_rep
  p temp_ir_rep
  #p button
  #button.save!
  new_button = Button.create!(:name => "#{control_type}_#{command}", :alphabet => temp_alphabet, :ir_representation => temp_ir_rep, :button_command => button_commands[command], :frequency_timer_count => frequency_timer_count )
  remote = Remote.find_by_model control_type
  new_gui_button = GuiButton.create!(:button_id => new_button.id, :remote_id => remote.id, :height => 20, :width => 20, :x => 1000, :y => 400)
end

