require_relative '../../clearcore/corelibs/ruby/tcp_client'
require_relative '../../clearcore/corelibs/ruby/clear_logger'

#Needs to open a connection to the AsyncAgent and controll the talking to it

class IRController

  def initialize sync_agent_ip, sync_agent_port, response_method
    @sync_agent_ip = sync_agent_ip
    @sync_agent_port = sync_agent_port
    @response_method = response_method

    @ir_sync_agent_tcp_client = TCPSocketClientSocket.new  
    @ir_sync_agent_tcp_client.connect sync_agent_ip, sync_agent_port, method(:data_method), method(:notification_method)
    #TODO: Any other code to keep alive the connection to the ir_sync_agent much still be done
  end

  def send data
    @ir_sync_agent_tcp_client.send data
  end

  def data_method data#, from
    log 'clearcommon', 'data_method -IR CONTROLLER GOT DATA!!!'
    log 'clearcommon', "data length: #{data.length} data: #{p data}"
    message_data = []
    data.each_byte do |b|
      message_data << b
    end
    log 'clearcommon', "message_data: #{p message_data}"
      @response_method.call nil, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
  end


  def notification_method source, state, message
    log 'clearcommon', 'IRController notification_method called!'
    log 'clearcommon', "source: #{source}, state: #{state}, message: #{message}"
  end

end
