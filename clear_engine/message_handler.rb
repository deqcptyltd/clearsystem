require_relative '../clear_hw_api/clearpics/clear_pics'
require_relative '../clear_hw_api/clearpics/ocr_parser'
require 'socket'
require 'active_record'
require_relative 'ir_controller'
require_relative '../../clearcore/corelibs/ruby/tcp_client'
require_relative 'constants_and_settings'
require_relative '../protocols/ruby/clear_protocol'
require_relative '../clear_hw_api/video_api'
require_relative '../clear_hw_api/audio_api'
require_relative '../clear_hw_api/serial_api'
require_relative '../clear_hw_api/pdu_api'
require_relative '../clear_admin_portal/config/environment.rb'
require_relative '../../clearcore/corelibs/ruby/clear_logger'
require 'net/http'

include Constants
include Clear_Protocol

class MessageHandler

  attr_accessor :slot_number
  
  def initialize controller
    @button_tx = 0
    @slot_number = 0
    @clear_pics = ClearPics.new
    TransactionHandler.init
    @controller = controller
    @ir_controller = IRController.new SETTINGS.ir_sync_agent_ip, SETTINGS.ir_sync_agent_port, method(:handle_message)
  end

  # The the entry point to the message handler
  def handle_message from, message

    if from && message
      transaction = Transaction.new from, message
      TransactionHandler.transactions << transaction
    end

    log 'clearcommon', "Controller-Message Handler-handle_message:#{message} from #{from}"
    case message.cmd_cat
    when ClearProtocolConst.COMMON_MESSAGES
      log 'clearcommon', "Controller-Message Handler-Got ClearCommon Command Type"
      case message.cmd_type
      when ClearProtocolConst.ACK
        log 'clearcommon', 'Controller-Message Handler-ACK received'
        handle_ack  message
      when ClearProtocolConst.NAK
        log 'clearcommon', 'Controller-Message Handler-NACK received'
        handle_nak  message
      when ClearProtocolConst.NOTIFICATION
        log 'clearcommon', 'Controller-Message Handler-NOTIFICATION received'
      end
    when ClearProtocolConst.ENGINE_MESSAGES
      log 'clearcommon', 'Controller-Message Handler-Got ClearEngine Command Type'
      case message.cmd_type
      when ClearProtocolConst.PUSH_BUTTON
        log 'clearcommon', 'Controller-Message Handler-Got PUSH_BUTTON command'
        handle_push_button from, message
      when ClearProtocolConst.STOP_ENGINE
        log 'clearcommon', 'Controller-Message Handler-Got STOP_ENGINE command'
        handle_stop_engine from, message
      when ClearProtocolConst.START_SERIAL_LOGGING
        log 'clearcommon', 'Controller-Message Handler-Got START_SERIAL_LOGGING command'
        handle_serial_logging from, message
      when ClearProtocolConst.POWER_ON_SLOT
        log 'clearcommon', 'Controller-Message Handler-Got POWER_ON command'
        handle_power_on from, message
      when ClearProtocolConst.POWER_OFF_SLOT
        log 'clearcommon', 'Controller-Message Handler-Got POWER_OFF command'
        handle_power_off from, message
      when ClearProtocolConst.POWER_CYCLE_SLOT
        log 'clearcommon', 'Controller-Message Handler-Got POWER_CYCLE command'
        handle_power_cycle from, message
      when ClearProtocolConst.CAPTURE_IMAGE
        log 'clearcommon', 'Controller-Message Handler-Got CAPTURE_SCREEN command'
        handle_capture_image from, message
      when ClearProtocolConst.COMPARE_PICTURE
        log 'clearcommon', 'Controller-Message Handler-Got COMPARE_IMAGE command'
        handle_compare_picture from, message
      when ClearProtocolConst.DETECT_MOVEMENT
        log 'clearcommon', 'Controller-Message Handler-Got DETECT_MOVEMENT command'
        handle_detect_movement from, message
      when ClearProtocolConst.OCR
        log 'clearcommon', 'Controller-Message Handler-Got OCR command'
        handle_ocr from, message
      when ClearProtocolConst.INITIALIZE_SLOT
        log 'clearcommon', 'Controller-Message Handler-Got INITIALIZE_SLOT command'
        handle_initialize_slot from, message
      when ClearProtocolConst.ZAP_TIME
        log 'clearcommon', 'Controller-Message Handler-Got ZAP_TIME command'
        handle_zap_time from, message
      when ClearProtocolConst.DETECT_SOUND
        log 'clearcommon', 'Controller-Message Handler-Got DETECT_SOUND command'
        handle_detect_sound from, message
      when ClearProtocolConst.ANALYSE_COMPARE_IMAGE
        log 'clearcommon', 'Controller-Message Handler-Got ANALYSE_COMPARE_IMAGE command'
        handle_analyse_compare_image from, message
      when ClearProtocolConst.CHECK_COLOUR
        log 'clearcommon', 'Controller-Message Handler-Got CHECK_COLOUR command'
        handle_check_colour from, message
      when ClearProtocolConst.CATALOGUE_VALIDATION
        log 'clearcommon', 'Controller-Message Handler-Got CATALOG_VALIDATION command'
        handle_catalogue_validation from, message
      when ClearProtocolConst.TAKE_PICTURE
        log 'clearcommon', 'Controller-Message Handler-Got TAKE_PICTURE command'
        take_picture from, message
      else
        log 'clearcommon', "Controller-Message Handler-Got Command Type not yet handled in ClearEngine[#{message.cmd_cat}]: #{message.cmd_type}"
      end
    when ClearProtocolConst.MASTER
      log 'clearcommon', 'Controller-Message Handler-Got Master Command Type'
    when ClearProtocolConst.SLAVE
      log 'clearcommon', 'Controller-Message Handler-Got Slave Command Type'
    else
      log 'clearcommon', "Controller-Message Handler-Got Command Category not yet handled: #{message.cmd_cat}"
    end
  end

  def catalogue_push_button command_number
    @button_tx += 1
    push_button = PushButton.new tx_number: @button_tx
    push_button.set_slot_number @slot_number
    push_button.set_button_command command_number
    @ir_controller.send push_button.to_binary
    sleep 1
  end 

  def handle_catalogue_validation from, message
    begin
      rectangle = eval message.get_rectangles
      url = message.get_url
      type = message.get_type
      url.slice! "http://"
      server = url[0..(url.index(':') -1)]
      port = url[(url.index(':') +1)..(url.index('/') -1)]
      action = url[(url.index(port) + port.length)..(url.index('?') -1)]
      variable = url[(url.index('?') +1)..(url.index('=') )]
      slot = Slot.find_by_slot_number @slot_number

      connection = Net::HTTP.new(server, port)
      response = connection.get("#{action}?#{variable}#{type}")
      if response.body.class == String
        required_items = JSON.parse response.body
      end 
      titles = []
      required_items.each do | item | 
        titles <<  item["Title"]
      end 

      ocr_parser = OCRParser.new titles, :catalogue

      found_titles = []
      title_index = 0
      catalogue_push_button 28 # EXIT 
      sleep 1
      catalogue_push_button 22 # BOXOFFICE 

      udp_url =  "udp://#{slot.output_url}:#{@slot_number + SETTINGS.udp_port_delta}"
      @clear_pics.udp_address = udp_url
      @clear_pics.video_device = slot.video_device
      iterations = titles.size * 2
      found_ocr = []
      duplicates = 0

      puts "ITERATIONS for Slot #{@slot_number} at #{Time.now} = #{iterations}"
      sleep 10
      iterations.times do  
        matched = ocr_parser.matched_strings.size
        ocr_hash = @clear_pics.do_catalogue_ocr rectangle

        if !found_ocr.include? ocr_hash[:results]['0_string']
          ocr_parser.match ocr_hash[:results]['0_string']
          found_ocr << ocr_hash[:results]['0_string']
          duplicates = 0
          
          if ocr_parser.status == true 
            catalogue_push_button 32 # RIGHT 
            sleep 2
            title_index += 1
          else 
            ocr_hash = @clear_pics.do_catalogue_ocr rectangle
            ocr_parser.match ocr_hash[:results]['0_string']
            catalogue_push_button 32 # RIGHT 
            sleep 2
            title_index += 1
          end 
          
          if (ocr_parser.matched_strings.size > matched) && (duplicates == 0)
            interim_result = TestResponse.new :tx_number => message.tx_number
            interim_result.set_result 'false'
            interim_data = {}
            interim_data[:message] = "Matched catalog item: [#{ocr_hash[:results]['0_string']}] to  #{ocr_parser.matched_strings.last}  - #{ocr_parser.method}"
            interim_data[:image] = ocr_hash[:results][:images][1]
            interim_result.set_data interim_data.inspect
            @controller.send_to_client interim_result.to_binary, from
            sleep 1
            matched = ocr_parser.matched_strings.size
          else 
            interim_result = TestResponse.new :tx_number => message.tx_number
            interim_result.set_result 'false'
            interim_data = {}
            interim_data[:message] = "Could not match catalog item: [#{ocr_hash[:results]['0_string']}]"
            interim_data[:image] = ocr_hash[:results][:images][1]
            interim_result.set_data interim_data.inspect
            @controller.send_to_client interim_result.to_binary, from
            sleep 1
          end 

        else 
          sleep 1 
          duplicates += 1
          title_index -= 1
          interim_result = TestResponse.new :tx_number => message.tx_number
          interim_result.set_result 'false'
          interim_data = {}
          interim_data[:message] = "Duplicate OCR item: [#{ocr_hash[:results]['0_string']}], not adding."
          interim_data[:image] = ocr_hash[:results][:images][1]
          interim_result.set_data interim_data.inspect
          @controller.send_to_client interim_result.to_binary, from
          sleep 1
        end 

        if duplicates > 1
          break
        end 
      end 
      puts "################################################# Result for Slot #{@slot_number} at #{Time.now}"
      print  'Matched: '
      p ocr_parser.matched_strings
      print  'Unmatched: '
      p ocr_parser.unmatched_strings
      puts "#################################################"
    rescue
      log 'clearcommon', "Couldn't validate the catalogue"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 12
      data.set_reason "Couldn't validate the catalogue"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result 'true'
      result = {}
      result[:titles] = titles.join(', ')
      result[:matched] = ocr_parser.matched_strings.join(', ')
      result[:unmatched] =  ocr_parser.unmatched_strings.join(', ')
      return_data = result.inspect
      return_value.set_data return_data
      @controller.send_to_client return_value.to_binary, from

    end
  end

  def take_picture from, message
    begin
      type = message.get_type
      slot = Slot.find_by_slot_number message.get_slot_number
      url =  "udp://#{slot.output_url}:#{message.get_slot_number + SETTINGS.udp_port_delta}"
      @clear_pics.udp_address = url
      @clear_pics.video_device = slot.video_device
      @clear_pics.move_picture type
      @clear_pics.take_picture type
    rescue
      log 'clearcommon', "Couldn't take the picture"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 12
      data.set_reason "Couldn't take the picture"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
    end
  end 

  def handle_analyse_compare_image from, message
    begin 
      rectangles = eval message.get_rectangles 
      button_command = message.get_button_command
      timeout = message.get_time_out
      image_path = message.get_image_path
      percent = message.get_percent
      push_button button_command, message.tx_number, from
      slot = Slot.find_by_slot_number @slot_number
      @clear_pics.video_device = slot.video_device
      return_data = @clear_pics.compare_multiple_images rectangles, timeout, image_path, percent
   rescue
      log 'clearcommon', "Could not analyse compare image"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 6
      data.set_reason "Could not analyse compare image"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result return_data[:status].to_s
      return_value.set_data return_data[:results].inspect
      @controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_check_colour from, message
    begin 
     rectangles = eval message.get_rectangles 
     red = message.get_red
     blue = message.get_blue
     green = message.get_green
     threshold = message.get_threshold
     percent = message.get_percent
     slot = Slot.find_by_slot_number @slot_number
     @clear_pics.video_device = slot.video_device
     url =  "udp://#{slot.output_url}:#{message.get_slot_number + SETTINGS.udp_port_delta}"
     @clear_pics.udp_address = url
     return_data = @clear_pics.colour_check red, green, blue, rectangles, threshold, percent
    rescue
      log 'clearcommon', "Could not check colour"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 5
      data.set_reason "Could not check colour"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result return_data[:status].to_s
      return_value.set_data return_data[:results].inspect
      @controller.send_to_client return_value.to_binary, from
    end
  end 

  def handle_zap_time from, message
    begin 
      rectangles = eval message.get_rectangles 
      button_command = message.get_button_command
      button_command_count = message.get_button_command_c
      timeout = message.get_time_out
      (0..button_command_count).each do | count | 
        push_button button_command, message.tx_number, from
      end 
      slot = Slot.find_by_slot_number @slot_number
      @clear_pics.video_device = slot.video_device
      return_data = @clear_pics.compare_multiple_images rectangles, timeout
    rescue
      log 'clearcommon', "Could not get Zaptime"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 5
      data.set_reason "Could not get Zaptime"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result return_data[:status].to_s
      return_value.set_data return_data[:results].inspect
      @controller.send_to_client return_value.to_binary, from
    end
  end 

  def push_button button_command, tx_number, from
    begin
      button = PushButton.new tx_number: tx_number
      button.set_slot_number @slot_number
      button.set_button_command button_command
      @ir_controller.send button.to_binary
    rescue
      log 'clearcommon', "Couldn't send the IR Pushbutton message"
      data = Nak.new :tx_number => tx_number
      data.set_error_code 12
      data.set_reason "Couldn't send the IR Pushbutton message"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
    end
  end

  def handle_detect_sound from, message
    begin 
      time_out = message.get_time_out 
      audio_api = AudioAPI.new @slot_number, time_out
      detected = audio_api.test_for_volume 
    rescue 
      puts "couldnt detect the audio for Slot #{@slot_number} at #{Time.now}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 7
      data.set_reason "Couldn't detect audio"
      @controller.send_to_client data.to_binary, from
    else 
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result "#{detected}"
      @controller.send_to_client return_value.to_binary, from
    end 
  end 

  def handle_capture_image from, message 
    begin
      rectangles_string = message.get_rectangles
      rectangles_hash = eval rectangles_string
      type = message.get_type
      slot = Slot.find_by_slot_number message.get_slot_number
      url =  "udp://#{slot.output_url}:#{message.get_slot_number + SETTINGS.udp_port_delta}"
      @clear_pics.udp_address = url
      @clear_pics.video_device = slot.video_device
      return_data = @clear_pics.take_picture type, rectangles_hash
    rescue
      puts "couldnt take the picture for Slot #{@slot_number} at #{Time.now}"
      puts "Return data was #{return_data}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 4
      data.set_reason "Couldn't take the screenshot"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result "true"
      return_value.set_data return_data
      @controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_compare_picture from, message
    begin
      rectangles_string = message.get_rectangles
      rectangles_hash = eval rectangles_string
      image_rectangles_string = message.get_image_rectangles
      image_rectangles = eval image_rectangles_string
      time_out = message.get_time_out
      percent = message.get_percent
      slot = Slot.find_by_slot_number message.get_slot_number
      url =  "udp://#{slot.output_url}:#{message.get_slot_number + SETTINGS.udp_port_delta}"
      puts "URL IS #{url}"
      @clear_pics.udp_address = url
      @clear_pics.video_device = slot.video_device
      image_path = message.get_image_path 
      puts "THE PASSED IMAGE PATH IS #{image_path} for Slot #{@slot_number} at #{Time.now} "
      return_result = @clear_pics.call_compare_images image_path, percent, time_out, rectangles_hash, image_rectangles
    rescue
      puts "couldnt compare the images for Slot #{@slot_number} at #{Time.now}"
      puts "Return data was #{return_result}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 4
      data.set_reason "Couldn't compare the images"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result return_result[:status].to_s
      return_value.set_data return_result[:results].inspect
      @controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_detect_movement from, message
    begin
      rectangles_string = message.get_rectangles
      rectangles_hash = eval rectangles_string
      puts rectangles_hash
      slot_number = message.get_slot_number
      slot = Slot.find_by_slot_number slot_number
      url =  "udp://#{slot.output_url}:#{message.get_slot_number + SETTINGS.udp_port_delta}"
      puts "URL IS #{url}"
      @clear_pics.udp_address = url
      @clear_pics.video_device = slot.video_device
      timeout = message.get_time_out
      percent = message.get_percent
      save = message.get_save
      puts "RUNNING DETECT MOVEMENT WITH timeout #{timeout} save #{save} percent #{percent} rectangles #{rectangles_hash} for Slot #{@slot_number} at #{Time.now}"
      result = @clear_pics.check_for_movement  timeout, save, percent, rectangles_hash
    rescue
      puts "couldnt detect the movement for Slot #{@slot_number} at #{Time.now}"
      puts "Return data was #{result}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 4
      data.set_reason "Couldn't detect the movement"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      return_value = TestResponse.new :tx_number => message.tx_number
      sleep 1
      return_value.set_result result[:status].to_s
      return_value.set_data result[:results].inspect
      @controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_ocr from, message
    begin
      search_string = message.get_search_string
      rectangle_string = message.get_rectangles
      rectangles = eval rectangle_string
      slot = Slot.find_by_slot_number message.get_slot_number
      url =  "udp://#{slot.output_url}:#{message.get_slot_number + SETTINGS.udp_port_delta}"
      @clear_pics.udp_address = url
      @clear_pics.video_device = slot.video_device
      ocr_hash = @clear_pics.do_ocr rectangles
      ocr_parser = OCRParser.new [search_string], :navigate
      rectangles.count.times do |t|
        if !ocr_hash[:results]["#{t}_result"]
          ocr_parser.match ocr_hash[:results]["#{t}_string"]
          if ocr_parser.matched_strings.length == 1
            ocr_hash[:results]["#{t}_string"] = ocr_parser.matched_strings[0]
            ocr_hash[:results]["#{t}_result"] = true 
            ocr_hash[:status] = true
          end
        end
      end
      puts "Result for OCR for Slot #{@slot_number} at #{Time.now}"
      print  'Matched: '
      p ocr_parser.matched_strings
      print  'Unmatched: '
      p ocr_parser.unmatched_strings
      return_result = ocr_hash[:status].to_s
      return_data = ocr_hash[:results].inspect
    rescue
      puts "couldnt evaluate the OCR for Slot #{@slot_number} at #{Time.now}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 4
      data.set_reason "Couldn't evaluate the OCR"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result return_result
      return_value.set_data return_data
      @controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_push_button from, message
    log 'clearcommon', "Controller-Message Handler-handle_push_button- from: #{from} message: #{message} for Slot #{@slot_number} at #{Time.now}"
    begin
      # Send message to connected ir_sync_agent
      @ir_controller.send message.to_binary
    rescue
      log 'clearcommon', "Couldn't send the power on message for Slot #{@slot_number} at #{Time.now}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 12
      data.set_reason "Couldn't send the power on message"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      #return_value = TestResponse.new :tx_number => message.tx_number
      #return_value.set_result "true"
      #@controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_initialize_slot from, message
    @slot_number = message.get_slot_number
    @clear_pics.clean_path @slot_number
    @clear_pics.determine_path @slot_number
  end 

  def handle_serial_logging from, message
    begin
      slot_number = message.get_slot_number
      client_ip = message.get_client_ip 
      puts "SLOT NUMBER FOR SERIAL IS #{slot_number}"
      puts "IP FOR SERIAL CLIENT IS #{client_ip}"
      fork do
        serial_connection = SerialAPI.new client_ip
        serial_connection.slot_listen slot_number
      end
    rescue
      log 'clearcommon', "Couldn't send the serial logging  message for Slot #{@slot_number} at #{Time.now}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 12
      data.set_reason "Couldn't send the serial logging message"
      @controller.send_to_client data.to_binary, from
    else
      data = Ack.new :tx_number => message.tx_number
      @controller.send_to_client data.to_binary, from
      sleep 1
      serial_started = SerialLoggingRequestResponse.new :tx_number => message.tx_number
      serial_started.set_port_number (slot_number + SETTINGS.client_serial_port_delta)
      @controller.send_to_client serial_started.to_binary, from
      puts "MESSAGE SENT TO CLIENT (#{from})"
    end
  end

  def handle_power_on from, message 
    begin
      slot_number = message.get_slot_number
      pdu_connection = PowerUnitAPI.new
      pdu_connection.slot_on slot_number
    rescue
      log 'clearcommon', "Couldn't send the power on  message for Slot #{@slot_number} at #{Time.now}"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 12
      data.set_reason "Couldn't send the IR Pushbutton message"
      @controller.send_to_client data.to_binary, from
    else
      pdu_on = Ack.new :tx_number => message.tx_number
      @controller.send_to_client pdu_on.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result "true"
      @controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_power_off from, message
    begin
      slot_number = message.get_slot_number
      pdu_connection = PowerUnitAPI.new
    rescue
      log 'clearcommon', "Couldn't send the IR Pushbutton message"
      data = Nak.new :tx_number => message.tx_number
      data.set_error_code 12
      data.set_reason "Couldn't send the IR Pushbutton message"
      @controller.send_to_client data.to_binary, from
    else pdu_connection.slot_off slot_number
      pdu_off = Ack.new :tx_number => message.tx_number
      @controller.send_to_client pdu_off.to_binary, from
      sleep 1
      return_value = TestResponse.new :tx_number => message.tx_number
      return_value.set_result "true"
      @controller.send_to_client return_value.to_binary, from
    end
  end

  def handle_reboot from, message
    #TODO this is not implemented in the xml yet 
    slot_number = message.get_slot_number
    pdu_connection = PDUAPI.new
    pdu_connection.slot_reboot slot_number
    reboot = Ack.new :tx_number => message.tx_number
    @controller.send_to_client reboot.to_binary, from
  end
  
  def handle_stop_engine from, message
    puts "SENDING CLOSING NAK for Slot #{@slot_number} at #{Time.now}"
    data = Nak.new :tx_number => message.tx_number
    data.set_error_code 5
    data.set_reason "Engine has been terminated"
    @controller.send_to_client data.to_binary, from
    puts "STOPPING ENGINE for Slot #{@slot_number} at #{Time.now}"
    @controller.stop_children
    @controller.stop_engine
  end 
  
  def handle_video_message_with_parameters
    #TODO enable the video API to be called by passing parameters instead of making a database call
  end

  def handle_ack message
    ack = Ack.new :tx_number => message.tx_number
    send_message  ack
  end

  def handle_nak message
    nak = Nak.new :tx_number => message.tx_number
    nak.set_error_code message.get_error_code
    send_message nak
  end

  def send_message message
    p message.tx_number
    transaction = TransactionHandler.get_transaction_from_tx_number message.tx_number
    p transaction
    if transaction 
      log 'clearcommon', "Transaction from: #{transaction.from}, for: #{transaction.message} with tx_number: #{message.tx_number} has completed for Slot #{@slot_number} at #{Time.now}"
      @controller.send_to_client message.to_binary, transaction.from
    end
  end

end


class TransactionHandler

  def self.transactions
    @transactions
  end

  def self.init
    @transactions = []
  end
  
  def self.print_transactions
    p @transactions
  end

  def self.manage_transaction transaction
    @transactions[transaction.server_transaction_number] = transaction
  end

  def self.get_transaction_from_tx_number tx_number
    transaction_copy = nil
    @transactions.each do | transaction | 
      if transaction.client_transaction_number == tx_number
        transaction_copy = transaction
        break
      end
    end
    if transaction_copy 
      @transactions.delete transaction_copy
      return transaction_copy
    end
  end
end

class Transaction

  attr_reader :from, :message, :client_transaction_number, :origin

  def initialize from, message
    @from  = from
    @message = message
    @client_transaction_number = message.tx_number
    @origin = origin
  end

end

