require_relative 'message_handler'
require_relative '../../clearcore/corelibs/ruby/tcp_server'
require_relative 'constants_and_settings'
require_relative '../protocols/ruby/clear_protocol'
require_relative '../../clearcore/corelibs/ruby/clear_logger'

# Hosts the tcp servers and controllers the data flow to and from it between the internals of this system and the outside world
# ie it will get a message do any pre formating on it parse it and then pass it on to the message handler for processing

include Constants
include Clear_Protocol

class Controller

  def initialize engine
    @engine = engine
    @message_handler = MessageHandler.new self
  end

  def start_server slot
    log 'clearcommon', 'start_server method started'
    begin
      @tcp_server = TCPServerSocket.new 
      @tcp_server.start slot + SETTINGS.server_port_delta, method(:data_method), method(:get_notification)
      return true
    rescue=>e
      log 'clearcommon', 'Got Exception:'
      log 'clearcommon', e.backtrace.join("\n")
      return false
    end
  end

  def send_to_client data, client
    log 'clearcommon', "send_to_client method started to #{client}"
    @tcp_server.send_data_to data, client
  end

  def stop_engine
    @engine.stop_engine
  end

  def stop_children
    @engine.stop_children
  end

  def get_notification notification_type, string
    if notification_type == :close_slot 
      puts "CLIENT DISCONNECTED, CLOSING SLOT"
      stop_children 
      stop_engine
    end 
  end 

  def data_method data, from
    log 'clearcommon', 'data_method - GOT DATA!!!'
    log 'clearcommon', "data length: #{data.length} data: #{p data}"
    message_data = []
    data.each_byte do |b|
      message_data << b
    end
    p message_data
    p  ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
    Thread.new do
      @message_handler.handle_message from, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
    end 
  end


end
