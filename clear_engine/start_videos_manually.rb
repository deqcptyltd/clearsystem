# slot 33 -> video0
# slot 48 -> video39
# slot 49 -> video48
# slot 64 -> video87

vid_ch = 48
(49..64).each do |i|
  thing1 = "ffmpeg -vcodec rawvideo -video_size 720x576 -f video4linux2 -i /dev/video#{vid_ch} -an -vcodec libx264 -profile main -tune zerolatency -x264opts intra-refresh=1:vbv-bufsize=32 -b:v 192k -f mpegts -r 25 -v quiet udp://239.1.1.1:#{i + 2000} &"
  vid_ch += 1
  if vid_ch % 4 == 0
    vid_ch += 8
  end
  thing2 = "ruby engine.rb #{i}"
  puts thing1


  pid_value = fork do
    begin
      [$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
      exec thing1
    ensure
      Process.exit!
    end
  end
  #puts thing2 + " started on PID #{pid_value}"
  puts "PID: #{pid_value}"
  sleep 5 #+ Random.rand(15)

end

