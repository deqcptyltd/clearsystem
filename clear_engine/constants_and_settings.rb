require 'yaml'
require_relative '../../clearcore/corelibs/ruby/ip_utility_code'

# Defines all application constants and settings

# Holds all real time settings that can be accessed or changed
class Settings
  def initialize config_file

    if !File.exists? config_file
      ip = check_local_ip # this  function in 'ip_utility_code'
      # create config.yml file automatically if not found in folder
      object = { :identifier => "ClearEngine",
                 :server_port_delta => 20000,
                 :ir_sync_agent_ip => " #{ip}".strip,   # trick here to force 8-bit ASCII encoding... yaml & ruby 1.9.3 quirk
                 :ir_sync_agent_port => 51515,
                 :debug => true,
                 :mode => :development,
                 :type => :test,
                 :client_serial_port_delta => 40000,
                 :udp_port_delta => 2000,
                 :video_device_delta => 2000
      }
      File.open(config_file, 'wb') { |f| f.write(YAML.dump(object)) }        
    end

    yaml = YAML::load(File.open(config_file))
   
    # Creating meta programming class accessors as defined in the yaml file
    yaml.each_pair do |key, val|
      self.class.instance_eval do
        define_method(key) {return val}
      end
    end
  end
end

module Constants

  # Info Constants

  VERSION = "1.0 Alpha"

  # Operational Constants

  SETTINGS = Settings.new 'config.yml'

  DEBUG = SETTINGS.debug

  DATABASE_SETTINGS = ""

end

