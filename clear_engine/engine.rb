require_relative 'constants_and_settings'
require_relative 'controller'
require_relative '../clear_hw_api/video_api'
require_relative '../../clearcore/corelibs/ruby/clear_logger'
require_relative '../../clearcore/corelibs/ruby/constants'
require_relative '../../clearcore/corelibs/ruby/timer'

# Engine main startup file
  #  - Initialises all the components and APIS and connects to any other components

class ClearEngine

  include Constants

  def initialize options
    @@active = true
    @@slot_number = options[:slot]
    set_mode SETTINGS.mode
    @type = SETTINGS.type
    if options[0] == "help"
      log 'clearsystem', "The argument format for Clear Engine is:"
      log 'clearsystem', "slot_number [input_codec] [size] [input_format] [video_device] [output_codec] [bitrate] [output_format] [framerate] [output_url]"
    else
      @controller = Controller.new self
      @controller.start_server options[:slot].to_i
      @video_api = VideoAPI.new 
      @video_api.start_video options
      @rails_port = options[:slot] + 10000
      @stopwatch = Timer.new
      @timeout = 0
      #if @type == :view
        fork do
          [$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
          exec "cd ../../clearview/engine_portal ; rails s -p #{@rails_port} -P #{@rails_port}.pid" 
        end
        log 'clearsystem', "Web server has been started on port #{@rails_port}"
      #end
    end
  end
  
  def stop_rails
    time = @stopwatch.elapsed_time
    Log.create(type: "Session End", content: "The session duration was #{time}")
    #File.delete ("engine_portal/#{slot + slot_port_delta}.pid")
    `rm ../../clearview//engine_portal/#{@@slot_number + slot_port_delta}.pid` 
    rails_pid = `ps -ef | grep ruby | grep #{@rails_port} | grep -v grep | awk -F ' ' '{print $2}'`
    `../scripts/stop_rails.sh #{rails_pid}`
    puts "RAILS PID #{rails_pid} WAS STOPPED"
  end

  def stop_children
    stop_rails
    @video_api.stop_video
  end

  def stop_engine
    @controller = nil
    @@active = false
  end
 
  def check_status
    @stopwatch.tick
    @timeout += 1
    if @timeout == 2700 && @type == :view
      stop_children
      sleep 1;
      stop_engine
    end
    return @@active
  end

end

def get_arguments arguments
  case arguments.length 
  when 1 
    options = {slot: arguments[0].to_i}
  when 10 
    options = {slot: arguments[0].to_i,
              input_codec: arguments[1], 
              size: arguments[2], 
              input_format: arguments[3],
              video_device: arguments[4],
              output_codec: arguments[5],
              bitrate: arguments[6],
              output_format: arguments[7],
              framerate: arguments[8],
              output_url: arguments[9]}
  end
  return options 
end

options = get_arguments ARGV

clear_engine =  ClearEngine.new options
while clear_engine.check_status == true do
  sleep(1)
end
