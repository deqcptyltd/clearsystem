require_relative '../../clearcommon/corelibs/ruby/tcp_server'
require_relative '../../protocols/clear_view_protocol'
require_relative '../../clearcommon/protocols/ruby/clear_protocol'
require 'socket'
require 'sqlite3'
require 'active_record'
require_relative '../../clearcommon/clear_admin_portal/config/environment.rb'


include Clear_Protocol
include Clear_View_Protocol

class Master

  @@tx_number = 0

  def start_server 
    puts "start_server method started"
    begin
      @master = TCPServerSocket.new 
      @master.start 15151, method(:data_method)
      return true
    rescue=>e
      puts "Got Exception: "
      p e
      puts e.backtrace.join("\n")
      return false
    end
  end

  def data_method data, from
    puts "data_method - GOT DATA!!!"
    p data
    if data.include? ']'
      puts "TCP CLIENT data_method IF REACHED"
      split = data.strip.gsub('[', '').split(']')
      split.each do |message|
        split_message = message.split(',')
        message_data = []
        split_message.each do |val|
          message_data << val.to_i
        end
        @message_handler.handle_message from, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
      end
    else
      puts "TCP CLIENT data_method ELSE REACHED"
      message_data = []
      data.each_byte do |b|
        message_data << b
      end
      p message_data
      clear_binary = ClearProtocolBinary.new(:data=>message_data)
      if clear_binary.cmd_cat == ClearProtocolConst.COMMON_MESSAGES || clear_binary.cmd_cat == ClearProtocolConst.ENGINE_MESSAGES
        handle_message from, clear_binary.to_clear_protocol_object
      else
        puts"Identified as clear view message"
        handle_message from, ClearViewProtocolBinary.new(:data=>message_data).to_clear_view_protocol_object
      end
    end
  end

  def handle_message from, message
    puts "MESSAGE FROM IS #{from} AND MESSAGE IS #{message}"
    puts "Master-Message Handler-handle_message:#{message} from #{from}"
    case message.cmd_cat
    when ClearProtocolConst.ENGINE_MESSAGES
      puts "Controller-Message Handler-Got ClearEngine Command Type"
      case message.cmd_type
      when ClearProtocolConst.PUSH_BUTTON
        puts "Controller-Message Handler-Got PUSH_BUTTON command"
      when ClearProtocolConst.STOP_ENGINE
        puts "Controller-Message Handler-Got STOP_ENGINE command"
        stop_engine message
      when ClearProtocolConst.START_ENGINE
        puts "Controller-Message Handler-Got start engine command"
        start_engine message.get_slot_number
      end
    when ClearProtocolConst.COMMON_MESSAGES
      log 'clearcommon', "Controller-Message Handler-Got ClearCommon Command Type"
      case message.cmd_type
      when ClearProtocolConst.ACK
        log 'clearcommon', 'Controller-Message Handler-ACK received'
        handle_ack  message
      when ClearProtocolConst.NAK
        log 'clearcommon', 'Controller-Message Handler-NACK received'
        handle_nak  message
      end
    end
    case message.cmd_cat
    when ClearViewProtocolConst.MASTER_MESSAGES
      puts "Controller-Message Handler-Got Master Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.REQUEST_STATUS
        puts "Controller-Message Handler-Status request received"
      when ClearViewProtocolConst.REQUEST_DECODER
        puts "Controller-Message Handler-Decoder request received"
        handle_decoder_request message.get_decoder_model
      when ClearViewProtocolConst.REQUEST_XTRAVIEW
        puts "Controller-Message Handler-Xtravtew request received"
      end
    when ClearViewProtocolConst.SLAVE_MESSAGES
      puts "Controller-Message Handler-Got Slave Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.SLAVE_STATUS_RESPONSE
        puts "Controller-Message Handler-Got Status response command"
        handle_slave_status_response message
      else
        puts "Controller-Message Handler-Got Command Type not yet handled in ClearEngine[#{message.cmd_cat}]: #{message.cmd_type} "
      end
    end
  end

  def send_to_client data, client
    puts "send_to_client method started"
    @master.send_data_to data, client
  end

  def start_engine slot_number
    slot = Slot.find_by_slot_number (slot_number)
    client_ip = slot.node.ip
    start_engine_request = StartEngine.new tx_number: get_tx_number
    start_engine_request.set_slot_number slot_number
    send_to_client start_engine_request.to_binary, client_ip
  end 

  def stop_engine slot_number 
    slot = Slot.find_by_slot_number (slot_number)
    client_ip = slot.node.ip
    stop_engine_request = StopEngine.new tx_number: get_tx_number
    stop_engine_request.set_slot_number slot_number
    send_to_client stop_engine_request.to_binary, client_ip
  end

  def request_status node
    node = Node.find_by_id (node)
    client_ip = node.ip
    status_request = RequestStatus.new tx_number: get_tx_number
    status_request.set_status_type 1
    send_to_client status_request.to_binary, client_ip
  end	

  def get_tx_number
    @@tx_number += 1
  end

end  

if __FILE__ == $0

  master = Master.new 
  master.start_server
  sleep (10) 
  master.request_status 1
  sleep (10)

  loop do
    sleep(1)
  end

end


