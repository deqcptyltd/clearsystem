$: << '/var/lib/gems/1.9.1/gems/rtesseract-1.2.2/lib'
$: << '/var/lib/gems/1.9.1/gems/chunky_png-1.3.1/lib'
require 'rtesseract'
require 'chunky_png'
require_relative '../../clear_hw_api/clearpics/spellcheck'

def do_ocr_on_image image_file, search_string, rectangle_hash=nil
  puts "OCR STARTING"
  puts image_file
  if !File.exists? image_file 
    raise "File does not exist"
    return 1
  end
  return_object = Hash.new
  return_object[:status] = true
  return_object[:results] = {}
  puts "RETURN OBJECT CREATED"
  if rectangle_hash == nil
    rectangle_hash = Hash.new
    image = RTesseract.read(image_file) do |img|
      rectangle = {x: 0, y: 0, width: img.columns, height: img.rows } # TODO get image width and height from image
      rectangle_hash[1] = rectangle
    end
  end
  count = 1
  rectangle_hash.each_pair do |key, rectangle|
    image = RTesseract.read(image_file) do |img| 
      x = rectangle[:x].to_i
      y = rectangle[:y].to_i
      width = rectangle[:width].to_i
      height = rectangle[:height].to_i
      img = img.excerpt(x, y, width, height)
      img = img.separate  # grayscale
      img = img.sharpen(0, 0.95)
      img = img.scale(3.0)
      img.write("#{@path}ocr#{count}.tiff")
      (1..40).each do |i|
        img = img.gaussian_blur(3, 0.5)
        img = img.sharpen(5, 0.64)
      end
      img = img.despeckle
      img.write("#{@path}ocr_file#{count}.tiff")
    end
    rt = RTesseract.new("#{@path}ocr_file#{count}.tiff", :lang => "eng", :processor => 'rmagick')
    puts "SpellChecking #{rt.to_s}"
    spell_check = SpellCheck.new
    puts "SpellChecking found #{spell_check.check(rt.to_s, search_string)}"
    puts "Should have found #{search_string}"
    return_object[:results].merge! "#{key}" => spell_check.check(rt.to_s, search_string) == search_string
    puts "return object #{return_object}"
    count += 1
    end
  return return_object
end

puts do_ocr_on_image ARGV[0], ARGV[1]

