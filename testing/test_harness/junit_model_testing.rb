# junit model testing
#
# tests on junit_models.rb
#
#

require_relative 'junit_models'

def do_junit_model_test

  test_case = TestCase.new
  test_case.skipped = ""

  1.upto(3).each do |i|
    err_item = Error.new "warn", "message #{i}"
    test_case.errors.push err_item

    failure_item = Failure.new "epic fail :-)", "this can't be real... we code no failures :-P [#{i}]"
    test_case.failures.push failure_item

    test_case.system_outs.push "system_out line #{i} of 3"
    test_case.system_errs.push "system_err line #{i} of 3"
  end

  test_case.name = "name of test case"
  test_case.assertions = "assert 1"

  test_case.time = Time.now.to_s
  test_case.classname = "junit_model_testing"
  test_case.status = "current status"

  property = Property.new "property_name", "property_value"

  properties = Properties.new
  properties.properties.push property

  local_hostname = `hostname`
  test_suite = TestSuite.new "name", "1", "", "", Time.now.to_s, "false", "false", "", local_hostname.strip, "", ""
  test_suite.properties = properties
  test_suite.testcases.push test_case
  test_suite.system_out = "system out message"
  test_suite.system_err = "system err message"

  puts test_suite.to_xml
end

if __FILE__ == $0
  do_junit_model_test
end

