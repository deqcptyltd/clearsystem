require 'yaml'

require_relative '../../corelibs/ruby/tcp_client'
require_relative '../../corelibs/ruby/clear_logger'
require_relative '../../protocols/ruby/clear_protocol'
require_relative 'message_handler'
require_relative 'transaction_module'
require_relative 'test_logger'
require_relative 'junit_models'

include Clear_Protocol
include MessageHandler
include Transaction_Module

### MODULE START ###
module Test_Module
  @local_ip = "172.31.225.111"
  @local_port = 51234
  @mode_tcp_or_stdout = "tcp"
  @local_slot_nr = 1
  @tx_number = 0
  @engine_slot_nr = 0
  @tcp_client_connected = false
  @tcp_response_received = false
  @tcp_client = nil
  @pid_to_remember = nil    #used to track a started process that you would like to stop
  @engine_running = false

  @test_logger = nil
  @any_auto_test_failures = false

  @test_suite = nil

  CONFIG_FILENAME = "config.yml"

  def set_ip_address value
    if value.class == Array
      @local_ip = value[0].to_s
    else
      @local_ip = value
    end

    if @test_logger != nil
      @test_logger.log_message "set_ip_address #{@local_ip}>pass"
    end
    store_last_config_in_file
  end

  def get_ip_address
    @local_ip
  end

  def set_port_number value
    @local_port = check_number(value)

    if @test_logger != nil
      @test_logger.log_message "set_port_number #{@local_port}>pass"
    end
    store_last_config_in_file
  end

  def get_port_number
    @local_port
  end

  def set_tcp_or_stdout value
    if value.class == Array
      @mode_tcp_or_stdout = value[0].to_s
    else
      @mode_tcp_or_stdout = value
    end
    store_last_config_in_file

    if @test_logger != nil
      @test_logger.log_message "set_tcp_or_stdout #{@mode_tcp_or_stdout}>pass"
    end
  end

  def get_tcp_or_stdout
    @mode_tcp_or_stdout
  end

  def set_slot_nr value
    @local_slot_nr = check_number(value)

    if @test_logger != nil
      @test_logger.log_message "set_slot_number #{@local_slot_nr}>pass"
    end
    store_last_config_in_file
  end

  def get_slot_nr
    @local_slot_nr
  end

  def set_engine_slot_nr value
    @engine_slot_nr = check_number(value)

    if @test_logger != nil
      @test_logger.log_message "set_engine_slot_number #{@engine_slot_nr}>pass"
    end
    store_last_config_in_file
  end

  def get_engine_slot_nr
    @engine_slot_nr
  end

  def get_engine_running
    @engine_running
  end

  def check_number value
    return_value = nil
    if value.kind_of?(Array)
      return_value = value[0].to_s.to_i
    else
      return_value = value.to_i
    end
    return_value
  end

  # Load static config variables from config file for persistence
  def check_and_load_test_config_file
    if File.exists?(CONFIG_FILENAME)
      object = YAML.load(File.read(CONFIG_FILENAME))
      if object.has_key?(:ipaddress)
        @local_ip = object[:ipaddress]
      end
      if object.has_key?(:port)
        @local_port = object[:port]
      end
      if object.has_key?(:mode)
        @mode_tcp_or_stdout = object[:mode]
      end
      if object.has_key?(:slot_nr)
        @local_slot_nr = object[:slot_nr].to_i
      end
      if object.has_key?(:engine_slot_nr)
        @engine_slot_nr = object[:engine_slot_nr].to_i
      end
    end
  end

  def store_last_config_in_file
    if File.exists?(CONFIG_FILENAME)
      File.delete(CONFIG_FILENAME)
    end
    object = {:ipaddress => @local_ip,
              :port => @local_port,
              :slot_nr => @local_slot_nr,
              :mode => @mode_tcp_or_stdout,
              :engine_slot_nr => @engine_slot_nr,
              :eofindicator => "***"
              }
    File.open(CONFIG_FILENAME, 'wb') { |f| f.write(YAML.dump(object)) }
  end

  def get_tx_number
    @tx_number += 1
  end

  

  def do_infrared command_nr
    command_nr_to_use = nil
    if command_nr.class == Array
      command_nr_to_use = command_nr[0].to_i
    else
      command_nr_to_use = command_nr
    end
    pushbutton_tx_number = self.get_tx_number
    pb = PushButton.new tx_number: pushbutton_tx_number
    pb.set_slot_number @local_slot_nr
    pb.set_button_command command_nr_to_use	#from clear_protocol.txt
    if @mode_tcp_or_stdout == "stdout" # print output to debug
      return_xml_binary = String(pb.to_xml true)
      return_xml_binary += "\n" + String(pb.to_binary)
      return return_xml_binary
    end
    if @mode_tcp_or_stdout == "tcp"
      Transaction_Module.add_transaction pushbutton_tx_number, "do_infrared #{command_nr_to_use}"
      msg = send_to_tcp  @local_ip, @local_port, pb.to_binary
      return msg
    end
  end	#do_infrared

  def connect_tcp_clnt ip_address, port_number
    if @tcp_client == nil
      @tcp_client = TCPSocketClientSocket.new
    end
    @tcp_client.connect ip_address, port_number, method(:handle_message_here), method(:handle_notification_tcp_client)
    # the code below for in case things aren't the way they should be
    @count = 0
    while !@tcp_client_connected && @count < 10
      sleep 1
      @count +=1
    end
    if @count >= 9 && !@tcp_client_connected
      raise "Could not connect to #{ip_address}:#{port_number}"   # JV: was asked to raise exception if the connection
                                                                  # to the remote ip could not be established.
      return "timeout on connect"
    else 
      return "success"
    end
  end

  def send_to_tcp ip_address, port_number, data
    if !@tcp_client_connected
      connect_tcp_clnt ip_address, port_number
    end
    
    if @tcp_client_connected
      msg = @tcp_client.send data
      # wait for ack or nack response
      if !(wait_for_response)
        # if test_suite in use for junit logging we can log a failure here
        # failure to connect

        if @test_suite != nil
          transaction_item = Transaction_Module.get_latest_transaction
          failure = Failure.new "no response-tx number:#{transaction_item.get_number.to_s}", "No response for tcp command: #{transaction_item.get_message}"
          @test_suite.get_testcase.failures << failure
          @test_suite.failures += 1
        end
        if @test_logger != nil
          @test_logger.log_message "fail>No response from #{ip_address}:#{port_number}"
        end
        @any_auto_test_failures = true
        #log :warn, "send_to_tcp failed - no response from remote host"
      end
      return msg
    else
      if @test_suite != nil
        failure = Failure.new "no response couldn't connect to #{ip_address}:#{port_number}", "No response while trying to connect"
        @test_suite.get_testcase.failures << failure
        @test_suite.failures += 1
      end
      if @test_logger != nil
        @test_logger.log_message "fail>Timeout connecting to #{ip_address}:#{port_number}"
      end
      @any_auto_test_failures = true
      err_msg = "connection failed to #{ip_address}:#{port_number}"
      # log :error, err_msg
      return err_msg
    end
  end

  def handle_message_here data
    # JV 2014-03-03 puts "**************test module********************"
    byte_data = []
    data.each_char { |c| byte_data << c.ord }
    # JV 2014-03-03 p byte_data
    # JV 2014-03-03 puts "*********************************************"
    # log :info, "tcpClient - Received data: |#{byte_data}|"

    message = ClearProtocolBinary.new(:data=>byte_data).to_clear_protocol_object
    
    case message.cmd_cat
    when ClearProtocolConst.COMMON_MESSAGES
      transaction_item = Transaction_Module.get_transaction_for_tx_number message.tx_number # get transaction and message details from here
      case message.cmd_type
      when ClearProtocolConst.ACK
        # Do logger stuff 
        Transaction_Module.ack_transaction message.tx_number

        if @test_logger != nil
          @test_logger.log_message transaction_item.get_message + ">pass"
        end
      when ClearProtocolConst.NAK
        Transaction_Module.nack_transaction message.tx_number
        # Update last test_case's stuff with error information on test case for the @test_suite
        if test_case != nil
          test_case = @test_suite.get_testcase
          error = Error.new "NAK", transaction_item.get_message
          test_case.errors << error
          @test_suite.errors += 1
        end
        if @test_logger != nil
          @test_logger.log_message (transaction_item.get_message + ">fail")
          @any_auto_test_failures = true  #This flag is used in the logged report to indicate if any tests failed
        end
      else
        if test_case != nil
          test_case = @test_suite.get_testcase
          error = Error.new "Unkown response", transaction_item.get_message
          test_case.errors << error
          @test_suite.errors += 1
        end
        @any_auto_test_failures = true  #This flag is used in the logged report to indicate if any tests failed
        error_message = "message not an Ack or a Nack - discarded ### information: " + message
        # log :warn, error_message
      end
    end
    # Below parameter used to hold testing app up until a response is received on a sent command
    @tcp_response_received = true
  end

  def handle_notification_tcp_client host, state, message
    # log :info, "tcpClient - Received notification: #{host}-#{state}-#{message}"
    if state && message == "Connected!"
      @tcp_client_connected = true
    end
  end

  def wait_for_response
    (1..100).each { |i| 
      if @tcp_response_received
        @tcp_response_received = false    # reset for next use
        return true   # return from delay loop
      end
      sleep 0.1
    }
    return false
  end

  def test_tcp_comms
    if !@tcp_client_connected
      connect_tcp_clnt @local_ip, @local_port
    end
    if @tcp_client_connected
      @tcp_client.disconnect
      @tcp_client_connected = false
      msg = "INFO: connection successfull to #{@local_ip}:#{@local_port}"
      # log :info, msg
      return msg
      else
      msg = "INFO: connection failed to #{@local_ip}:#{@local_port}"
      # log :info, msg
      return msg
    end
  end

  def validate value
    if value.has_key? :slot_number
      return value[:slot_number] =~ /^[-+]?[0-9]+$/	#Just tests if it is an integer
    end
    if value.has_key? :different_key
      # validate different_key
    end
  end
  
  # This method will read an external file line by line calling the specified function with
  # it's parameters
  def read_script_file filename
    base_filename = filename[0..filename.rindex('.')-1] || filename
    puts "filename: #{filename} base-filename: #{base_filename}"
    # junit instanciation
    # Create a junit test output file
    local_hostname = `hostname`
    @test_suite = TestSuite.new filename, 0, 0, 0, Time.now.to_s, "false", "false", Time.now.to_s, local_hostname.strip, "", ""
    test_case = TestCase.new filename, 0, Time.now.to_s, $0.to_s, ""
    @test_suite.testcases << test_case
    @test_suite.tests += 1

    @test_suite.properties.properties << Property.new("description", "Automated test harness test suite")

    # Instanciate TestLogger for use in automated testing
    @test_logger = TestLogger.new base_filename
    @test_logger.clear_file # Just so that we don't have twenty tests' worth of logged output
    @test_logger.log_message ("---------------BEGIN:"+filename+"---------------")

    File.open(filename, 'r') do |f|
      f.each_line do |line|
        line.strip!
        if line == nil || line == '' || line[0] == '#'
          next  #skip this line as its a comment
        end
        p line
        line_arr = line.split(' ')
        #line_arr.each do |item|
        #  puts item
        #end
        test_case.assertions += 1
        if line_arr.count >= 2
          method_string = line_arr[0]
          do_action(method(method_string), line_arr[1..-1])
        else
          method_string = line_arr[0]
          do_action(method(method_string), nil)
        end
      end
    end
    if @any_auto_test_failures
      message_at_end_of_file = "###############END>fail###############"
    else
      message_at_end_of_file = "###############END>pass###############"
    end
    @test_logger.log_message message_at_end_of_file
    # Write the test_suite XML to file! 
    File.open(base_filename + ".xml", 'w') do |f|
      puts "Writing junit xml to file <" + base_filename + ".xml>"
      f.write @test_suite.to_xml
    end
  end

  def do_action method, params
    if method != nil
      raise ArgumentError.new "#{method} method not of type method" unless method != Method
        puts "Call #{method}"
        if params == nil
          sleep 1.2
          method.call
          return
        end
        param_count = 0
        params.each do |item|
          param_count+=1
          puts "param-#{param_count}: #{item}"
        end
        puts "ok... going to call method..."
        sleep 0.8 # Delay in case IR command not over yet
        method.call(params)
    else
      raise ArgumentError.new "#{method} may not be nil"
    end
  end
  
  def test_delay value
    p value
    sleep_time = check_number value
    p sleep_time
    sleep sleep_time
  end

  def start_engine 
    # start engine here
    @pid_to_remember = Process.fork do
      Dir.chdir "../../clear_engine/"
      exec "ruby engine.rb #{@local_slot_nr}"
    end
    @engine_running = true  # housekeeping
  end

  def stop_engine
    current_pids = `ps -ef | grep -v defunct | awk -F ' ' '{print $2}'` #grep -v defunct just keeps killed processes from being in the returned list
    current_pid_array = current_pids.split(/\n/).reject(&:empty?)
    if current_pid_array.include? @pid_to_remember.to_s
      exit_status = `kill #{@pid_to_remember}; echo $?`
      exit_status = exit_status.chomp # chomp discards newline \n
      if exit_status == "0"
        @engine_running = false
        return "PID found, kill sent"
      end
         @engine_running = false
         return "PID could not be killed"
    else
      @engine_running = false
      return "Pid not found under running services"
    end
  end

  # This used to see which PID was remembered for the engine.rb thread
  def get_pid_of_interest
    @pid_to_remember
  end

  def shutdown
    puts "\n\nshutting down..."
    if @tcp_client_connected
      puts "closing tcp client"
      @tcp_client.disconnect
      @tcp_client_connected = false
    end

    if @engine_running
      puts "closing clear engine"
      stop_engine
      @engine_running = false
    end
  end

  # stuff below used to test a transaction handler module
  # use hash map {:tx_number & :message}
  def test_transactions
    transaction_in_and_out_test = true
    ack_test = true
    nack_test = true

    transaction = []
    transaction[1] = TransactionItem. new(1, "set_ip_address 172.31.225.102") # { tx_number: 1, message: "set_ip_address 172.31.225.102" }
    Transaction_Module.add_transaction 1, "set_ip_address 172.31.225.102"
    transaction[2] = TransactionItem. new(2, "do_infrared 2") # { tx_number: 2, message: "do_infrared 2" }
    Transaction_Module.add_transaction 2, "do_infrared 2"    
    transaction[3] = TransactionItem. new(3, "do_infrared 3") # { tx_number: 3, message: "do_infrared 3" }
    Transaction_Module. add_transaction 3, "do_infrared 3"

    (1..3).each{ |i|
      returned_transaction = Transaction_Module.get_transaction_for_tx_number i
      #puts "stored transaction"
      #p transaction[i]
      #puts "returned transaction"
      #p returned_transaction
      if (returned_transaction.get_number != transaction[i].get_number) ||
        (returned_transaction.get_message != transaction[i].get_message)
        #puts "Transaction_Module.get_transaction_for_tx_number #{i} failed"
        transaction_in_and_out_test = false
      end
    }

    if !(Transaction_Module.ack_transaction 2)
      #puts "Transaction_Module.ack_transaction 2 if entered because false returned"
      ack_test = false
    end
    
    if !(Transaction_Module.nack_transaction 3)
      #puts "Transaction_Module.nack_transaction 3 if entered because false returned"
      nack_test = false
    end

    if transaction_in_and_out_test && ack_test && nack_test
      message = "Test passed - transaction_module.rb functioning correctly"
      puts message
      # log :info, message
    end
    if !transaction_in_and_out_test
      msg = "Transaction_Module.get_transaction_for_tx_number failed"
      # log :error, msg
      puts msg
    end
    if !ack_test
      msg = "Test failed - ack_transaction problem with transaction_module.rb"
      # log :error, msg
      puts msg
    end
    if !nack_test
      msg = "Test failed - nack_transaction problem with transaction_module.rb"
      # log :error, msg
      puts msg
    end
  end

  def test_junit_models
    output = `ruby junit_model_testing.rb`
    puts output.chomp()
  end

end
