# JV 2014-03-05
# Logging app to generate logs for use when doing automated tests
# from test_harness.rb

class TestLogger
  def initialize filename="test_log.log"
    @filename = filename+".log"
  end

  def log_message message
    File.open(@filename, 'a+') do |file_handle|
      # Datetime header not required at this point
      # message = Time.new.inspect + " " + message
      file_handle.puts message
    end
  end

  def clear_file
    File.open(@filename, 'a+') { |file| file.truncate(0) }
  end

end
  
