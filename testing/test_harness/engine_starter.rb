require_relative 'test_harness_modules'

if ARGV.count != 2
  raise "\nPlease input start slot nr and nr of engines as argument 1 and 2\n\n"
end

begin
  start_engine_slot_nr = ARGV[0].to_i
  nr_of_engines_to_start = ARGV[1].to_i
  
  puts "start_engine_slot_nr => #{start_engine_slot_nr}"
  puts "nr_of_engines_to_start => #{nr_of_engines_to_start}"
  Dir.chdir "../../clear_engine"

  pids_of_running_engines = []
  nr_of_engines_to_start.times do |n|
    slot_to_start = start_engine_slot_nr + n
    puts "start engine #{slot_to_start}"
    pid_nr = Process.fork do
      exec "ruby engine.rb #{slot_to_start}"
    end
    pids_of_running_engines << pid_nr
    sleep 3
  end
  Dir.chdir "../testing/test_harness"

  sleep 5
  puts "\n\nPids nrs\n"
  p pids_of_running_engines
  puts "\n\nAny key to kill all engine(s)"
  STDIN.gets.chomp()
  pids_of_running_engines.each { |pid_nr|
    puts "killing pid nr #{pid_nr}"
    Process.fork do 
      exec "kill -9 #{pid_nr}"
    end
  }

  rescue Exception => e
    puts "\noh no... something went horribly wrong...\n"
    raise e
end
