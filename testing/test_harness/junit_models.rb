# junit models
#
# created to aid in automated build testing
# the xml output of these objects will be interpreted by Atlassian Bamboo to confirm successfull testing
# 
# from the url:
# https://svn.jenkins-ci.org/trunk/hudson/dtkit/dtkit-format/dtkit-junit-model/src/main/resources/com/thalesgroup/dtkit/junit/model/xsd/junit-4.xsd
# 
require 'nokogiri'


class Failure
  attr_accessor :failure_type, :message
  def initialize failure_type="", message=""
    @failure_type = failure_type
    @message = message
  end
end

class Error
  attr_accessor :error_type, :message
  def initialize error_type="", message=""
    @error_type = error_type
    @message = message
  end
end

class Properties
  attr_accessor :properties
  def initialize
    @properties = []
  end
end

class Property
  attr_accessor :name, :value
  def initialize name="", value=""
    @name = name
    @value = value
  end
end

class TestCase
  attr_accessor :skipped, :errors, :failures, :system_outs, :system_errs, :name, :assertions, :time, :classname, :status
  def initialize name="", assertions=0, time="", classname="", status=""
    @skipped = "" 
    @errors= []
    @failures = []
    @system_outs = []
    @system_errs = []
    @name = name
    @assertions = assertions
    @time = time
    @classname = classname
    @status = status
  end
  
  def skipped?
    @skipped != ""
  end
end

class TestSuite
  attr_accessor :properties, :testcases, :system_out, :system_err, :name, :tests, :failures, :errors, :time, :disabled, :skipped, :timestamp, :hostname, :id, :package
  def initialize name="", tests=0, failures=0, errors=0, time="", disabled="", skipped="", timestamp="", hostname="", id="", package=""
    @properties=Properties.new #single item
    @testcases=[]
    @system_out=""
    @system_err=""
    @name=name
    @tests=tests
    @failures=failures
    @errors=errors
    @time=time
    @disabled=disabled
    @skipped=skipped
    @timestamp=timestamp
    @hostname=hostname
    @id=id
    @package=package
  end

  def get_testcase
    if @testcases.length > 0
      return @testcases[@testcases.length - 1]
    end
  end

  def to_xml
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.testsuite(name: @name, tests: @tests, failures: @failures, errors: @errors, time: @time, disabled: @disabled, skipped: @skipped, timestamp: @timestamp, hostname: @hostname, id: @id, package: @package) {
        xml.properties {
          @properties.properties.each do |property|
            xml.property(name: property.name, value: property.value) { }
          end  
        }
        @testcases.each do |testcase|
          xml.testcase(name: testcase.name, assertions: testcase.assertions, time: testcase.time, classname: testcase.classname, status: testcase.status) {
            if testcase.skipped?
              xml.skipped {
                testcase.skipped
              }
            end
            testcase.errors.each do |error|
              xml.error(type: error.error_type, message: error.message) { }
            end
            testcase.failures.each do |failure|
              xml.failure(type: failure.failure_type, message: failure.message) { }
            end
            testcase.system_outs.each do |out|
              xml.send(:'system-out', out) 
            end
            testcase.system_errs.each do |err|
              xml.send(:'system-err', err) 
            end
          }
        end
        xml.send(:'system-out', @system_out) 
        xml.send(:'system-err', @system_err) 
      }
    end

    builder.to_xml
  end

end

class TestSuites
  attr_accessor :testsuite, :name, :time, :tests, :failures, :disabled, :errors
  def initialize name="", time="", tests="", failures="", disabled="", errors=""
    @testsuite=[]
    @name=name
    @time=time
    @tests=tests
    @failures=failures
    @disabled=disabled
    @errors=errors
  end
end


