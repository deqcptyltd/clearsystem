require_relative 'controller'
require_relative 'constants_and_settings'
require_relative '../../clearcore/corelibs/ruby/clear_logger'


class ClearSlave
 
  def initialize node_id
    set_mode SETTINGS.mode
    @controller = Controller.new self
    @controller.start_client node_id 
    #possibly start an ir_sync_agent here if does not exist yet

  end
end

node_id  = ARGV[0]

ClearSlave.new node_id
loop do 
  sleep(1)
end
