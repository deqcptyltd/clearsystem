require_relative '../../clearcore/corelibs/ruby/tcp_client'
require_relative '../../clearcore/corelibs/ruby/clear_logger'
require_relative '../../clearcore/corelibs/ruby/constants'
require_relative 'constants_and_settings'
require_relative '../protocols/clear_view_protocol'
require_relative '../protocols/ruby/clear_protocol'
require 'socket'
require 'active_record'
require_relative '../clear_admin_portal/config/environment.rb'


include Clear_View_Protocol
include Clear_Protocol
include Constants

class MessageHandler

  @@status = {}

  def initialize controller
    @controller = controller
    @health_monitor =  Thread.new do
      until !@health_monitor do
        sleep 10
        puts "HEALTH CHECK HAS BEEN EXECUTED"
        return_status
      end
    end
  end

  def handle_message from, message
    puts "SlaveController-Message Handler-handle_message:#{message} from #{from}"
    p message.cmd_cat
    p message.cmd_type
    case message.cmd_cat
    when ClearProtocolConst.ENGINE_MESSAGES
      puts "Controller-Message Handler-Got ClearEngine Command Type"
      case message.cmd_type
      when ClearProtocolConst.PUSH_BUTTON
        puts "Controller-Message Handler-Got PUSH_BUTTON command"
        handle_push_button from, message
      when ClearProtocolConst.STOP_ENGINE
        puts "Controller-Message Handler-Got PUSH_BUTTON command"
        stop_engine message
      when ClearProtocolConst.START_ENGINE
        puts "Controller-Message Handler-Got start engine command"
        start_engine message
      end
    when ClearProtocolConst.COMMON_MESSAGES
      log 'clearsystem', "Controller-Message Handler-Got ClearCommon Command Type"
      case message.cmd_type
      when ClearProtocolConst.ACK
        log 'clearsystem', 'Controller-Message Handler-ACK received'
        handle_ack  message
      when ClearProtocolConst.NAK
        log 'clearsystem', 'Controller-Message Handler-NACK received'
        handle_nak  message
      end
    end
    case message.cmd_cat
    when ClearViewProtocolConst.MASTER_MESSAGES
      puts "Controller-Message Handler-Got Master Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.REQUEST_STATUS
        puts "Controller-Message Handler-Status request received"
        return_status message
      when ClearViewProtocolConst.REQUEST_XTRAVIEW
        puts "Controller-Message Handler-Status request received"
        start_xtraview message
      end
    when ClearViewProtocolConst.SLAVE_MESSAGES
      puts "Controller-Message Handler-Got Slave Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.SLAVE_STATUS_RESPONSE
        puts "Controller-Message Handler-Got Status response command"
      when ClearViewProtocolConst.START_XTRAVIEW
        puts "Controller-Message Handler-Got Status response command"
        start_xtraview message
      else
        puts "Controller-Message Handler-Got Command Type not yet handled in ClearEngine[#{message.cmd_cat}]: #{message.cmd_type} "
      end
    end
  end

  def start_engine message
    slot_number = message.get_slot_number
    prestart_pids = `ps -ef | grep engine.rb | grep -v sh | grep -v grep |   awk -F ' ' '{print $2}'`
    prestart_array = prestart_pids.split(/\n/).reject(&:empty?)
    fork do
    #[$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
    exec  "cd ../clear_engine; ruby -d engine.rb #{slot_number}"
    end
    poststart_pids = `ps -ef | grep engine.rb | grep -v sh | grep -v grep | awk -F ' ' '{print $2}'` 
    poststart_array = poststart_pids.split(/\n/).reject(&:empty?)
    engine_pid = compare(prestart_array, poststart_array)
    @@status[slot_number] = engine_pid
    puts "ENGINE HAS BEEN STARTED FOR SLOT #{slot_number} WITH PID #{@@status[slot_number]}"
    if SETTINGS.type == :view
      while !File.exist? ("../clear_engine/engine_portal/#{slot_number + slot_port_delta}.pid")
        sleep (0.1)
        puts "Waiting for engine_portal"
      end
    end
    sleep 5
    decoder_response = DecoderRequestResponse.new tx_number: message.tx_number
    decoder_response.set_slot_number slot_number
    decoder_response.set_node_ip check_local_ip
    @controller.send decoder_response.to_binary
  end

  def start_xtraview message
    slot_number = message.get_slot_number
    prestart_pids = `ps -ef | grep engine.rb | grep -v sh | grep -v grep |   awk -F ' ' '{print $2}'`
    prestart_array = prestart_pids.split(/\n/).reject(&:empty?)
    fork do
      [$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
      exec "cd ../clearsystem/clear_engine; ruby -d engine.rb #{slot_number}"
    end
    poststart_pids = `ps -ef | grep engine.rb | grep -v sh | grep -v grep | awk -F ' ' '{print $2}'` 
    poststart_array = poststart_pids.split(/\n/).reject(&:empty?)
    engine_pid = compare(prestart_array, poststart_array)
    @@status[slot_number] = engine_pid
    puts "ENGINE HAS BEEN STARTED FOR SLOT #{slot_number} WITH PID #{@@status[slot_number]}"
    while !File.exist? ("../clearsystem/clear_engine/engine_portal/#{slot_number + slot_port_delta}.pid")
      sleep (0.1)
      puts "Waiting for engine_portal"
    end
    sleep 5
    xtraview_request_response = XtraviewRequestResponse.new tx_number: message.tx_number
    xtraview_request_response.set_slot_number slot_number
    @controller.send xtraview_request_response.to_binary
  end

  def stop_engine message
    puts "Controller-Message Handler- stop_engine - "
    @engine_connector = EngineConnector.new message.get_slot_number, self
    @engine_connector.send message.to_binary
  end

  def handle_ack message
    ack = Ack.new :tx_number => message.tx_number
    @controller.send ack.to_binary
  end

  def handle_nack message
    nak = Nak.new :tx_number => message.tx_number
    nak.set_error_code message.get_error_code
    @controller.send nak.to_binary
  end
  
  def report_status status
    slave_status_response = SlaveStatusResponse.new tx_number: -1
    slave_status_response.set_active_slots status
    puts "slave_status_response is #{slave_status_response}(#{slave_status_response.class})"
    p slave_status_response
    binary_response = slave_status_response.to_binary
    @controller.send binary_response
  end


  def return_status 
    current_pids  = `ps -ef | grep engine.rb | grep -v sh | grep -v grep |   awk -F ' ' '{print $2}'`
    current_array = current_pids.split(/\n/).reject(&:empty?)
    puts "Current array for node PIDs is #{current_array}(#{current_array[0].class})"
    status = []
    @@status.each_pair do |slot, pid|
      puts "SLOT IS #{slot}(#{slot.class}) AND PID IS #{pid}(#{pid.class})"
      if pid != nil
        if !current_array.include? pid.to_s
          @@status[slot] = nil
          if File.exist? ("../clearsystem/clear_engine/engine_portal/#{slot + slot_port_delta}.pid")
            File.delete ("../clearsystem/clear_engine/engine_portal/#{slot + slot_port_delta}.pid")
	    puts "Slot #{slot} PID has been removed"
          end
          puts "Slot #{slot} has been reclaimed"
        else
          status << slot
        end
      end
    end 
    puts "Reported status is #{status}"
    report_status status
  end

  def notification_method source, state, message
    puts 'IRController notification_method called!'
    p source
    p state
    p message
  end

  def compare first, second 
    diff = second - first 
    engine_pid = diff[0].to_i
  end

  def send_message message
    p message.tx_number
    transaction = TransactionHandler.get_transaction_from_tx_number message.tx_number
    p transaction
    if transaction 
      puts "Transaction from: #{transaction.from}, for: #{transaction.message} with tx_number: #{message.tx_number} has completed!"
      @controller.send_to_client message.to_binary, transaction.from
    end
  end
  
  def check_local_ip
    orig, Socket.do_not_reverse_lookup = Socket.do_not_reverse_lookup, true  # turn off reverse 
    UDPSocket.open do |s|
      s.connect '8.8.8.8', 1
      s.addr.last
    end
    ensure
      Socket.do_not_reverse_lookup = orig
  end

end 

class EngineConnector
    
  def initialize slot_number, message_handler
    puts "Engine connector started"
    @message_handler = message_handler
    slot = Slot.find_by_slot_number(slot_number)
    @engine_ip = slot.node.ip
    @engine_tcp_port = slot.slot_number + SETTINGS.engine_port_delta
    @engine_tcp_client = TCPSocketClientSocket.new
    @engine_tcp_client.connect @engine_ip, @engine_tcp_port, method(:data_method), method(:notification_method)
  end

  def send data
    puts "Engine connector send started"
    @engine_tcp_client.send data
  end
  
  def data_method data
    puts "data_method -ENGINE CONNECTOR GOT DATA!!!"
    p data
    if data.include? ']'
      puts "ENGINE CONNECTOR data_method IF REACHED"
      split = data.strip.gsub('[', '').split(']')
      split.each do |message|
        split_message = message.split(',')
        message_data = []
        split_message.each do |val|
          message_data << val.to_i
        end
        @message_handler.handle_message @engine_ip, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
      end
    else
      puts "ENGINE CONNECTOR data_method ELSE REACHED"
      message_data = []
      data.each_byte do |b|
        message_data << b
      end
      p message_data
      @message_handler.handle_message @engine_ip, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
    end
  end

  def notification_method source, state, message
    puts 'Engine Connector notification_method called!'
    p source
    p state
    p message
  end

end
