require 'yaml'

# Defines all application constants and settings

# Holds all real time settings that can be accessed or changed
class Settings

  def initialize config_file

    if File.exists? config_file
      yaml = YAML::load(File.open(config_file))
      # Creating meta programming class accessors as defined in the yaml file
      yaml.each_pair do |key, val|
        self.class.instance_eval do
          define_method(key) {return val}
        end
      end
    else
      raise "Missing config.yml file"
    end
  end

end

module Constants

  # Info Constants

  VERSION = "1.0 Alpha"

  # Operational Constants

  SETTINGS = Settings.new 'config.yml'

  DEBUG = SETTINGS.debug

  DATABASE_SETTINGS = ""

end

